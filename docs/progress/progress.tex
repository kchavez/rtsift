\documentclass[10pt]{article}
\usepackage{fullpage,enumitem,amsmath,amssymb,graphicx,forest}
\usepackage{tabularx}
\addtolength{\oddsidemargin}{-.30in}
\addtolength{\evensidemargin}{-.20in}
\addtolength{\textwidth}{.5in}
\addtolength{\textheight}{0.6in}

\begin{document}

\title{\vspace{-7ex}CS 221 Project Progress}
\author{\vspace{-15ex}Kevin Chavez (kjchavez) \and Fahim Dalvi (fdalvi)}
\date{\vspace{-5ex}}
\maketitle
\begin{center}\emph{\vspace{-4ex}This is a combined project for CS 221 and CS 224N.}\end{center}
\section{Introduction}
Yelp provides valuable information for both business owners and customers, namely a view of the customers' perspectives. However, the way this information is currently presented is either too compact (such as an average star rating for the business) or too diluted---some review threads contain hundreds of reviews. Our goal is to create an intermediate representation which is concise but still reflects much of the semantic information in the review thread. This representation can then serve as a useful summary for Yelp users to quickly understand what others are saying about a business.

Yelp has a similar feature on their mobile app, titled ``Review Highlights.'' However, it seems less sophisticated in the way it processes the actual text from the reviews. Even though it leverages additional data that Yelp has about the restaurant, such as the menu, it does a poor job of providing an overall impression of the restaurant. For example, it tells us that people are talking about ``Chicken Satay'', but not what they actually think about it.

\section{Model}
To formalize the notion of a concise representation, we will build on the following assumptions about a review thread and its constituent reviews:
\begin{itemize}\itemsep=-0.5em
\item A review thread is a collection of independent reviews
\item Each review consists of a small number of ``important'' phrases
\item These important phrases are useful for classifying the star rating of a review
\item The rest of the text in the review is less relevant for the user
\end{itemize}

\paragraph{Data.} We decomposed our dataset into multiple entities that we can then use to build our model. A sentence is defined by a sequence of words and a parse tree. We will denote this as
$
	S = ((w_1, w_2, \ldots w_n), P),
$
where $w_1, \ldots w_n$ are the leaves of $P$. Within a sentence, there exist many \textbf{phrases.} The definition of a phrase is identical to that of a sentence, except the parse tree for a phrase must be a subtree of $P$. A review is modelled as a bag of independent sentences ($R = \{S_1, S_2, \ldots S_m\}$). Note, that this is a simplifying assumption, since sentences are not necessarily independent and can modify each others' meanings. Further, we are ignoring the sentence order.

\paragraph{Model.} We decided to model the problem as a feature selection problem. Given a particular review, we seek to extract textual features that are most discriminative in a star-rating classification task. This can be decomposed into two stages. The first is to generate feature proposals. The second is to then filter the proposals down to a size which provides the benefits of a summary. Additionally, we require that the features be human-interpretable or easily convertible to a form that is human-interpretable. 

Our baseline method treats each word as a proposed feature. Then it uses a Naive Bayes model to identify the most useful words depending on their weights. The system proposed for this project is significantly more complex. To propose features, we learn ``phrase clouds'' that exist in the collection of reviews. These capture groups of syntactically-plausible phrases that are semantically similar (see the Algorithms section for details). The feature vector for a review then becomes an indicator of whether or not the review contains a phrase from each phrase cloud. To select the important features, we use an L1 regularized softmax regression to classify reviews by their star rating. For concreteness, suppose we have $K$ phrase clouds, then the feature vector is
$
	\phi(R) \in \mathbf{R}^K,
$
and the parameters of the softmax regression are
$
	\theta \in \mathbf{R}^{5\times K}.
$
The regularization term is given by
\[
	\lambda \sum_{i=1}^K \|\theta_i\|_2\ ,
\]
where each $\theta_i$ is a column of $\theta$. Note that this L1 regularization on the columnwise norms of $\theta$ will encourage entire columns to go to zero, which then suggests that the corresponding phrase cloud is not particularly important for the classification task. We only keep phrase clouds that correspond to non-zero weights in $\theta$ as the useful features.

\section{Baseline system}
In the proposal, we mention using a Multinomial Naive Bayes model (with word stemming) to do star rating classification. However, this is only half the picture. The other half is producing a compact yet descriptive representation of a review thread. We extended our baseline to include this portion of the project. We trained the naive Bayes model on $700,000$ reviews, with an average F1 score of $0.510$ across the 5 classes. See Appendix \ref{appendix:baseline} for details. The resulting parameters, $\theta^k_w = p(\mathrm{word} = w \mid \mathrm{stars} = k)$, and $\gamma^k = p(\mathrm{stars} = k)$, allow us to determine which words are most informative. We compute the negative entropy of the probability distribution over star ratings, given the presence of a particular word. Specifically,
\[
	I(w) = \sum_{k=1}^5 \frac{\theta^k_w \gamma^k}{p(w)} \log \left(\frac{\theta^k_w \gamma^k}{ p(w)}\right).
\]
Greater values of $I$ indicate distributions that are more ``focused'' on a single star-rating. See Appendix \ref{appendix:baseline} for a list of the most discriminative words by class.

\section{Algorithm}
The entire system consists of five independent phases, where the input of each successive phase comes from the previous one. 
\subsection*{Phase 1: Preprocessing}
In this phase we convert all the data that is provided to us by Yelp into a format that is useful for us. We first create a file per business, where each file contains all the reviews (delimited appropriately) for that particular business. We then pass each sentence of every review to the Stanford Parser, and save the resulting parse trees. Therefore, when this phase is complete, we have one file per business, each file containing the parse trees for all of the sentences occurring in the reviews for that particular business. We also have a database of 50 dimensional word vectors from the GloVe project$^{[1]}$.
\subsection*{Phase 2: Proposed feature generation}
The goal of the phase is to use the parse trees from phase 1 and the word vectors to create meaningful distributed vector representations for phrases. We drew inspiration from \emph{Compositional Vector Grammars}, but with several major changes. We process each sentence from Phase 1 using the following steps:
\begin{itemize}
\item We first build a recursive neural network, whose structure mimics that of the parse tree of the sentence. The aim of this neural network is to train several parameters: the weight matrix $W$, the bias for the weight matrix $b$, the reconstruction matrix $U$ and the bias for the reconstruction matrix $c$. At each node, we chose the error to be the reconstruction loss at that node. Specifically, the error is defined as follows:
\[
	E = \left\Vert U a +c - \begin{bmatrix}x_1\\x_2\end{bmatrix} \right \Vert_2^2
\]
where the vector $a$ is the activation at the particular node (i.e. the vector representation of the phrase). Since our dimensionality for the words vectors is fixed, the length of this vector would be equal to the length of the original word vectors ($50$ in our case). The vector of $x_1$ and $x_2$ is a stacked vector of the inputs to this node. The length of this vector depends on the dimensionality of the original word vectors, and the number of children this node has. 
\item Next, we do forward propagation on our neural network. We perform a postorder traversal of the network (Since we know it is analogous to the parse tree in our system), and at each node we compute the activation using the sigmoid function. Specifically, our activations are computed as follows:
$$f\left( W\begin{bmatrix}x_1\\x_2\end{bmatrix} + b\right)$$
where $f$ is the sigmoid function. We also compute the reconstruction loss at each node, as defined in the previous step. 
\item After we have all the activations and reconstruction losses at each step, we perform our back-propagation on the network. The gradients are computed as follows:

\begin{center}
\begin{tabular}{ccccc}
	$ f' =\ a \circ (\mathbf{1}-a) $    &      $r =\ Ua + c - x \ \  \mathrm{(the\ residual)}$     &      $\nabla_c E =\ 2r$    &       $\nabla_U E =\ 2ra^T$ \\
	$\nabla_b E =\ 2U^T r \circ f' $  &  $\nabla_W E =\ 2(U^T r \circ f')x^T$  & \multicolumn{2}{c}{$\nabla_x E =\ -2r + 2\ \left(U{(f'\mathbf{1}^T \circ W)}\right)^T r$}
\end{tabular}
\end{center}
Here, $\circ$ is element-wise multiplication. $f'$ is the gradient of the sigmoid function with respect to the activations at the current node. The gradients for $U$ and $c$ only depend on the current node, while the gradients for $W$ and $b$ depend on the reconstruction losses of the parents of the current node (All the way to the root, as we propagate from the root towards the leaves).
\item For improving the quality of our parameters, we actually use a syntactically untied RNN, and hence have 4 parameters for each ``label'', where in our case a ``label'' is defined as the number of children a particular node has. Hence, all nodes that have $2$ children will contribute towards the training of $W, b, U$ and $c$ associated with nodes having $2$ children, and so on. 
\end{itemize}
Using the above setup, we train the parameters 700,000 reviews. Hence, at the end of this phase, we have $W$ and $b$, using which we can now extract the vector representation of any given phrase using its parse tree. 

\subsection*{Phase 3: Phrase clouds formation}
Once we have the phrase level vector representations, our next step is to form ``phrase clouds''. For example, two sentences like \emph{``I devoured that turkey''} and \emph{``I guzzled that turkey''} are very similar phrases, and it makes sense to group them together into a phrase cloud. To do this clustering, we use the K-means algorithm. We will either tune the number of clusters $K$ manually as a hyper parameter, or modify it dynamically, where we introduce a new cluster if an existing cluster becomes too big (in terms of geometrical distance in the 50-dimensional space, not the number of elements in the cluster). To reduce the run time in this phase, we use the mini-batch K-means algorithm instead of the full-batch version.


\subsection*{Phase 4: Feature filtering} 
The next step is to filter the large number of features (i.e. phrases in our system) we currently have. We use a star-rating classification task to help identify important features. Our underlying assumption here is that phrases that are discriminative in the classification task are also those that carry semantically significant information.  We will use a softmax classifier with L1 regularization (as defined in the model section) to identify which features are most important. Hence, at the end of this phase, we will have marked several phrase clouds as ``important'', and if a phrase is not in any of these phrase clouds, we know that it is not important for the overall representation of the review. 

\subsection*{Phase 5: Review thread representation} 
The last phase is to build up a representation of a review thread. At this time, we believe a simple summation of the review representations should produce a meaningful yet concise representation of a review thread. We are essentially defining what key phrases (representative of the important phrase clouds) make up a review thread. 

\section{Concrete example}
Consider the following review:
\begin{quote}
I love this place!
They have the most \textbf{AMAZING Chicken/Apple/Pecan salad}!
I know, a sports pub and a salad may not seem like they 'go together,' but \textbf{the salad is the best}.
Fresh grilled chicken breast (not pieces, reheated), fresh sliced apples, and a raspberry dressing that is to DIE FOR!
They also have an \textbf{amazing turkey melt}.
I felt like \textbf{the service was great} and \textbf{everything was fresh}.
I have only eaten there during the lunch hour, not evenings... and they seem to have free, self-serve popcorn - freshly popped in the popper.
Last time I ate there was a Friday during lunch.
I highly recommend.
\end{quote}
There is some useful information here, but a lot of it is unnecessary. If this is a review presented at test time, we first use the Stanford Parser to generate parse trees for each of the sentences. Figure \ref{fig:parsetree} shows a sample parse tree. In the second phase, we evaluate each sentence with its parse tree using our trained syntactically untied recursive neural network (SURNN). The outputs of the SURNN yield vector representations of phrases in the review. Some sample phrases from our review are:
\begin{center}
\begin{tabular}{cc}
	{\bf Phrase } & {\bf Vector representation} \\
	the service was great & [$1.21$\ \ $0.12$\ \ldots\ $3.23$] \\
	everything was fresh &  [$0.73$\ \ $3.11$\ \ldots\ $0.28$] \\
	a Friday during lunch & [$0.33$\ \ $0.19$\ \ldots\ $1.12$]
\end{tabular}
\end{center}

In phase 3, we create the review representation using one of two methods. For each phrase in the review, we\ldots
\begin{itemize}
\item \ldots use the clusters as a strict partition. This will give us a sparse, discrete-valued representation of a review, where the representation answers the question of which phrase clouds build up the sentence. For our example, we might have a vector like
\[
	\phi(R) = [2\ 0\ 1\ 0\ 0\ \ldots\ 0\ 1\ 1],
\]
where the first index corresponds to a phrase cloud containing phrases about amazing food, such as the first and third bolded phrases in our review, and the other non-zero elements come from other important (bolded) phrases in the review. Notice that phrases such as ``a Friday during lunch'' are not preserved because they are too far from any centroid of an important phrase cloud.
\item \ldots use the clusters, but weigh each phrase by its inverse distance to the important clusters. This will create a very dense representation, but will preserve the relationships between phrase cloud meanings. For our example, we might have a vector like
\[
	\phi(R) = [0.93\ 0.01\ 1.56\ 0.00\ \ldots\ 2.21\ 0.99\ 0.48],
\]
where $\phi(R)_i = \sum_{p \in R} \frac{1}{1+\|c_i - p\|_2}$, $c_i$ being the centroid of the $i^{th}$ phrase cloud. 
\end{itemize}

We have yet to determine which of these two approaches works best.

The next step depends on our application. If we are doing star rating prediction, we would run the classifier on these features to predict the star rating. If we are doing summarization, we would pick the phrases from the highest ranking phrase clouds in the above review representation. For example, if we were only showing two phrases per review, we would chose a phrase from each of the clouds with the two highest weights. A similar summarization process would also work for review threads, based on the representation from phase 5.

\section{Current State of Affairs}
At this point, we have completely implemented phases 1 and 2 of the system. Phase 3 and 4 have also been implemented, but we need to make a few optimizations before we can run it on the entire dataset. We have parse trees for all 1M+ reviews and we have implemented a syntactically untied recursive neural network in Julia. Our baseline system can now also produce summaries of review threads, which provides a concrete point of comparison.

We do not have any numbers from our primary system yet, as we need to run the dataset on at least phase 4 to get quantifiable results. Phases 1 and 2 were very time consuming, especially as debugging a recursive neural network is not an easy task. We had to spend considerable amount of time to verify the math behind the system before we could even begin implementing it. After implementing the system, we also had to spend some time confirming the correctness of the implementation by gradient checking. We should have results soon, as we have the bits and pieces for all phases, but we just to need optimize and bring them together to get the results.

\section*{References}
[1] http://www-nlp.stanford.edu/projects/glove/

\newpage
\appendix
\section{Figures}

\begin{center}
\begin{forest}
[ROOT[S[S[NP[PRP[I]]][VP[VBD[felt]][SBAR[IN[like]][S[NP[DT[the]][NN[service]]][VP[VBD[was]][ADJP[JJ[great]]]]]]]][CC[and]][S[NP[NN[everything]]][VP[VBD[was]][ADJP[JJ[fresh]]]]][.[.]]]]
\end{forest}
\end{center}
\label{fig:parsetree}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\textwidth]{illustrations/png/phase1}
  \caption{Phase 1}
\end{figure}
\begin{figure}[htbp]
  \centering
  \includegraphics[width=\textwidth]{illustrations/png/phase2}
  \caption{Phase 2}
\end{figure}
\begin{figure}[htbp]
  \centering
  \includegraphics[width=\textwidth]{illustrations/png/phase3}
  \caption{Phase 3}
\end{figure}
\begin{figure}[htbp]
  \centering
  \includegraphics[width=\textwidth]{illustrations/png/phase4}
  \caption{Phase 4}
\end{figure}
\begin{figure}[htbp]
  \centering
  \includegraphics[width=\textwidth]{illustrations/png/phase5}
  \caption{Phase 5}
\end{figure}


\section{Baseline Results}\label{appendix:baseline}
\subsection{Classification performance}
\[
	\begin{array}{|c|ccccc|}
	\hline
	\mathrm{Rating} & 1 & 2 & 3 & 4 & 5 \\
	\hline
	\mathrm{Precision} & 0.499 & 0.382 & 0.430 & 0.521 &  0.701 \\
	\mathrm{Recall} & 0.640 & 0.348 & 0.411 & 0.536 & 0.658 \\
	\mathrm{F1\ score} & 0.561 & 0.364 & 0.420 & 0.528 &  0.679 \\
	\hline
	\end{array}
\]

\subsection{Discriminative words}

\begin{tabular}{c|l}
Rating & Top 10 discriminative words/stems \\
\hline
1-star & worst ,
unprofession,
refund,
rude,
horribl,
scam,
incompet,
disgust,
told,
manag  \\

2-star & meh,
bland,
mediocr,
flavorless,
overpr,
underwhelm,
overcook,
undercook,
lukewarm,
unimpress \\

3-star & a-ok,
3.5,
decent,
noth,
averag,
ok.,
okay,
ok,
lack,
alright \\

4-star & good,
enjoy,
nice,
tasti,
littl,
realli,
like,
pleasantli,
select,
pretti \\

5-star & amaz,
!,
highli,
love,
great,
perfect,
delici,
awesom,
favorit,
best,
\end{tabular}
\linebreak\newline
To create a summarized review thread representation, we would select a value $K$ and take the $K$ most discriminative words from the set of reviews in the thread. For the summarization task, we always have a star rating available as part of the data. Note, of course, that we lose all the context in which these words appeared.

\end{document}
