# Business Clustering using Customer Reviews
Using reviews from sites like Yelp (actually, probably just Yelp for now), we can group together businesses that are similar in the eyes of the customers. Traditional categories don't capture all the features of a business. Why does this matter? We can make intelligent suggestions to floundering business to help them boost their future rating based on the features of similar, but successful, businesses. This can actually be incorporated into the business owner's dashboard in Yelp.

### Fit in with CS 224N
- Extracting "business properties" from natural language reviews
- Deep learning

### Fit in with CS 221
- Clustering

### Ground Truth?
- Use mechanical turk to see how humans cluster business based only on review thread

### Other Notes
- Hot dogs are not a good suggestion for a Chinese restaurant.
- We do all our work under a specific category, i.e. Restaurants
	- We model all sub categories, i.e. American, Chinese
	- We then find similar american and chinese restaurants
	- We suggest the chinese restaurant what they can do based on them being chinese but being similar to the american

### Metrics
How can we quantify how well our clustering algorithm is working, or even before that, how "useful" our representation of a review thread is?

* Use meta tags 


### Notes from Percy
* Hold meta data constant, then learn representations within these groups.
* Clean interface between NLP and AI stuff. NLP for feature learning and AI for applications -- business improvement, star-rating prediction, etc.
* Evaluation: Hand label some small, reasonable number of examples. 
* Possibly look into temporal data...


### Notes from Andrej
* word2vec for word vectors
* Richard Soecher(?) 

## Formalize

### Goal
Create a compact, yet useful representation of review threads.

### Metrics
For *compactness* 


For *usefulness*:

* Intuitively, if we cluster by this representation, we should have *similar* threads in the same cluster
* Quantitatively, some suggestions... 
    1. Given a single review, which threads are most similar? Should contain the review thread that the review actually belongs to.
    2. Similarity between RT representations should correspond to similarities between star ratings.
    3. 
