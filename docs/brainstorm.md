# Brainstorm session 10/13

## Idea 1: Business **Clustering** using Customer Reviews
Using reviews from sites like Yelp (actually, probably just Yelp for now), we can group together businesses that are similar in the eyes of the customers. Traditional categories don't capture all the features of a business. Why does this matter? We can make intelligent suggestions to floundering business to help them boost their future rating based on the features of similar, but successful, businesses. This can actually be incorporated into the business owner's dashboard in Yelp.

### Fit in with CS 224N
- Extracting "business properties" from natural language reviews
- Deep learning

### Fit in with CS 221
- Clustering

### Ground Truth?
- Use mechanical turk to see how humans cluster business based only on review thread

### Other Notes
- Hot dogs are not a good suggestion for a Chinese restaurant.
- We do all our work under a specific category, i.e. Restaurants
	- We model all sub categories, i.e. American, Chinese
	- We then find similar american and chinese restaurants
	- We suggest the chinese restaurant what they can do based on them being chinese but being similar to the american

## Idea 2: Education Trajectories
Using lecture notes, slides, basically any class material, generate sequences of classes that provide the "smoothest" learning experience.

## Idea 3: Auto-Debater
Using a collection of debate transcripts, we create a system that can believably debate you on some restricted domain of topics.
Suppose we use government election data, then we would probably be able to debate about issues like the economy, national security, immigration, etc. Basically an **adversarial chatbot**.

### Baseline

### Challenges

### Oracle


