\documentclass[10pt, twocolumn]{article}
\usepackage{fullpage,graphicx,psfrag,amsmath,amsfonts,verbatim}
\usepackage{tikz,tikz-qtree}
\usepackage{placeins}
\usepackage{natbib}
\usepackage{array}
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\usepackage[small,bf]{caption}
\bibliographystyle{apalike}
\setlength{\bibhang}{2pt}

\input defs.tex

\bibliographystyle{alpha}

\title{RT-SIFT: Concise and Meaningful Review Thread Representations}
\author{Kevin Chavez (kjchavez) \and Fahim Dalvi (fdalvi)}
\date{This is a joint project with CS 221.}

\begin{document}
\maketitle

\section{Introduction}
Yelp provides valuable information for both business owners and customers, namely a view of the customers' perspectives. However, the way this information is currently presented is either too compact (such as an average star rating for the business) or too diluted---some review threads contain hundreds of reviews. Our goal is to create an intermediate representation which is concise but still reflects much of the semantic information in the review thread. This representation can then serve as a useful summary for Yelp users to quickly understand what others are saying about a business.

Yelp has a similar feature on their mobile app, titled ``Review Highlights.'' However, it seems less sophisticated in the way it processes the actual text from the reviews. Even though it leverages additional data that Yelp has about the restaurant, such as the menu, it does a poor job of providing an overall impression of the restaurant. For example, it tells us that people are talking about ``Chicken Satay'', but not what they actually think about it.

\section{Problem Statement}
We aim to produce a representation of review threads that is concise, interpretable, and preserves much of the meaning of the full text. Further, the representation should be useful for various applications such as summarization, topic modeling, and star-rating prediction. This task can be modelled as a feature selection problem which consists of two stages: generating feature proposals and filtering/ranking these features. The first stage automatically produces a large set of human-interpretable candidate features, while the second stage reduces that set to achieve a more concise representation. 

\section{Related Work}
 There has been some work done related to analyzing reviews, primarily on summarization and opinion mining. Although many of these projects focus on specific applications, they inherently have a review representation which is used to arrive at their final results. Several research papers like \cite{Hu-Liu:mining-summarizing} and \cite{Hu-Liu:web-summarizing} use Parts-of-Speech tagging and occurrence frequencies to extract features. They then ``rank'' or classify the features based on context and word opinion datasets, and then use these to create their summaries. Other research projects concentrate on linguistic techniques alone, like \cite{AbulaishEtAl:subjectivity}. They also use Parts-of-Speech tagging, but combine it with subjectivity analysis to extract the most useful features for their summarization. Some research projects like \cite{Barzilay:lexical-chains} use a completely different approach, and extract their features by first selecting a set of candidate words, and finding their lexical chains. Although these projects are all focused on summarization, we can see that they all have inherent representations of the reviews (like POS tags, frequencies etc), which they then filter in order to obtain their summaries.

\section{Baseline}
\FloatBarrier
We use a vector of counts of unique Porter stemmed words as a baseline representation for a review (and review thread). With this set of features, each document would be represented as a $69$K dimensional vector. Using this representation we trained a multinomial naive Bayes model for star-rating classification on $700,000$ reviews, and tested on $300,000$. The scores are reported in Table \ref{table:baseline-prediction}.

\begin{table}[h!]
\begin{tabular}{|c|ccccc|}
\hline
Rating & 1 & 2 & 3 & 4 & 5 \\
\hline
Precision & $0.499$ & $0.382$ & $0.430$ & $0.521$ & $0.701$ \\
Recall & $0.640$ & $0.348$ & $0.411$ & $0.536$ & $0.658$ \\
F1 score & $0.561$ & $0.364$ & $0.420$ & $0.528$ & $0.679$ \\
\hline
\end{tabular}
\caption{Baseline star rating prediction.}
\label{table:baseline-prediction}
\end{table} 

The resulting parameters, $\theta^k_w = p(\mathrm{word} = w \mid \mathrm{stars} = k)$, and $\gamma^k = p(\mathrm{stars} = k)$, allow us to determine which words are most informative. We compute the negative entropy of the probability distribution over star ratings, given the presence of a particular word. Specifically,
\[
	I(w) = \sum_{k=1}^5 \frac{\theta^k_w \gamma^k}{p(w)} \log \left(\frac{\theta^k_w \gamma^k}{ p(w)}\right).
\]
Greater values of $I$ indicate distributions that are more ``focused'' on a single star-rating. To produce a more concise representation of fixed size $K$, we can take the top $K$ features with the highest scores.

\begin{table}
\begin{tabular}{|c|L{6cm}|}
\hline
Rating & Top 10 discriminative words/stems \\
\hline
1-star & worst ,
unprofession,
refund,
rude,
horribl,
scam,
incompet,
disgust,
told,
manag  \\
\hline
2-star & meh,
bland,
mediocr,
flavorless,
overpr,
underwhelm,
overcook,
undercook,
lukewarm,
unimpress \\
\hline
3-star & a-ok,
3.5,
decent,
noth,
averag,
ok.,
okay,
ok,
lack,
alright \\
\hline
4-star & good,
enjoy,
nice,
tasti,
littl,
realli,
like,
pleasantli,
select,
pretti \\
\hline
5-star & amaz,
!,
highli,
love,
great,
perfect,
delici,
awesom,
favorit,
best \\
\hline
\end{tabular}
\caption{High scoring word features for each rating.}
\label{table:baseline-high-scoring-words}
\end{table}

\section{Approach}
\subsection{Feature generation}\label{sec:feature-gen}
To represent a review, $R$, we propose features of the form,
\[
	\phi_k(\mathrm{R}) = \bigg\lvert\{\mathrm{phrase} \mid 
	     \mathrm{phrase} \in \mathrm{R}, \mathrm{phrase} \in \mathrm{C}_k\}\bigg\rvert,
\]
where $C_k$ is the $k^\mathrm{th}$ phrase cloud. In other words, each feature corresponds to a particular phrase cloud and the value of the feature is the number of times a phrase from that cloud appears in the review. The representation at this level can be simple because most of the sophistication goes into producing appropriate phrase clouds.

\paragraph{Phrase clouds.} For every phrase in our dataset, we generate a 50 dimensional vector representation using a trained syntactically untied recursive neural network. Looking across a subset of these phrase vectors, we find clusters of syntactically and semantically similar phrases. Each cluster defines a phrase cloud with a mean vector $\mu$ and a radius $r$. A phrase is a member of the phrase cloud if it's contained within the hypersphere centered at the $\mu$ with radius $r$.

\subsection{Feature filtering}
Feature generation produces a large number of candidate features. Since we seek to produce a concise representation, it is necessary to filter and rank these features. Each proposed feature is given a score, which will later be used to determine the representation of a review. Some may be given a score of negative infinity, which indicates that they will not be part of the representation. One way to compute a score is to train a classifier that predicts the star rating of a review given these features and score models based on their contribution to classification accuracy. We tried various classifiers and ultimately decided to use a multinomial naive Bayes model because it afforded clear interpretations of the scoring metric. 
 
\subsection{Linguistic assumptions}
\paragraph{Principle of compositionality.} Our model builds on the assumption that complex meaning is derived by composing meanings of individual constituents. In particular, the original source of meaning comes from the GloVe word vectors, which have been pre-trained on large corpora. However, we build up meanings of more complex expressions (longer phrases) by using context-specific data and parametrized composition functions from the SURNN.

\paragraph{Independence of phrases.} If phrases are co-dependent, then the task of finding an optimal set of features for the review representation becomes an exponential-size problem. To make the task of feature filtering tractable, we assume that, given a star rating, the presence of a phrase from one particular phrase cloud in the review is independent of the presence of a phrase from any other phrase cloud. While this assumption may not be valid for some short phrases, longer phrases are more likely to be self-contained.

\paragraph{Speaker irrelevance.} Reviews come from many different ``speakers,'' but we treat text from all speakers identically. While it is possible to create models of Yelp users and modify our treatment of their reviews accordingly, this complicates the system significantly. Instead, our treatment of the review text depends only on the star rating associated with that review.

\section{Data}
We used the \emph{Yelp Academic Dataset} as our primary source of data for this project. We had about 41,956 business with a cumulative total of 1,125,458 reviews at our disposal. We also used 50-dimensional word vectors from the \emph{Global Vectors for Word Representation} (GloVe) project. This project has representations for 400,000 words.

\section{Implementation}

\subsection{Preprocessing}
In this step we convert all the data that is provided to us by Yelp into a format that is useful for us. We first create a file per business, where each file contains all the reviews (delimited appropriately) for that particular business. Next, we tokenize these reviews into sentences, so that we can work at the level of a sentence. We then pass each sentence of every review to the Stanford Parser, and save the resulting parse trees. Therefore, when this step is complete, we have one file per business, each file containing the parse trees for all of the sentences occurring in the reviews for that particular business.

\subsection{Phrase extraction}
To create phrase vectors for use in feature generation, we first train a syntactically untied recursive neural network (SURNN). An SURNN consists of various parameterized composition functions that generate a parent vector from some set of child vectors. The choice of composition function is determined by the syntactic combination that occurs between the children to create the parent phrase. Specifically, for a parent $p$ and children $x_1, \ldots x_k$ related by the $i^\mathrm{th}$ composition function, we have
\[
	p = f^{(i)}\left(x_1,\ldots,x_k\right) = g \left(W^{(i)} \left[\begin{matrix}
					x_1 \\
					\vdots \\
					x_k
				   \end{matrix}\right] + b^{(i)} \right)
\]
where $g$ is a nonlinear function, $W^{(i)} \in \reals^{d \times dk}$ and $b^{(i)}, p, x_j \in \reals^d$. In our system, we use the logistic function $g(x) = \frac{1}{1+\exp(-x)}$. Each training example for the SURNN consists of a sentence from a review, its parse tree, and the review's star rating. The choice of composition function at each node in the tree is determined by the tag of the parent (e.g. NP, VP, S, etc.) and the number of children it has. (Note: technically this is only partially syntactically untied since, for example, NP $\to$ DET NN and NP $\to$ ADJ NP would use the same composition function.)

We wish to find parameters for the compositions such that we can approximately reconstruct the child vectors from a parent vector using an affine transformation and such that we can accurately classify the star rating from the vector representations of the top level phrases.

\paragraph{Reconstruction.} To reconstruct the child vectors, we introduce two new parameters for each composition $U^{(i)} \in \reals^{dk \times d}$ and $c^{(i)} \in \reals^{dk}$ such that our reconstruction is given by
\[
	\left[\begin{matrix}
					\hat{x}_1 \\
					\vdots \\
					\hat{x}_k
				   \end{matrix}\right] = U^{(i)} p + c^{(i)}.
\]
Let $T$ be the parse tree of a sentence. We define the per-sentence reconstruction loss as the sum of the squared errors in reconstructing the children of a node from its parent vector. Specifically, 
\[
	J_r(T) = \sum_{\mathrm{n} \in T} \sum_{j=1}^k \left\|\hat{x}^{(n)}_j - x^{(n)}_j\right\|_2^2
\]
As we compose vector representations higher and higher up the parse tree, the useful and interesting properties of dense, distributed representations start to deteriorate. Therefore, we divide up large trees into sub-trees such that no sub-tree has more than $6$ leaves. The reconstruction loss is computed over all nodes of all sub-trees.

\paragraph{Classification.} 
To classify a sentence's star rating, we use a softmax regression layer, parameterized by $\theta \in \reals^{5 \times n}$, on top of the roots of the sentence's sub-trees. This layer predicts a conditional probability distribution over the 5 star ratings given vector representations at the roots $r_1$ through $r_m$,
\[
	p(s \mid r_1, \ldots, r_m) = \frac{e^{\theta_s^T(r_1 + \cdots + r_m)}}{\sum_{\ell=1}^5 e^{\theta_\ell^T(r_1 + \cdots + r_m)}}.
\]
We define the classification loss to be the regularized logistic loss function,
\[
	J_c(T) = -\sum_{s=1}^5 \mathbf{1}\{s = S\} \log p(s \mid r_1, \ldots, r_m) + \lambda \| \theta \|_\mathrm{fro}^2,
\]
where $S$ is the true star rating of the review containing the sentence.

\paragraph{Loss function.}
The overall loss function for the SURNN consists of a weighted combination of reconstruction and classification loss,
\[
	J(T) = J_r(T) + \beta J_c(T).
\]
By minimizing reconstruction loss, the generated phrase vectors will reflect similarities in syntactic structure as well as some general semantic structure (originating from the pre-trained word vectors). However, augmenting this with the classification loss allows us to propagate supervised label information into the phrase vector space. The parameter $\beta$ controls the trade-off between these two goals.

\paragraph{Local quantities.}
We use stochastic gradient descent to minimize the loss function over our training set. To do so, we must compute the gradient of $J(T)$ with respect to the parameters $W^{(i)}$, $U^{(i)}$, $b^{(i)}$, $c^{(i)}$, and $\theta$. Note that these gradients depend heavily on the structure of the parse tree, so let's define a few quantities that are local to each node in the tree. Consider a node $n$ with $k$ children which uses the composition function $f^{(i)}$. Let $J_r^{(n)}$ be the \emph{local reconstruction loss},
\[
	J_r^{(n)} = \sum_{j=1}^k \left\|\hat{x}^{(n)}_j - x^{(n)}_j\right\|_2^2.
\]
For notational convenience let $g$ be the output vector of this node, $g' = g \circ (\mathbf{1} - g)$ be the derivative of the logistic function with respect to its input, and $x^{(n)}$ be the vertical concatenation of all children vectors. Finally, let $r$ be the residual $r = U^{(i)}g + c^{(i)} - x^{(n)}$. The gradients of the local loss function with respect to the parameters used by this node are
\[\begin{array}{ll}
	\nabla_{W^{(i)}} J_r^{(n)} = 2(U^{(i)T} r \circ g')x^{(n)T} & \nabla_{U^{(i)}} J_r^{(n)} = 2rg^T  \\
	\nabla_{b^{(i)}} J_r^{(n)} = 2U^{(i)T} r \circ p' & \nabla_{c^{(i)}} J_r^{(n)} = 2r
\end{array},\]
where the symbol $\circ$ denotes the Hadamard, or element-wise product. For backpropagation, we will also need the gradient of the local reconstruction loss with respect to the inputs to this node,
\[
	\nabla_{x^{(n)}} J_r^{(n)} = -2r + 2\ \left(U{(g'\mathbf{1}^T \circ W^{(i)})}\right)^T r
\]
For the nodes that are roots of sub-trees, we can also compute simple local gradients of the classification loss with respect to the output vector of the root node,
\[
	\nabla_{r_i} J_c = \theta^T(y - \hat{y}) 
\]
where $y$ is the delta distribution on the correct star rating of the sentence (for example, if it had a rating of $2$ then $y = [0; 1; 0; 0; 0]$) and $\hat{y}$ is the distribution predicted by the softmax model $p(s \mid r_i)$.

It's important to note that the local reconstruction loss at a node is affected by parameters used by descendant nodes, but it is not affected by what happens in its ancestors. Therefore we must propagate each local reconstruction error only through it's subtree.

\paragraph{Backpropagation.}
We will only consider how to propagate the reconstruction loss through a parse tree's structure. For the classification loss, the treatment is more straightforward since the loss function is only defined on ``output nodes,'' or nodes that don't feed into any others. See, for example, \cite{Elkan:Meaning}.

\subparagraph{}To propagate the \emph{total} reconstruction loss, $J_r$, through the tree, we define a variable $\delta_r^{(n)}$ for every node $n$ as
\[
	\delta_r^{(n)} = \nabla_{x^{(n)}} J_r
\]
where $x^{(n)}$ is again the vertical concatenation of all the input vectors to node $n$. If we know the value of $\delta_r^{(p)}$ for this node's parent, $p$, and we know that $n$ is the $k^\mathrm{th}$ child of $p$, then we can efficiently compute $\delta_r^{(n)}$ as the sum of the local gradient and a term that depends on $\delta_r^{(p)}$,
\begin{equation}
	\delta_r^{(n)} = \nabla_{x^{(n)}} J_r^{(n)} + \left(g'\mathbf{1}^T \circ W^{(i)}\right)^T S_k\delta_r^{(p)}, \label{delta_recon}
\end{equation}
where $S_k$ is a selection matrix which effectively selects the $((k-1)d+1)^\mathrm{th}$ to $kd^\mathrm{th}$ elements of $\delta_r^{(p)}$. (Reminder: $d$ is the dimensionality of word/phrase vectors.) The first term is simply the local gradient that we have described in the previous section. 
\begin{figure}
\begin{center}
\includegraphics[scale=0.3]{figures/backprop}
\caption{}
\label{fig:backprop}
\end{center}
\end{figure}
To see where the second term comes from, it's best to use a different notation for the gradients. In particular,
\[
	\nabla_{x^{(n)}} J_r = \frac{\partial J_r}{\partial x^{(n)}} = \frac{\partial J_r^{(n)}}{\partial x^{(n)}} + \frac{\partial \sum_{m \neq n} J_r^{(m)}}{\partial x^{(n)}}
\]
Let's rewrite the second term as
\[
	\frac{\partial \sum_{m \neq n} J_r^{(m)}}{\partial x^{(n)}} = \left(\frac{\partial x^{(p)}}{\partial x^{(n)}}\right)^T \frac{\partial \sum_{m \neq n} J_r^{(m)}}{\partial x^{(p)}}.
\]
Note that since $p$ is the parent of $n$, the local reconstruction loss $J_r^{(n)}$ doesn't depend on $x^{(p)}$. Therefore
\[
	\frac{\partial \sum_{m \neq n} J_r^{(m)}}{\partial x^{(p)}} = \frac{\partial \sum_{m \neq n} J_r^{(m)}}{\partial x^{(p)}}+\frac{\partial J_r^{(n)}}{\partial x^{(p)}}= \frac{\partial J_r}{\partial x^{(p)}} =  \delta_r^{(p)}.
\]
It can be shown that 
\[
	\left(\frac{\partial x^{(p)}}{\partial x^{(n)}}\right)^T = \left(g'\mathbf{1}^T \circ W^{(i)}\right)^TS_k,
\]
which brings us to the form in \eqref{delta_recon}. For any output node, $t$, that doesn't feed into any others,  $\delta_r^{(t)}$ only consists of the gradient of the local reconstruction loss, with respect to $x^{(t)}$

\paragraph{Global gradients.} Let $\left(\nabla_{\mu} J_r\right)_n$ be the contribution to $\nabla_{\mu} J_r$ from node $n$. Note $J_r$ is the total reconstruction loss, not just the local loss. In other words
\[
	\nabla_{\mu} J_r = \sum_{n \in \mathrm{tree}} \left(\nabla_{\mu} J_r\right)_n,
\]
for any parameter $\mu$. Once we have computed $\delta_r^{(n)}$ for each node, we can also compute the node's contributions to the gradients of $J_r$ with respect to the parameters of our model. We state these here without the derivation. Suppose node $n$ used the composition function $f^{(i)}$. Then
\[\begin{array}{l}
	\left(\nabla_{W^{(i)}} J_r\right)_n = \left(S_k\delta_r^{(p)} \circ g'\right) x^{(n)T} + \nabla_{W^{(i)}} J_r^{(n)} \\
	\left(\nabla_{b^{(i)}} J_r\right)_n = \left(S_k\delta_r^{(p)} \circ g'\right) + \nabla_{b^{(i)}}  J_r^{(n)} \\
	\left(\nabla_{U^{(i)}} J_r\right)_n = \nabla_{U^{(i)}} J_r^{(n)} \\
	\left(\nabla_{c^{(i)}} J_r\right)_n = \nabla_{c^{(i)}} J_r^{(n)}.
\end{array}\]

\subsection{Phrase cloud formation}
\FloatBarrier
With a trained SURNN, we extract phrases of lengths $2$ through $6$ from the reviews for all $42$K businesses. Using a subset of $2.5$ million of these phrases, we use K-means to partition the phrases into $50,000$ clusters. The motivation for choosing a particular value of $K$ comes from the corresponding cluster radii distributions. Figure \ref{fig:radii} shows radii distributions for two values of $K$. Notice that for $K=25,000$, the radii distribution is bimodal, whereas for $K = 50,000$, we have a unimodal distribution. A unimodal distribution indicates consistently sized clusters, which partitions the space in an unbiased manner.
\begin{figure}[!htb]
\minipage{0.24\textwidth}
	\includegraphics[width=\linewidth]{figures/radii-25k}
	\caption*{25K clusters}
\endminipage\hfill
\minipage{0.24\textwidth}
	\includegraphics[width=\linewidth]{figures/radii-50k}
	\caption*{50K clusters}
\endminipage
\caption{Comparison of radii distributions.}
\label{fig:radii}
\end{figure}
\FloatBarrier

For each cluster (also referred to as a phrase cloud), we store the centroid and the distance to farthest member of that cluster seen during training. This defines the volume of the phrase vector space that corresponds to that cloud.


\subsection{Phrase cloud scoring}
To score the proposed phrase cloud features, we use an entropy-based value. Across our set of review data, we have around $57$ million phrases, each with an associated star-rating from its parent review. We assume that given a particular star rating for a review, the presence of any of these phrase clouds is independent of any other. Thus, we can compute the probabilities $p(c_i \mid \mathrm{rating} = s)$ for each phrase cloud $c_i$ (this is equivalent to training a Naive Bayes model to predict star ratings over our data). Using Bayes rule and the prior distribution $p(\mathrm{rating} = s)$, we can compute the conditional probability of a review have a particular star rating given the presence of a phrase from phrase cloud $c_i$,
\[
	p(\mathrm{rating} = s \mid c_i) = \frac{p(c_i \mid \mathrm{rating} = s)p(\mathrm{rating} = s)}{p(c_i)}
\]
We use the entropy of this distribution to measure the relevance of a phrase cloud. Note that here we are leveraging labels for star rating prediction, but the phrase clouds themselves have more general structure in them due to the reconstruction objective imposed during phrase extraction. Thus, the score reflects how well a group of structurally and semantically similar phrases align with a particular type of experience as indicated by the star rating. To be concrete, the score of a phrase cloud is defined as the negative entropy
\[
	\mathrm{Score}(c_i) = \sum_{s=1}^5 	p(\mathrm{rating} = s \mid c_i) \log p(\mathrm{rating} = s \mid c_i).
\]
Greater scores indicate distributions that are more “focused” on a single star-rating. Low scores indicate diffuse distributions which suggests mal-formed or less relevant phrase clouds.

For some phrase clouds, the score might be high due to insufficient data. In the extreme case, suppose only one phrase from the $57$ million belonged to this phrase cloud and it happened to come from a review with star rating $4$. Then our distribution over star ratings would be well-peaked at $4$ and would receive a high score, even though such a phrase cloud empirically only has a $1.75\times10^{-6}\%$ chance of occuring. Ideally, with well-formed phrase clouds this shouldn't be a problem. However, we have seen cases while testing that suggest otherwise. Thus we place a threshold on $p(c_i)$. If a cloud is below this threshold, its score is set to $-\infty$.

\subsection{Representing a review}
Let $k$ be the number of phrase clouds with a score greater than negative infinity. We represent a review consisting of the set of phrases $P$ as a vector $\phi(R) \in \reals^k$ of counts of phrases from each the $k$ phrase clouds. Precisely, similar to the form described in section~\ref{sec:feature-gen},
\[
	\phi_k(\mathrm{R}) = \bigg\lvert\{\mathrm{phrase} \mid \mathrm{phrase} \in \mathrm{R}, \mathrm{phrase} \in C_k\}\bigg\rvert.
\]
The difference is that we have filtered out a fair number of phrase clouds. 
 
\section{Results}
\FloatBarrier
\subsection{Phrase clouds}
\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.4]{figures/phrase-vectors-20000-beta-5}
\label{phrase-vec-space}
\caption{Twenty thousand phrase vectors projected into 3D show non-trivial structure.}
\end{center}
\end{figure}

In Table \ref{table:phrase-clouds} we show sample phrases from the highest scoring phrase clouds, separated by the mode star rating. The best way to interpret the phrases in this table: if one of these phrases or a similar one appears in a review with the corresponding star rating, it will certainly be used in the review's representation. If our application is summarization, then the sentence or fragment containing this phrase will be a part of the summary.
\begin{table}
\begin{tabular}{|c|C{6cm}|}
\hline
1 star & horrible customer service, waste your money, add insult to injury, hung up on me\\
\hline
2 star & was just meh, won't be going back, underwhelmed with the main courses, was mediocre at best\\
\hline
3 star & is just okay, hit or miss, did enjoy the fresh teppanyaki, wasn't that impressed
\\
\hline
4 star & won me over, loved the dishes, had a great time, is a great add\\
\hline
5 star & LOVE THIS PLACE !!!, highly recommend this place, Excellent service and care, recommend them to future brides\\
\hline
\end{tabular}
\caption{Representative phrases from high scoring clouds.}
\label{table:phrase-clouds}
\end{table}
For these clusters to be meaningful, we would like similar clouds of phrases to be closer together in the vector space, and those that are most dissimilar to be far apart. If this structure exists, we can build a useful distance metric over review representations. While we do not cluster the phrase cloud centroids explicitly, we analyze the distribution of pairwise distances between cluster centroids. The histogram in Figure \ref{fig:centroid-dist} shows that the distribution of distances is bimodal. This distribution implies that there super-clusters of our phrase clouds.  
\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.4]{figures/cluster-distance-distribution}
\caption{Distribution of pairwise centroid distances}
\label{fig:centroid-dist}
\end{center}
\end{figure}
\FloatBarrier

Analyzing some of the phrase clouds, we notice cohesiveness in syntax as well as semantics among the phrases. We also noticed examples of two phrase clouds with similar syntax, but differing in star-rating orientation. An example of a high scoring five-star cluster contains:
\begin{quote}
\emph{a stellar job, a fantastic job, an incredible job, a super job, a lasting job, an amazing job}
\end{quote}

\subsection{Star rating prediction}
With only a small subset of the data used to create phrase clouds (only $2.5$ million out of $17$ million phrases or around $15$\%), we show that star rating prediction scores are competitive with the baseline model, which generated features by using the entire dataset.  The confusion matrix for the RTSift model is shown in Figure \ref{fig:stars-prediction}; precision, recall and F1 scores are in Table \ref{table:rtsift-pred}. 

\begin{table}[!h]
\begin{tabular}{|c|ccccc|}
\hline
Rating & 1 star & 2 star & 3 star & 4 star & 5 star \\
\hline
Precision & 0.406 & 0.387 & 0.449 & 0.446 & 0.589 \\
Recall & 0.502  & 0.246 & 0.323 & 0.398 & 0.718 \\
F1 & 0.449 & 0.301 & 0.376 & 0.421 & 0.647 \\
\hline
\end{tabular}
\caption{Classification scores for $\beta = 5.0$ RTSift model.}
\label{table:rtsift-pred}
\end{table}

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.4]{figures/confusion_matrix}
\caption{Confusion matrix for star rating prediction}
\label{fig:stars-prediction}
\end{center}
\end{figure}
\FloatBarrier
Even though our scores are not very high, we can see from the confusion matrix that the majority of the errors are near to the diagonal. Hence, the predictions are close to the true predictions, and can be used as a reliable metric for ranking/filtering our features.

\subsection{Summarization}
For getting quantitative results for summarization as an application of our system, we conducted a survey. In the survey, we choose relatively short review threads (20-30 sentences) which each had between 3 to 5 reviews. We then present the users with three summaries (each 5 sentences long), and ask them to choose which of the three choices summarizes the review thread best. The three choices were Random, First-Last and RTSift. Random was just picking random sentences from the review thread. First-Last was a random sample from the set of all first and last sentences from every review in the review thread. Finally, RTSift was the summary from our system. We received a total of 31 responses, with the following results:
\begin{table}[h!]
\begin{center}
\begin{tabular}{|c|cc|}
\hline
System & \#Responses & \%  \\
\hline
Random & $11$ & $35.48\%$ \\
FirstLast & $13$ & $41.94\%$ \\
RTSift & $7$ & $22.58\%$ \\
\hline
\end{tabular}
\caption{Summarization survey results.}
\label{table:summarization-survey}
\end{center}
\end{table} 
\FloatBarrier

Although the results do not look promising, there are a few things to keep in mind:
\begin{enumerate}
\item The model used for the survey only used VP and FRAG as potential features, in order to reduce our processing time.
\item The reconstruction-classification trade-off parameter $\beta$ has not been very finely tuned, again because of time constraints. Trying different values of $\beta$ requires us to re-run the entire pipeline, which runs in the order of a day.
\item The K-means model was also trained on only $15\%$ of the set of all VPs and FRAGs, equivalent to only $4.5\%$ of all phrases. Even though we optimized the K-means phase by using MiniBatch K-means, the runtime was still a limiting factor.
\end{enumerate}

Therefore, we believe that resolving these issues (either by optimizing or using more time) should give us better summaries.

\section{Challenges}
Throughout our journey in the project, we faced multiple challenges. Some of the biggest challenges we encountered and had to tackle were:
\paragraph{Indirect supervision.} The biggest conceptual challenge we had was that even though the available data was labeled, it was labeled for a very specific task, namely star-rating prediction. We wanted our review representation to be generalizable, so that we could use it for other applications like summarization, business comparison, review thread topic modeling, recommendation systems etc. Although star-rating definitely has a relationship to each of these applications (for example, in determining what a summary should contain), we had to make sure that it is not the only feature we depend on.
\paragraph{High dimensionality.} The word vectors we used and the phrase vectors we generated were all in 50 dimensions, far beyond something that can be easily visualized. The number of clusters we were working with to form phrase clouds was also huge, in the order of tens of thousands. Hence, making sense of all of this data to make sure we were in the right direction was very difficult. We ended up using techniques like \emph{Principle Component Analysis} to reduce the dimensionality so that we could get a rough idea of what the space looked like.
\paragraph{Limited processing power.} Our pipeline was very heavy in terms of the processing power required. Even though we tried our best to parallelize each step, there was only so much that could be done. Intermediate results in the pipeline also did not give us a holistic idea of what the final results would look like, and hence the entire pipeline had to be processed in order to judge the success of the system. For example, the first step of the pipeline (converting the sentences in every review to its parse tree) alone took over 50 days of CPU time (which was reduced to a few days in real time thanks to the Barley machines). We had to come up with several heuristics so that each step does not have to process all the data, like early stopping, or processing data in batches so that we converge more quickly to a reasonable solution. This was also keeping in mind that at several stages in the pipeline, the entire data simply could not fit in memory, and hence we had to split the data into batches.
 
\section{Error Analysis}
\subsection{Alternatives}
While implementing the pipeline, we had to re-evaluate the design choices at multiple stages. Initially, we started out with an SURNN with only reconstruction losses as our objective function. After running the complete pipeline, we noticed poor star-rating classification performance. While analyzing the cause, we realized that several phrase clouds did not have a focused star-rating orientation (even though they were syntactically well-formed). To fix this, we decided to introduce classification loss as an additional objective in the SURNN so that the star-ratings affect the structure of the phrase vector space. 

At this point, we had improved our prediction scores, but they were still not comparable to the baseline. We realized that the softmax classifier that we were using as the last stage of our pipeline to select the best features did not have a clear way of ranking \emph{individual} features. Further, we had used a multinomial Naive Bayes classifier in our baseline, and had a well defined ranking metric. This classifier also works well in situations with counts, similar to what we had. Hence, we decided to switch to a multinomial Naive Bayes model for the last stage.

As mentioned earlier, the runtime of our entire pipeline was on the order of a day. Hence debugging was much harder. This led us to investigate alternative ranking schemes that could potential decrease computation time. We implemented a phrase ranking algorithm (instead of clustering and selecting the best features) using the parameters of the SURNN's softmax layer as a foundation. We examined the posterior distribution over star-ratings given the distributed representation of a single phrase vector, and used the same entropy based score to rank phrases. Unfortunately, this did not work out well. The most discriminative phrases according to this approach were of poor quality and we had to fall back to our earlier implementation.
\FloatBarrier
\subsection{Phrase extraction}
The full Yelp Academic Dataset contains quite a large amount of text. Training the SURNN is an expensive operation and it was critical to see how much our system was improving as we used larger and larger subsets of the data. The plot in Figure \ref{fig:training-plot} shows that training past \~4000 businesses proved
to be ineffective in decreasing our average sentence loss (which is the weighted sum of reconstruction and classification losses). The plot helped us determine an early stopping criteria to progress with pipeline.
\begin{figure}[h!]
\begin{center}
\includegraphics[scale=0.4]{figures/model-eval-avg-sentence-loss}
\label{fig:training-plot}
\caption{Training on larger and larger datasets.}
\end{center}
\end{figure}

\subsection{Phrase clouds}
There are a few ways to consider systematic issues our system might face. The most effective has been to consider the application of review thread summarization. Qualitative mistakes in the summary can lead us to more quantitative analysis. For example, we noticed in several instances that the source of poor summaries was elaborate story telling in reviews (which did not necessarily correspond to an actual opinion). Currently, our system has no way of recognizing this, and is bound to make some errors in this regard. A more sophisticated summary system would preprocess/filter the data (some possibilities are discussed in \ref{section:future-work}. Consider the summary with the following head phrases:
\begin{quote}
 is worth of the rating
 - had in the madison area
 - piping hot
 - lives in reality
 - is great pizza
\end{quote}
All of these phrases belong to phrase clouds with relatively low scores. Since these were our top choices, the system chose the ``lesser of many evils''. One reason for this was that we had extracted phrases that were VPs or FRAGs. Expanding the phrase extraction to include more tags would be expected to generate more and better choices.

\section{Future Work}\label{section:future-work}
Our goal throughout the project was to create a review representation that is generalizable, so that it can be used in many different applications. Apart from the summarization that was implemented as part of the project, some possible future applications include:
\paragraph{Business Comparison.} The review representation can be used to compare the key similarities and differences between any given businesses, and can thus help suggest changes a business should make in order to improve its average star rating. Since the representation is based on reviews, this procedure would help capture hidden insights that customers talk about often, not just superficial differences (often found in meta-data) that may or may not be extremely useful.
\paragraph{Recommendation Systems.} We could also use the review representations of past choices that a user has made, and compare it against future choices to predict what business the user would enjoy the most. Again, this is not done with already available data like business type and keywords, but rather with insights from customer reviews.
\paragraph{Topic modeling.} Similar to the summarization application, we could also perform topic modeling on a review thread to grasp key ideas related to a business. 

Other than the applications themselves, there are key ideas that might potentially improve the final representation itself. For example, as mentioned earlier, we tried Softmax Regression, Naive Bayes and a Softmax layer over the SURNN to rank phrases/phrase clouds by their importances. One idea would be to combine these methods together to form an aggregate rank, which may better serve to identify important phrase clouds. It might also be fruitful to try different clustering algorithms other than K-Means, as we notice some bad phrase clouds even with 50,000 clusters. 

On the data side of things, it would be ideal to train on more data, especially K-Means so that it can form better phrase clouds. Experimenting with more or less clusters would also be helpful to find overall better clouds. Also, currently to keep the processing time reasonable, we cutoff the parse trees both while creating the SURNN structure and while extracting phrases in the pipeline. We have not experimented with various levels of cutoff. We also consider only specific tags like NP or VP. The ideal situation would be to consider the complete tree with all the labels and no cutoff.

Finally we can also improve the application considered in this project, summarization. An improvement can be expected by performing subjectivity analysis on a sentence level, so we choose better candidates for our summary. Taking care of dangling anaphoric references would also improve the quality of the summary, as currently sentences picked from anecdotes in the reviews usually have dangling anaphoric references.

%\renewcommand{\refname}{Related Work} 
\nocite{Hu-Liu:mining-summarizing}
\nocite{Hu-Liu:web-summarizing}
\nocite{AbulaishEtAl:subjectivity}
\nocite{Barzilay:lexical-chains}
\nocite{Elkan:Meaning}
\nocite{mikolov2013distributed}
\nocite{socher2011dynamic}
\nocite{pennington2014glove}
%\bibliographystyle{abbrv}
\bibliography{report}

\end{document}
