# CS 224N Project Proposal

Name: Kevin Chavez(kjchavez) & Fahim Dalvi(fdalvi)

`This will be a combined project for CS 221 and CS 224N.`

## Introduction
Yelp provides valuable information for both business owners and customers, namely a view of the customers' perspectives. However, the way this information is currently presented is either too compact (such as an average star rating) or too diluted (such as the full thread of possibly hundreds of reviews). Our goal is to create a representation that is still compact but captures more of the meaning of the review thread. Not only is the compactness of the representation useful in and of itself, but it can also help us perform higher level tasks more effectively. Some example tasks are summarizing a review thread or suggesting ways to improve a business's star rating.

## Our approach
At the lowest level, we'll represent words (and possibly bigrams) using dense word vectors trained on Google News dataset, obtained from Google. Next, we'll learn a compositional vector grammar to create a representation of individual reviews. This creates "meaning vectors" at every node in the review's parse tree. We may have to introduce independence assumptions to simplify the model and keep it tractable. Each phrase (and corresponding "meaning vector"') from the CVG can be treated as a feature for some classification or regression task. However, there are a lot of these, and we would like to reduce the number of features. One approach would be to keep the features that contribute the most to the classification accuracy in predicting the star rating for a review. We'll combine these important features to create a representation of an entire review thread in a way that preserves interpretability. We can then use this representation for the applications described above.

## Evaluation metrics
* Star rating prediction error: Star ratings should reflect the positivity/negativity of a user's overall experience with a business, as should the text of his/her review. Thus, we would expect that any accurate representation of a review can recover the star rating. Hence, a good quantitative metric for the relevance of a representation is the classification precision/recall/F1 score when predicting the rating. At the review thread level, we can use our compact representation of a review thread to predict the star rating for a business. A good representation would ensure that this predicted star rating is close to the true average rating. Therefore, we can use this as another quantitative metric to measure the relevance and accuracy of our system.
* Nearest neighbor visualization: Given a particular review, we can list the closest reviews (by the natural metric in our representation). This is not a quantitative metric, but it can provide some insight as to whether or not our representation preserves intuitive notions of distance and is useful for detailed error analysis.

## Baseline
As a baseline, we create a representation by first stemming all of the words in the review using a Porter Stemmer, then we create a vector using a term frequency/inverse document frequency weighting scheme. Using this representation, we run a multivariate Bernoulli naive Bayes classifier to predict the star rating for each review. There are only 5 classes since a user can only give an integer-valued rating. We trained this baseline model on 700,000 reviews and tested on 300,000. The table below shows our baseline results.

 _____________________________________________________________________________
| Rating    | 1          | 2          | 3          | 4          | 5           |
|-----------|------------|------------|------------|------------|-------------|
| Precision | 0.50924497 | 0.30081271 | 0.36125904 | 0.4851312  |  0.52363328 |
| Recall    | 0.52780993 | 0.29926795 | 0.29470002 | 0.32825908 | 0.70094469  |
| F1 score  | 0.51836128 | 0.30003834 | 0.32460271 | 0.3915678  |  0.59945218 |
 -----------------------------------------------------------------------------

## Challenges
* Fairly large dataset consisting of 145,146,076 words of text. Any algorithm we choose has to scale to that size.
* Hidden user biases in the reviews that skew the ratings. We may have to "normalize" in some way.
* Wide range of lengths of reviews and review threads. For example, there are single sentence reviews and people who write a few paragraphs in their review.
* Finding the balance between compactness and completeness for our review representation.

## Relevant course topics
* CS 221: Neural networks, feature selection, classification, search for fast access to nearest neighbors.
* CS 224N: Parsing, deep learning, phrase clustering, compositional vector grammars.

## Milestones
* Week 1: Create a representation of each review based on word vectors and CVG
* Week 2: Train a classifier to select features that are relevant
* Week 3: Create review thread representation, Create Nearest neighbor and star rating visualization interface
* Week 4: Focus on application 1: Summarization
* Week 5: Focus on application 2: Suggestion creation
* Week 6: Polish and plot

