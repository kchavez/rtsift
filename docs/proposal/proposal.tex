\documentclass[10pt]{article}
\usepackage{fullpage,enumitem,amsmath,amssymb,graphicx}
\addtolength{\oddsidemargin}{-.20in}
\addtolength{\evensidemargin}{-.10in}
\addtolength{\textwidth}{.5in}

\begin{document}
\begin{center}
{\Large CS 221 Project Proposal}
\linebreak
\end{center}
\begin{center}
\begin{tabular}{rcc}
Name: & Kevin Chavez & Fahim Dalvi \\
SUNetId: & kjchavez & fdalvi \\
\end{tabular}
\linebreak\newline
\emph{This will be a combined project for CS 221 and CS 224N.}
\end{center}



\section*{Introduction}
Yelp provides valuable information for both business owners and customers, namely a view of the customers' perspectives. However, the way this information is currently presented is either too compact (such as an average star rating) or too diluted (such as the full thread of possibly hundreds of reviews). Our goal is to create a representation that is still compact but captures more of the meaning of the review thread. Not only is the compactness of the representation useful in and of itself, but it can also help us perform higher level tasks more effectively. Some example tasks are summarizing a review thread or suggesting ways to improve a business's star rating.

\section*{Input-Output behavior}
\paragraph{Representation.} The input to this part of the project would be reviews from the Yelp dataset. We have 1,125,458 reviews across 42,153 businesses from 252,898 distinct users. To train, we will use review text, star ratings and other labeled meta-data. The output would be a compact, yet interpretable representation of an entire review thread for a particular business. 
\paragraph{Applications.} The input to the applications would be the compact representation that was created in the first part of the project, and the output would depend on the application itself. For example, for summarizing a review thread, we would use the compact representation to choose a specific subset of phrases to display. For suggesting ways to improve the star rating for a particular business, we would output particular properties from similar businesses that are thriving more than our target business.  


\section*{Our approach}
At the lowest level, we'll represent words (and possibly bigrams) using dense word vectors trained on Google News dataset, obtained from Google. Next, we'll learn a compositional vector grammar to create a representation of individual reviews. This creates ``meaning vectors'' at every node in the review's parse tree. We may have to introduce independence assumptions to simplify the model and keep it tractable. Each phrase (and corresponding ``meaning vector'') from the CVG can be treated as a feature for some classification or regression task. However, there are a lot of these, and we would like to reduce the number of features. One approach would be to keep the features that contribute the most to the classification accuracy in predicting the star rating for a review. We'll combine these important features to create a representation of an entire review thread in a way that preserves interpretability. We can then use this representation for the applications described above.

\section*{Evaluation metrics}
\paragraph{Star rating prediction error.} Star ratings should reflect the positivity/negativity of a user's overall experience with a business, as should the text of his/her review. Thus, we would expect that any accurate representation of a review can recover the star rating. Hence, a good quantitative metric for the relevance of a representation is the classification precision/recall/F1 score when predicting the rating. At the review thread level, we can use our compact representation of a review thread to predict the star rating for a business. A good representation would ensure that this predicted star rating is close to the true average rating. Therefore, we can use this as another quantitative metric to measure the relevance and accuracy of our system.
\paragraph{Nearest neighbor visualization.} Given a particular review, we can list the closest reviews (by the natural metric in our representation). This is not a quantitative metric, but it can provide some insight as to whether or not our representation preserves intuitive notions of distance and is useful for detailed error analysis.

\section*{Baseline}
As a baseline, we create a representation by first stemming all of the words in the review using a Porter Stemmer, then we create a vector using a term frequency/inverse document frequency weighting scheme. Using this representation, we run a multivariate Bernoulli naive Bayes classifier to predict the star rating for each review. There are only 5 classes since a user can only give an integer-valued rating. We trained this baseline model on $700,000$ reviews and tested on $300,000$. The table below shows our baseline results.
\[
	\begin{array}{|c|ccccc|}
	\hline
	\mathrm{Rating} & 1 & 2 & 3 & 4 & 5 \\
	\hline
	\mathrm{Precision} & 0.50924497 & 0.30081271 & 0.36125904 & 0.4851312 &  0.52363328 \\
	\mathrm{Recall} & 0.52780993 & 0.29926795 & 0.29470002 & 0.32825908 & 0.70094469 \\
	\mathrm{F1 score} & 0.51836128 & 0.30003834 & 0.32460271 & 0.3915678 &  0.59945218 \\
	\hline
	\end{array}
\]

\section*{Oracle}
For our quantitative metric, the oracle would just be the ground truth data from the yelp dataset. After reading through a few reviews, we realized that it is very difficult for humans to predict the correct star rating given five choices. Humans are able to identify if the review is generally positive or negative, but it is very hard for them to distingush between a 4 star review and a 5 star review. Hence, we established that the ground truth would serve as an appropriate upper bound to how good our system can get.

\section*{Challenges}
\begin{itemize}
\item Fairly large dataset consisting of 145,146,076 words of text. Any algorithm we choose has to scale to that size.
\item Hidden user biases in the reviews that skew the ratings. We may have to "normalize" in some way.
\item Wide range of lengths of reviews and review threads. For example, there are single sentence reviews and people who write a few paragraphs in their review.
\item Finding the balance between compactness and completeness for our review representation.
\end{itemize}

\section*{Relevant course topics}
CS 221: Neural networks, feature selection, classification, search for fast access to nearest neighbors.\newline
CS 224N: Parsing, deep learning, phrase clustering, compositional vector grammars.

\renewcommand{\refname}{Related Work} 
\nocite{SocherEtAl2013:CVG}
\nocite{Elkan:Meaning}
\nocite{wang:clustered}
\bibliographystyle{amsplain}
\bibliography{proposal}


\end{document}
