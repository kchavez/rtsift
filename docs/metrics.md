# Metrics
We are developing representations of **reviews** and **review threads** in order to do many higher level tasks more effectively. What are potential tasks?

1. To summarize the content of a review thread
2. To suggest business improvements to improve rating

However, we must first judge the efficacy of our representations with relation to these tasks.

## Representation of a review
How can we measure the effectiveness of our representation? 

* **Star rating prediction error.** Star ratings should reflect the positivity/negativity of a user's overall experience at a restaurant. As should the text of his/her review. Thus, we should expect that any accurate representation of the text can recover the star rating. Possible approaches:
	- **Linear regression.** Predict rating as a continous variable. Consider correct if it rounds to the correct half star.
	- **Softmax regression.** Since we only have 10 distinct classes (one for each half-star), we can use a multiclass classifier.
* **Nearest neighbor visualization.** This is NOT a quantitative metric, but it should be used to see if the representation is intuitive. Given a particular review, we list out which reviews in our set are *closest* by some metric (if using a vector representation, Euclidean distance would be the natural choice).

## Representation of a review thread
As we scale up the representation, the metrics become less intuitive. It is actually more useful to list out desired properties of this level of representation before diving into concrete metrics.

1. **Weak linearity.** Assume we use the same representation for threads of all lengths. Suppose we have a review thread **T** that can be split in half into shorter threads **A** and **B**. Let *R* be our representation function. We would like there to be some (simple) function *f* such that: 

    R(T) = f(R(A),R(B))

If *f(x,y) = x + y*, then this would be a sort of linearity property. Why is this desirable? Consider task #2 above. We're trying to identify certain aspects of a review thread that, perhaps, are missing in others. To do this we should be able to determine that augmenting **A** with the properties of **B** will give us the same properties as **T**.

2. **Star rating prediction.**
