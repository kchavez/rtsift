# Wed Nov 26
## Goals
- Identify when more training yields only marginal gains
- Try a different learning rate schedule
- Fix extractphrases.jl script; new plan is to write one csv for vectors and a separate one for phrases, but maintain ordering. File per business? Probably. We can always concatenate later.
- Extract phrases using one of our best models so far
- Cluster a subset of phrases.
- Project subset of phrases to 2 dimensional space for visualization
- Train SGD classifier on partial data (fraction of reviews and fraction of phrase clusters)
- Anything else?

## What we actually did
- Experiments:
	- Restart medium-sized model to see if we were stuck in local minimum
	- Try full scale model with one composition per permutation of children labels
- Modified extract phrases to take VP, NP, and PP. Writes 3 files per business (.phrases, .vectors, and .tags) 

## Monday December 1
Implemented Softmax layer on top of surnn, but running into NaN issues. Here is the culprit:

String["adam","was","my","server","and","did","a","great","job","of","suggesting","things","as","well","as","being","a","helpful","opinion","when","i","was","choosing","between","one","dish","or","another","...","-","calamari","was","heavenly","and","really","hearty","and","heavy","...","this","is","not","the","kind","of","calamari","that","you","get","at","olive","garden","or","most","italian","restaurants","...","this","was","heavenly","...","perfect","bit","of","spiciness","and","the","portion","was","enormous","-","scampi","gabriela","...","was","amazing","...","the","pasta","was","perfectly","al","dente","...","and","the","shrimp","were","huge","...","a","legitimate","dinner","portion","that","you","had","to","take","home","-","bread","pudding","for","dessert","...","not","at","all","the","type","that","you","get","at","other","restaurants","again","...","this","was","mushy","and","did","n't","resemble","the","bread","pudding","i","'m","use","to","...","but","it","was","actually","the","best","bread","pudding","i","'ve","ever","had","...","it","was","simply","mouthwatering","-","i","had","a","glass","of","pinot","noir","with","my","dinner","and","it","was","a","great","accompaniment","to","my","meal","one","of","the","owners","...","clarissa","came","by","and","personally","asked","how","i","liked","the","food","and","the","service","...","we","had","a","long","chat","...","told","her","what","i","loved","and","a","few","pointers","...","she","has","the","same","name","as","my","mom","so","i","instantly","felt","a","connection","with","her","..."]
String["this","is","a","must","when","in","town","...","i","will","definitely","be","back","!"]

