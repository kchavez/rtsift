 Baseline model using bag of words with tfidf weighting
    with tfidf weighting
            and non-negative matrix factorization.
Commandline args: Namespace(dimensions=50, file=None, num_reviews=1000000, output=None, path='yelp_data/threads')
Vectorizer: CountVectorizer(analyzer=u'word', binary=False, charset=None,
        charset_error=None, decode_error=u'strict',
        dtype=<type 'numpy.int64'>, encoding=u'utf-8', input=u'content',
        lowercase=True, max_df=0.9, max_features=None, min_df=5,
        ngram_range=(1, 1), preprocessor=None, stop_words='english',
        strip_accents=None, token_pattern=u'(?u)\\b\\w\\w+\\b',
        tokenizer=<function stem_tokenizer at 0x7f18ae758b18>,
        vocabulary=None)
Num words: 70388
Training error
[ 0.51983045  0.43567118  0.48012332  0.55080706  0.71593208]
[ 0.65998575  0.40199963  0.46213739  0.56253786  0.67533307]
[ 0.58158328  0.41815866  0.47095869  0.55661065  0.69504021]
Class counts: [  68753.   64112.  102106.  212983.  252046.]
Dev error:
[ 0.49905748  0.38156266  0.42957091  0.52103305  0.70093872]
[ 0.63983253  0.34807153  0.41133163  0.53551427  0.65846151]
[ 0.5607446   0.36404845  0.42025346  0.52817442  0.67903647]
