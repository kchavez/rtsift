""" Useful functions for accessing review data """
import numpy as np
import glob
import os
import random

PHRASE_FILE_PREFIX = "b"
def get_phrases(directory,businesses=None):
    if isinstance(businesses,str):
        businesses = [businesses]

    num_phrases = 0
    if businesses:
        files_to_use = [os.path.join(directory,b) for b in businesses]
    else:
        files_to_use = [b.rsplit(".")[0] for b in glob.glob(
                        os.path.join(directory,PHRASE_FILE_PREFIX+"*.phrases"))]

    for filename in files_to_use:
        try:
            phrase_file = open(filename+".phrases")
            vector_file = open(filename+".vectors")
            tag_file = open(filename+".tags")
        except:
            print "Couldn't open files for", filename
            return

        while True:
            try:
                phrase = next(phrase_file).strip()
                if phrase:
                    vector = map(float,next(vector_file).split(","))
                    tag = next(tag_file).strip()
                else:
                    vector = []
                    assert(next(vector_file) == "\n")
                    tag = ""
                    assert(next(tag_file) == "\n")
            except:
                phrase_file.close()
                vector_file.close()
                tag_file.close()
                break
            num_phrases += 1
            #print num_phrases, phrase, vector[0], tag
            yield (phrase, vector, tag)

    return

class PhraseReader:
    @staticmethod
    def get_reviews(directory,businesses,dimension=50):
        # Load phrases
        phrases = get_phrases(directory,businesses=businesses)
        reviews = [] # list of numpy arrays
        review_phrases = []
        for phrase,vector,tag in phrases:
            if not phrase:
                if review_phrases:
                    reviews.append(np.array(review_phrases))
                else: # We have no phrases for this review
                    pass
                    #print "No phrases found for review"
                    reviews.append(np.zeros((0,dimension)))
                review_phrases = []
                continue
            review_phrases.append(vector)

        return reviews

    @staticmethod
    def random_businesses(directory,N,suffix=""):
        filenames = glob.glob(os.path.join(directory,"*"+suffix))
        businesses = random.sample(filenames,N)
        return [b.rsplit(".")[0].rsplit("/",1)[1] for b in businesses]

    @staticmethod
    def load_phrases(directory,num_phrases=100,dimensions=50):
        data = np.empty((num_phrases,dimensions))
        phrases = []
        phrase_index = 0

        for (phrase,vector,tag) in get_phrases(directory):
            if phrase_index == num_phrases:
                break
            if not phrase:
                continue

            phrases.append(phrase)
            data[phrase_index,:] = vector
            phrase_index += 1

        return phrases, data


def get_reviews(files,N=1000):
    """ Generator for reviews from list of files """
    num_fetched = 0
    for f in files:
        with open(f,'r') as fp:
            for line in fp:
                if num_fetched == N:
                    return
                num_fetched += 1
                if num_fetched % 10000 == 0:
                    print float(num_fetched)/N * 100, "%"
                yield line.split(" ",1)[1].strip().lower()

def get_stars(files,N=1000):
    """ Generator for reviews from list of files """
    num_fetched = 0
    for f in files:
        with open(f,'r') as fp:
            for line in fp:
                if num_fetched == N:
                    return
                num_fetched += 1
                yield int(line.split(" ",1)[0])

class GloveWordVectors:
    unknown = "<-UNK->"

    def __init__(self,filename):
        self.len = 0
        self.model = {}
        self.dim = None
        with open(filename) as fd:
            for line in fd:
                self.len += 1
                tokens = line.split()
                self.model[tokens[0]] = np.array(map(float,tokens[1:]))
                if not self.dim: self.dim = len(tokens[1:])
                assert len(tokens[1:]) == self.dim

        self.UNK = np.random.randn(self.dim)

    def __getitem__(self,item):
        return self.model.get(item.lower(),self.UNK)

    def __iter__(self):
        yield GloveWordVectors.unknown
        for word in self.model:
            yield word

    def __len__(self):
        return self.len+1

    def keys(self):
        return self.model.keys()

