import matplotlib.pyplot as plt
import argparse

def model_number(model):
    return int(model.split(".")[0].split("-")[1])

def model_evaluation(filename):
    models = []
    losses = []
    with open(filename) as fp:
        for line in fp:
            try:
                model,loss = line.split()
            except:
                continue
            models.append(model_number(model))

            losses.append(float(loss)/25724)

    plt.scatter(models,losses)
    plt.title("Model Evalution")
    plt.xlabel("businesses processed")
    plt.ylabel("average sentence loss")
    plt.show()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("plot",choices=["model-evaluation"])

    # Fixed paths
    models_file = "data/temp/rtsift/statistics/model-evaluation.txt"

    args = parser.parse_args()

    if args.plot == "model-evaluation":
        model_evaluation(models_file)

if __name__ == "__main__":
    main()

