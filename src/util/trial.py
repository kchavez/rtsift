import os
import cPickle

class Trial:
    def __init__(self,path,message=""):
        self.path = path
        if not os.path.isdir(self.path):
            os.makedirs(path)
        with open(os.path.join(path,"readme"),'w') as fp:
            print >> fp, message

    def save_model(self,model,filename):
        with open(os.path.join(self.path,filename),'w') as fp:
            cPickle.dump(model,fp)

    def add_to_readme(self,message):
        with open(os.path.join(self.path,"readme"),'a') as fp:
            print >> fp, message
            print message

    def write_file(self,lines,filename):
        with open(os.path.join(self.path,filename),'w') as fp:
            for line in lines:
                print >> fp, line.encode('utf-8')
