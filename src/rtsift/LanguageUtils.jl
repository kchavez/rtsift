module LanguageUtils
export get_filenames,produce_parsetrees,produce_stars,load_wordvectors,get_sentencematrix!, leaves, forestcode

function get_filenames(directory::String)
	if !isdir(directory)
		println("Not a valid directory")
		exit()
	end
	files = readdir(directory)
	return files
end


# TODO: There may be arbitrarily many newlines at end of file
function produce_parsetrees(filename::String)
	f = open(filename)
	curr_tree = ""
	for line in eachline(f)
		if line == "\n"
			produce(curr_tree)
			curr_tree = ""
		end
		curr_tree *= line
	end
	close(f)
end

function produce_stars(filename::String)
    f = open(filename)
    for line in eachline(f)
        produce(int(line[1:1]))
    end
    close(f)
end

function load_wordvectors(filename::String)
	words = Dict{String, Vector{Float32}}()
	fd = open(filename)
	for line in eachline(fd)
		tokens = split(line)
		words[tokens[1]] = map(parsefloat,tokens[2:end])
	end
	return words
end

function leaves(tree_string::String)
	 matches = matchall(r"([^\)\s]+)\)",tree_string)
	 leafs = convert(Vector{String},[lowercase(m[1:end-1]) for m in matches])
	 return leafs
end

function get_sentencematrix!(sentence::Vector{String},word_vectors::Dict{String,Vector{Float32}},data::Matrix{Float32})
	dim = size(data,1)
	for i = 1:length(sentence)
		if !haskey(word_vectors,sentence[i])
			word_vectors[sentence[i]] = randn(dim)
		end
		data[:,i] = word_vectors[sentence[i]]
	end
	return nothing
end

function forestcode(tree)
    result = "[" * tree.label
    if length(tree.children) != 0
        for child in tree.children
            result = result * forestcode(child)
        end
    end
    result = result * "]"
    return result
end

# END MODULE
end
