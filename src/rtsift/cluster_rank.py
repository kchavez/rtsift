import random
import scipy
import numpy as np
import cPickle
import argparse

from sklearn.naive_bayes import MultinomialNB
from sklearn.preprocessing import normalize

from src.rtsift.rtsift import PhraseClusterer

class Ranker:
    def __init__(self,classifier,clusterer):
        """
        Args:
            classifier: A trained naive bayes classifier
            size:       Max number of words to keep in a summary
        """
        self.classifier = classifier
        self.clusterer = clusterer

        class_probs = normalize(np.exp(classifier.feature_log_prob_ +
            np.reshape(classifier.class_log_prior_,(5,1))),norm="l1",axis=0)
        cluster_entropies = scipy.stats.entropy(class_probs)

        # Centroids in sorted order (by entropy)
        self.entropies = cluster_entropies
        self.class_indicators = np.argmax(class_probs,axis=0)
        self.ranks = scipy.stats.rankdata(cluster_entropies)
        self.order = np.argsort(self.entropies)
        self.probabilities = np.sum(np.exp(classifier.feature_log_prob_),axis=0)

    def get_top_clusters(self,label,prob_thresh,N=10):
        candidates = [self.order[i] for i in range(self.order.size) if
                      self.class_indicators[self.order[i]] == label]
        probs = [self.probabilities[c] for c in candidates]

        return [candidates[i] for i in range(len(candidates))
                if probs[i] >= prob_thresh][0:N]

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--clusterer", default="data/temp/rtsift/phrase-clusterer.p-2500000.c-50000.mdl")
    parser.add_argument("--classifier", default="data/temp/rtsift/best-models/nb.p-2500000.c-50000.b-9950.mdl")

    args = parser.parse_args()

    # Load clusterer and classifier
    with open(args.clusterer) as fp:
        pc = cPickle.load(fp)

    with open(args.classifier) as fp:
        clf = cPickle.load(fp)


    ranker = Ranker(clf,pc)

    prob_thresh = np.mean(ranker.probabilities)
    print "Mean probability:", prob_thresh
    for s in range(5):
        clusters = ranker.get_top_clusters(s,prob_thresh)
        filenames = ["data/temp/rtsift/phrase-clouds/cluster-%05d.txt" % c for
                     c in clusters]
        phrases = []
        for f in filenames:
            with open(f) as fp:
                phrases += random.sample(fp.readlines(),5)

        with open("%d-star.ent" % (s+1),'w') as fp:
            for p in phrases:
                print >> fp, p

        print "For %d-stars: Clusters"%(s+1), ",".join([str(x) for x in
            clusters])
        print " Probs....", ",".join(["%0.5f" % ranker.probabilities[x]
            for x in clusters])

if __name__ == "__main__":
    main()
