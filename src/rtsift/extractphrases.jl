using SyntacticallyUntiedRNN
using LanguageUtils
using HDF5, JLD
using ArgParse

# Function: process_parsetrees
# -----------------------------------------------------------------------------
# Extracts the phrases and associated vectors from RNN trees structure.
#
# We are only interested in extracting short phrases, since it is likely that
# the vector representation becomes increasingly noisy/meaningless as we go
# higher up. Thus, extract verb phrases (VP) that have less than |threshold|
# constituents.
function extract_phrases(parsetrees::Vector{RNNNode}; dim::Integer=50, maxlength::Integer=6, minlength::Integer=2)
	phrases = Array(String,0)
	vectors = Array(Vector{Float32},0)
	tags = Array(String,0)

	# Do a depth first search for all nodes of type VP that meet criteria
	function dfs(node::RNNNode)
		if (node.label == "VP" || node.label == "FRAG") && #|| node.label == "S" || node.label == "SBAR" || node.label == "FRAG") &&
		   minlength <= length(node.constituents) <= maxlength
			push!(phrases,join(node.constituents," "))
			push!(vectors,node.activation)
			push!(tags,node.label)
		end
		for child in node.children
			dfs(child)
		end
	end

	for parsetree in parsetrees
		dfs(parsetree)
	end
	return (phrases, vectors, tags)
end

# Function: write_phrases
# -----------------------------------------------------------------------------
# Writes the |phrases| and associated vectors to an open text file.
#
# Args:
#	phrases: 1D array of strings
#	vectors: vectors for each phrase in matrix form where the ith column
#		 corresponds to the ith phrase
#	outputfile: iostream to write the results to (typically open file)
function write_phrases(phrases::Vector{String}, vectors::Vector{Vector{Float32}}, outputfile::IOStream)
	# Loop in column-major order
	for i = 1:length(phrases)
		write(outputfile,phrases[i]*"|"*join([string(x) for x in vectors[i]],",")*"\n")
	end
	return nothing
end

function write_phrases(phrases::Vector{String}, vectors::Vector{Vector{Float32}}, tags::Vector{String}, businessname::String)
	fp = open(businessname*".vectors","a")
	for v in vectors
		write(fp, join(v,",")*"\n")
	end
	close(fp)
	
	fp = open(businessname*".phrases","a")
	for phrase in phrases
		write(fp, phrase*"\n")
	end
	close(fp)
	
	fp = open(businessname*".tags","a")
	for tag in tags
		write(fp, tag*"\n")
	end
	close(fp)
end

function delimit_phrases(businessname::String)
	fp = open(businessname*".vectors","a")
	write(fp, "\n")
	close(fp)
	
	fp = open(businessname*".phrases","a")
	write(fp, "\n")
	close(fp)
	
	fp = open(businessname*".tags","a")
	write(fp, "\n")
	close(fp)
end



# Function: process_business
# -----------------------------------------------------------------------------
# Extracts all phrases from a businesses review thread, processes them with
# the trained surnn, and writes a single file containing all of the phrase
# vectors.
function process_business(business_filename::String,surnn::SURNN,wordvectors,input_path,output_path)
	function producer()
		produce_parsetrees(input_path*"/"*business_filename)
	end

	# Create new output file
	filename = output_path*"/"*business_filename

	count = 0
	end_of_review = false
	for treestring in Task(producer)
		if isempty(strip(treestring))
			if end_of_review
				end_of_review = false
			else
				delimit_phrases(filename)
				end_of_review = true
			end
			continue
		end
		sentence = leaves(treestring)
		data = Array(Float32,surnn.inputdim,length(sentence))
		get_sentencematrix!(sentence,wordvectors,data)
		tree,num_nodes = evaluate(surnn,treestring,data)
		phrases,vectors,tags = extract_phrases(tree)
		count += length(phrases)
		write_phrases(phrases,vectors,tags,filename)
		end_of_review = false
	end
	println("Added ", count, " phrases.")
	return nothing
end

function main()
	s = ArgParseSettings()
    @add_arg_table s begin
        "--wordvectors", "-w"
            help = "file containinf the word vectors"
            default = "../../data/word-vectors/vectors.6B.50d.txt"
        "--parsetrees", "-p"
            help = "path to parse trees"
            default = "../../data/processed/parsetrees/"
        "--model", "-m"
            help = "directory containing the model to use"
            default = "../../data/temp/rtsift/models/surnn-02900.jld"
        "--output", "-o"
            help = "output directory"
            default = "../../data/phrase-vectors/"
    end

    parsed_args = parse_args(ARGS, s)
	# Default paths
	wordvectors_filename = parsed_args["wordvectors"]
	parsetrees_path = parsed_args["parsetrees"]
	output_path = parsed_args["output"]
	surnn_path = "../../data/temp/rtsift/"
	surnn_filename = parsed_args["model"]

	# println(parsetrees_path)
	# println(output_path)

	mkpath(output_path)

	print("Loading word vectors...")
	wordvectors = load_wordvectors(wordvectors_filename)
	println("done.")

	print("Loading surnn model...")
	surnn = load(surnn_filename,"surnn")
	println("done.")

	filenames = get_filenames(parsetrees_path)
	for (i,fn) in enumerate(filenames)
		process_business(fn,surnn,wordvectors,parsetrees_path,output_path)
		if i % 10 == 0
			println(i," businesses processed.")
		end
	end
end

main()
