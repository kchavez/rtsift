module SyntacticallyUntiedRNN
using Softmax
export SURNN, train!, evaluate, predict, reconstruction_loss, classification_loss, RNNNode

typealias Float Float32
# Type: Composition
# -----------------------------------------------------------------------------
# A Composition defines one of the ways the SURNN can combine children
# into a parent node, as well how to decode the original inputs
# from the output. In particular,
#
#               activation = f(W*[x,y] + b)
#           reconstruction = U*activation + c
type Composition
    label::String
    W::Matrix{Float}
    b::Vector{Float}
    U::Matrix{Float}
    c::Vector{Float}
    gW::Matrix{Float}
    gb::Vector{Float}
    gU::Matrix{Float}
    gc::Vector{Float}
    access_counter::Int16
end

# Constructor
# Randomly initializes parameters of the composition using a standard
# normal distribution.
# TODO: More appropriate 'random' initialization, based on fan-in/fan-out
# NOTES: 
#     - sqrt(6/(fan_in + fan_out)) if using tanh or 4x bigger for sigmoid
#     - initialize biases at 0 and reconstruction biases to optimal if weights were 0
#
function Composition(label::String,inputdim::Integer,numchildren::Integer)
	fan_in = 50*numchildren
	fan_out = 50

	r = 4.0*sqrt(6.0/(fan_in+fan_out))

	function randu(r, dims)
		return 2*r*rand(dims...)-r
	end
    Composition(
        label,
        randu(r,(inputdim,inputdim*numchildren)),   # W, n x kn matrix
        zeros(inputdim),                        # b, n vector
        randu(r,(inputdim*numchildren,inputdim)),   # U, kn x n matrix
        randu(r,(inputdim*numchildren)),            # c, kn vector
        zeros(inputdim,inputdim*numchildren),   # gW, n x kn matrix
        zeros(inputdim),                        # gb, n vector
        zeros(inputdim*numchildren,inputdim),   # gU, kn x n matrix
        zeros(inputdim*numchildren),            # gc, kn vector
        0                                       # access_counter, integer
    )
end

# Type: RNNNode
# -----------------------------------------------------------------------------
# An RNNNode holds information related to a single composition in the evaluation
# of a single datum (a sentence with its parse tree).
# For every example we present to the SURNN, we must create a tree of RNNNodes
# to hold intermediate values of computation for both forward and back(ward)
# propagation.
#
# Fields:
#   label           -  
#   param_label         - indicates which composition of the SURNN to use
#   children        -
#   activation      -
#   reconstruction_loss     -
#   gW, gb, gU, gc      -
#   delta           -
type RNNNode 
    label::String
    param_label::String
    children::Vector{RNNNode}
    activation::Vector{Float}
    reconstruction_loss::Float
    constituents::Vector{UTF8String}
end


function RNNNode(label::String,outputdim::Integer)
    RNNNode(label,"",[],zeros(outputdim),0.,[])
end

function extract_param_label(node::RNNNode, children::Vector{RNNNode})
    #return join([child.label for child in children], " ")
    #return join(vcat([string(length(children))],[children[i].label for i = 1:min(length(children),2)])," ")
    #return string(length(children))
    return string(length(children))*" "*node.label
end

function set_children!(node::RNNNode,children::Vector{RNNNode})
    node.param_label = extract_param_label(node, children)
    node.children = children
    node.constituents = vcat([child.constituents for child in children]...)
    return nothing
end
        
# Type: SURNN
# -----------------------------------------------------------------------------
# A syntactically-untied recursive neural network. Stores dictionary of
# parameter labels to "Composition." Each composition stores the parameters 
# needed to process the inputs of a node in the surnn, using:
#
#           f(W*x + b)
#
# It also stores the parameters needed to reconstruct the inputs from the output:
#
#           xhat = U*f(W*x + b) + c
type SURNN
    inputdim::Integer
    parameters::Dict{String,Composition}    # parameters for composition/reconstruction
    theta::Matrix{Float}                  # parameters for softmax regression
    lambda::Float                         # regularization parameter for softmax
    beta::Float                           # trade-off parameter for reconstruction vs classification errors
end

# Constructor
function SURNN(inputdim,num_classes;lambda::Real=0.01,beta::Real=1.0)
    return SURNN(inputdim,Dict{String,Composition}(),zeros(num_classes,inputdim),lambda,beta)
end

# Function: activation
# ---------------------------------------------------------------------------------
# Computes the "output" of a single node in the RNN.
#
# Arguments:
#   (option 1)  W - Weight matrix from the surnn
#           b - Bias vector from the surrnn corresponding to same label
#           x - Concatenated vector of all inputs to this node
#   (option 2)  surnn - Full SURNN structure
#           label - parameter label for this node   
#           x - Concatenated vector of all inputs to this node
function activation(W::Matrix{Float},b::Vector{Float},x::Vector{Float})
    return one(Float) ./ (one(Float) + exp(-(W*x + b)))
end

function activation(surnn::SURNN,label::String,x::Vector{Float})
    return one(Float) ./ (one(Float) + 
                   exp(-(surnn.parameters[label].W*x + 
                   surnn.parameters[label].b))) 
end

# Function: loss
# -----------------------------------------------------------------------------
# Computes the reconstruction loss for a node given either all its parameters
# and the input, or the input, output, and parameter label
function loss(W::Matrix{Float},b::Vector{Float},
          U::Matrix{Float},c::Vector{Float}, x::Vector{Float})
    r = U*activation(W,b,x) + c
    error = x - r

    return dot(error,error)
end

function reconstruction_loss(surnn::SURNN, label::String, input::Vector{Float}, output::Vector{Float})
    theta = surnn.parameters[label]
    error = theta.U*output + theta.c - input

    return dot(error,error)
end

function classification_loss(surnn::SURNN, root::RNNNode, class::Integer)
    return softmax_cost(surnn.theta,root.activation,class; lambda=surnn.lambda)
end

# Total loss includes classification loss and reconstruction loss
function reconstruction_loss(trees::Vector{RNNNode})
    loss = 0.0
    function increment_loss(t)
        loss += t.reconstruction_loss
    end

    for tree in trees
	    postorder(tree,increment_loss)
    end

    return loss
end

function classification_loss(surnn::SURNN, trees::Vector{RNNNode}, class::Integer)
    X = Array(Float,surnn.inputdim,length(trees))
    for (i,t) in enumerate(trees)
        X[:,i] = t.activation
    end
    return softmax_cost(surnn.theta,X,class,surnn.lambda)
end

# Function: gradient
# -----------------------------------------------------------------------------
# Computes the local gradient of the reconstruction loss with respect to a 
# node's parameters (W, b, U, c) and its input (x)
# See project wiki for derivation.
function gradient(comp::Composition, x::Vector{Float})
    f = activation(comp.W,comp.b,x)
    f_prime = f .* (1 - f)
    res = comp.U*f + comp.c - x # residual
    dc = 2*res
    dU = 2*res*f'
    db =2*comp.U'*res .* f_prime
    dW = db*x'
    df_dx = f_prime .* comp.W
    dx  = -2*res + 2*df_dx'*comp.U'*res 
    return dW,db,dU,dc,dx
end

function reset_gradients!(surnn::SURNN)
	# Set all gradients to zero 
    for label in keys(surnn.parameters)
        fill!(surnn.parameters[label].gW,0.)
        fill!(surnn.parameters[label].gb,0.)
        fill!(surnn.parameters[label].gU,0.)
        fill!(surnn.parameters[label].gc,0.)
        surnn.parameters[label].access_counter = 0
    end
end


# Function: forward_propagate
# -----------------------------------------------------------------------------
# Propagates the input "data" through the structure defined by "tree," using
# the current compositions defined in "surnn." If a particular composition in
# the tree does not exist in surnn, it's created
function forward_propagate(surnn::SURNN, node::RNNNode, data::Matrix{Float}; offset::Integer=1)
    if isempty(node.children)
        node.activation = data[:,offset]
        return 1
    end

    i = offset
    for child in node.children
        i += forward_propagate(surnn,child,data; offset=i)
    end

    label = extract_param_label(node, node.children)
    child_activations = vcat([child.activation for child in node.children]...)
    if !haskey(surnn.parameters,label)
        println("Creating new parameters for SURNN.")
        println("Label: ", label)
        surnn.parameters[label] = Composition(label,surnn.inputdim,length(node.children))
    end
    node.activation = activation(surnn,label,child_activations)
    node.reconstruction_loss = reconstruction_loss(surnn,label,child_activations,node.activation)

    return i - offset
end

# Top level call to backpropagate, pass in cdelta computed by using all phrases for a sentence
function backpropagate(surnn::SURNN, node::RNNNode, cdelta::Vector, class::Integer)
    if length(node.children) == 0
        return nothing
    end

    # Local logistic loss function gradient
    label = node.param_label
    
    # Increment the access counter of this composition
    surnn.parameters[label].access_counter += 1
    
    # Local reconstruction gradient
    child_activations = vcat([child.activation for child in node.children]...)
    dW,db,dU,dc,dx = gradient(surnn.parameters[label],child_activations)
    rdelta = dx # delta for propagating reconstruction loss through network
    
    surnn.parameters[label].gW += dW
    surnn.parameters[label].gb += db
    surnn.parameters[label].gU += dU
    surnn.parameters[label].gc += dc

    for (i,child) in enumerate(node.children)
        backpropagate(surnn,child,cdelta,rdelta[(i-1)*surnn.inputdim+1:i*surnn.inputdim],
                      surnn.parameters[label].W[:,(i-1)*surnn.inputdim+1:i*surnn.inputdim])
    end
    return nothing
end

# Back propagation for reconstruction / softmax regression classification combined error
function backpropagate(surnn::SURNN, node::RNNNode, pcdelta::Vector, prdelta::Vector, V::Matrix)
    if length(node.children) == 0
        return nothing
    end

    fprime = node.activation.*(1-node.activation)
    cdelta = fprime .* V'*pcdelta # elem of R^{n}
    label = node.param_label
    
    # Increment the access counter of this composition
    surnn.parameters[label].access_counter += 1
    
    child_activations = vcat([child.activation for child in node.children]...)
    
    # Propagate classification error
    surnn.parameters[label].gW += cdelta*child_activations'
    surnn.parameters[label].gb += cdelta

    # Local reconstruction gradient
    dW,db,dU,dc,dx = gradient(surnn.parameters[label],child_activations)

    # Delta for propagating reconstruction error
    da =  fprime.*surnn.parameters[label].W
    rdelta = dx + da'*prdelta # elem of R^{kn}
    surnn.parameters[label].gW += prdelta.*fprime*child_activations' + dW
    surnn.parameters[label].gb += prdelta.*fprime + db
    surnn.parameters[label].gU += dU
    surnn.parameters[label].gc += dc

    for (i,child) in enumerate(node.children)
        backpropagate(surnn,child,cdelta,rdelta[(i-1)*surnn.inputdim+1:i*surnn.inputdim],
                      surnn.parameters[label].W[:,(i-1)*surnn.inputdim+1:i*surnn.inputdim])
    end
    return nothing
end

function postorder(root,callback::Function)
    for child in root.children
        postorder(child,callback)
    end
    callback(root)

    return nothing
end

function preorder(root,callback::Function,parent,index::Integer)
    callback(root,parent,index)
    for (i,child) in enumerate(root.children)
        preorder(child,callback,root,i)
    end

    return nothing
end

# Parse the output of the Stanford Parser into a tree of RNNNodes
function read_rnn_structure(structure_string::String,dimension::Integer)
    structure_string = replace(structure_string,"\n"," ")
    stack::Vector{RNNNode} = []
    num_nodes = 0
    while length(structure_string) > 0 
        m = match(r"^\s*\(([^\s\)\(]+)\s*(.*)",structure_string)
        if m == nothing
            m = match(r"^([^\s\)\(]+)?\)\s*(.*)",structure_string)
            if m == nothing
                return stack
            end
            if m.captures[1] != nothing
                node = RNNNode(m.captures[1],dimension)
                num_nodes += 1

                node.constituents = [m.captures[1]]
                children = [node]
            else
                children::Vector{RNNNode} = []
            end
            previous = pop!(stack)
            while length(previous.children) != 0 && length(stack) != 0
                push!(children,previous)
                previous = pop!(stack)
            end
            set_children!(previous,reverse(children))
            push!(stack,previous)
            structure_string = m.captures[2]

        else
            parent = RNNNode(m.captures[1],dimension) # Initialize an blank rnn node
            num_nodes += 1
            push!(stack,parent)
            structure_string = m.captures[2]
        end
    end
    num_nodes -= trim!(stack[1])
    return stack[1], num_nodes
end

function trim!(tree)
    if length(tree.children) == 1 && length(tree.children[1].children) == 0
        tree.children = []
        return 1
    end
    
    num_trimmed = 0
    for child in tree.children
        num_trimmed += trim!(child)
    end
    return num_trimmed
end


function analyze_loss(surnn::SURNN,tree; level = 0)
    println("\t"^level, tree.label, " ", sqrt(tree.reconstruction_loss/(surnn.inputdim*length(tree.children))))
    for child in tree.children
        analyze_loss(surnn,child;level=level+1)
    end

    return nothing
end

function extract_phrase_trees(parsetree::RNNNode; maxlength::Integer=6, minlength::Integer=2)
	phrase_roots = Array(RNNNode,0)
	num_explored_nodes = 0
	# Do a depth first search for all nodes of type VP that meet criteria
	function dfs(node::RNNNode)
		if length(node.constituents) < maxlength
			push!(phrase_roots, node)
			return
		end
		num_explored_nodes += 1
		for child in node.children
			dfs(child)
		end
	end

	dfs(parsetree)
	return (phrase_roots, num_explored_nodes)
end

# Function: train
# -----------------------------------------------------------------------------
# Computes
function train!(surnn::SURNN,sentence_tree::String,data::Matrix{Float},class::Integer; alpha::Float=0.01)
    # Forward propagation and truncate tops of trees
    trees, num_nodes = evaluate(surnn, sentence_tree, data)

    reset_gradients!(surnn)
    X = Array(Float,surnn.inputdim,length(trees))
    rootvectors!(trees,X)
    predicted_label = softmax_predict(surnn.theta,X)
    probs = softmax_probs(surnn.theta, X)
    closs = classification_loss(surnn,trees,class)
    rloss = reconstruction_loss(trees)
    
    # Back propagation
    dx, dtheta = softmax_gradient(surnn.theta,X,class,surnn.lambda)
    for t in trees
        backpropagate(surnn,t,surnn.beta*dx,class)
    end

    # SGD step
    # TODO: Implement Adagrad
    surnn.theta = surnn.theta - alpha*surnn.beta*dtheta
    for label in keys(surnn.parameters)
        if surnn.parameters[label].access_counter > 0
            # Scale alpha by surnn.access_counter ?
            step = alpha # / surnn.parameters[label].access_counter
            surnn.parameters[label].W = surnn.parameters[label].W - step*surnn.parameters[label].gW
            surnn.parameters[label].U = surnn.parameters[label].U - step*surnn.parameters[label].gU
            surnn.parameters[label].b = surnn.parameters[label].b - step*surnn.parameters[label].gb
            surnn.parameters[label].c = surnn.parameters[label].c - step*surnn.parameters[label].gc
        end
    end

    return rloss, closs, num_nodes, predicted_label, probs
end

function evaluate(surnn::SURNN, tree::String, data::Matrix{Float})
    original_tree, num_nodes = read_rnn_structure(tree,surnn.inputdim)
    trees, num_explored_nodes = extract_phrase_trees(original_tree)
    num_nodes -= num_explored_nodes

    offset = 1
    for t in trees
	    forward_propagate(surnn, t, data; offset=offset)
	    offset += length(t.constituents)
    end
    return trees, num_nodes
end

function rootvectors!(trees::Vector{RNNNode}, X::Matrix)
    for (i,tree) in enumerate(trees)
        X[:,i] = tree.activation
    end
    return nothing
end

function predict(surnn::SURNN, sentence_parse::String, data::Matrix{Float})
    trees, num_nodes = evaluate(surnn,sentence_parse, data)
    X = Array(Float,surnn.inputdim,length(trees))
    rootvectors!(trees,X)

    return softmax_predict(surnn.theta,X)
end

#run_gradient_check()
# END MODULE
end
