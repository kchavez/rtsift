using SyntacticallyUntiedRNN
using HDF5, JLD

function write_array(filename::String, array)
	writecsv(filename*".csv",array)
end

function main()
	# Default paths
	output_path = "../../data/temp/rtsift/surnn-parameters/"
	
	mkpath(output_path)
	if length(ARGS) < 1
		println("Please provide model file.")
		return
	end

	surnn = load(ARGS[1],"surnn")

	for label in keys(surnn.parameters)
		filename = label*".W"
		write_array(output_path*filename,surnn.parameters[label].W)
		filename = label*".b"
		write_array(output_path*filename,surnn.parameters[label].b)
		filename = label*".U"
		write_array(output_path*filename,surnn.parameters[label].U)
		filename = label*".c"
		write_array(output_path*filename,surnn.parameters[label].c)
	end
    write_array(output_path*"theta", surnn.theta)
end

main()
