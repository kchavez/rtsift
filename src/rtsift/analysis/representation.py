import argparse
import cPickle
from src.rtsift.rtsift import *
from src.baselines.baseline import *
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import CountVectorizer
from src.util.util import *


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--phrase-directory",'-p',dest="phrase_directory",default="data/phrase-vectors")
    parser.add_argument("--num-businesses",'-n',dest="num_businesses",default=100)
    parser.add_argument("--threads-directory",'-t',dest="threads_directory",default="data/processed/threads")
    parser.add_argument("--vectorizer","-v",default="modules/baseline.1000000.module")
    parser.add_argument("module")

    args = parser.parse_args()

    with open(args.module) as fp:
        module = cPickle.load(fp)
        nb = module.classifier
        clusterer = module.clusterer

    with open(args.vectorizer) as fp:
        vectorizer = cPickle.load(fp)
        vectorizer = vectorizer.vectorizer

    businesses = PhraseReader.random_businesses(args.phrase_directory,
            args.num_businesses,suffix=".phrases")

    threads = [os.path.join(args.threads_directory,b) for b in businesses]
    review_matrix = vectorizer.transform(get_reviews(threads,-1))

    reviews = PhraseReader.get_reviews(args.phrase_directory,businesses)
    filenames = [os.path.join(args.threads_directory,b) for b in businesses]
    labels = np.array([s for s in get_stars(filenames,-1)])
    print "Transforming to review representation for dev set"
    rt_data, assignments = clusterer.transform_reviews(reviews)

    print rt_data.shape, review_matrix.shape
    data = np.concatenate((rt_data,review_matrix),axis=1)


    pts = []
    # Modify data to simulate using fewer features
    mod_data = scipy.sparse.lil_matrix(data)
    for i in range(data.shape[1]):
        remove_index = module.order[data.shape[1]-i-1]
        mod_data[:,remove_index] = 0.

        pred_labels = nb.predict(mod_data)

        _,_,scores,_ = metrics.precision_recall_fscore_support(labels,
                                pred_labels)

        score = np.mean(scores)
        pts.append((data.shape[1]-i,score))
        print pts[-1],scores

    with open("rep-vs-size.txt",'w') as fp:
        cPickle.dump(pts,fp)

if __name__ == "__main__":
    main()
