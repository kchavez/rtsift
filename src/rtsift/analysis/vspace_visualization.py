import plotly.plotly as py
from plotly.graph_objs import *
from sklearn.decomposition import RandomizedPCA
import random
import time
import sys
from ...util.util import *
import numpy as np
import glob
import nltk
import os
import argparse

py.sign_in("fdalvi", "3kvkvs1bv8")

def reduce_dimensions(data, n, random_state=None):
    pca = RandomizedPCA(n_components = n, random_state=random_state)
    return pca.fit_transform(data)

def plot_data(x,y,z,labels, showlabels = True):
    if showlabels:
        mode = 'markers+text'
    else:
        mode = 'markers'
    trace1 = Scatter3d(
        x=x,
        y=y,
        z=z,
        text=labels,
        mode=mode,
        marker=Marker(
            color='rgb(127, 127, 127)',
            size=12,
            symbol='circle',
            line=Line(
                color='rgb(204, 204, 204)',
                width=1
            ),
            opacity=0.9
        )
    )
    data = Data([trace1])
    layout = Layout(
        margin=Margin(
            l=0,
            r=0,
            b=0,
            t=0
        )
    )
    fig = Figure(data=data, layout=layout)
    plot_url = py.plot(fig, filename='3d-scatter-vectors')

def get_words(review_path):
    review_thread_files = glob.glob(os.path.join(review_path,"*"))

    reviews = get_reviews(review_thread_files,10)

    words = []
    for review in reviews:
        words += nltk.word_tokenize(unicode(review,"utf-8"))

    return words

def load_word_vectors(word_vectors_path):
    NUM_WORDS = 1000

    print "Loading word vectors from %s..."%word_vectors_path ,
    sys.stdout.flush()
    start = time.time()
    word_vectors = GloveWordVectors(word_vectors_path)
    end = time.time()
    print "done (%d s)"%(end - start)

    words = get_words()

    NUM_WORDS = len(words)
    vectors = np.empty((NUM_WORDS, 50))
    for i,word in enumerate(words):
        vectors[i,:] = word_vectors[word]

    vectors = 100*vectors
    return (words, vectors)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--threads-path",'-p',
                         dest="path",default="data/processed/threads")
    parser.add_argument('--wordvectors','-w',default="data/word-vectors/vectors.6B.50d.txt")
    parser.add_argument('-n',dest="num_phrases",type=int,
                        default=1000,help="max number of phrases to plot")
    parser.add_argument('--phrasevectors','-v',default="data/phrase-vectors")
    parser.add_argument('--showlabels', action='store_true')

    
    args = parser.parse_args()
    
    num_phrases = args.num_phrases
    phrase_vectors_path = args.phrasevectors


    (labels, vectors) = PhraseReader.load_phrases(phrase_vectors_path,
                        num_phrases=num_phrases)
    print len(labels), len(vectors)
    print vectors
    vectors = reduce_dimensions(vectors,3)
    plot_data(vectors[:,0], vectors[:,1], vectors[:,2], labels, showlabels = args.showlabels)

if __name__ == "__main__":
    main()
