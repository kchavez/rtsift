import csv
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("csv",help="file containg qualtrics results")
    parser.add_argument("order",help="file containg order of randomization")

    args = parser.parse_args()

    orders = {}
    with open(args.order, 'r') as of:
        while True:
            try:
                review_thread = next(of).strip()
                order = (next(of).strip(), next(of).strip(), next(of).strip())
                orders[review_thread] = order

                # Read empty line
                next(of)
            except StopIteration:
                break

    with open(args.csv, 'rb') as f:
        reader = csv.reader(f)
        rows = iter(reader)

        # Ignore first row
        next(rows)

        # Parse headers
        headers = next(rows)

        # Format output header
        print "Duration".center(9), \
                  "Review thread".center(32), \
                  "Choice".center(12)
        print "-"*53

        questions = []
        statistics = {'RTSift':0, 'Random':0, 'First-Last':0}
        for i, header in enumerate(headers):
            if "Q_TotalDuration" in header:
                duration_index = i
            if "review thread carefully" in header:
                questions.append(i)
            if "Display Order" in header:
                rt_index = i

        for row in rows:
            q_total_duration = row[duration_index]
            review_thread = row[rt_index].split(' - ')[0]

            for q_index in questions:
                if row[q_index+2] != '':
                    choice = int(row[q_index+2]) - 1
                    break

            print str(q_total_duration).center(9), \
                  str(review_thread).center(32), \
                  str(orders[review_thread][choice]).center(12)

            statistics[str(orders[review_thread][choice])] += 1

        print ''
        total = 0
        for system in statistics:
            total += statistics[system]
        print "System".center(12) + "Count".center(8) + "Percent".center(8)
        print "-"*28
        for system in statistics:
            percent = "%0.2f%%"%(float(100)*statistics[system]/total)
            print system.ljust(12) + str(statistics[system]).center(8) + str(percent).center(8)

        print "-"*28
        print "Total".ljust(12) + str(total).center(8)


if __name__ == "__main__":
    main()