import argparse
import cPickle
from src.rtsift.rtsift import *
from src.util.util import *
import matplotlib.pyplot as plt
import glob

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("module",help="filename of module to load for demo")
    parser.add_argument("--phrase-directory",dest="phrase_directory",
                        default="data/phrase-vectors")
    args = parser.parse_args()

    print "Loading %s module..." % args.module,
    with open(args.module) as fp:
        module = cPickle.load(fp)
    print "done."

    #all_businesses = [f[len(args.phrase_directory):-len('.phrases')] for f in glob.glob(args.phrase_directory + '/*.phrases')]
    all_businesses = ['b01757', 'b20386', 'b21614', 'b27537', 'b04235', 'b33374', 'b06960', 'b14082', 'b31897', 'b09778', 'b28400', 'b01963', 'b31075', 'b03531', 'b09309', 'b35932', 'b23356', 'b14755', 'b03954', 'b28283', 'b30481', 'b30417', 'b35061', 'b06085', 'b20499', 'b31930', 'b05250', 'b13584', 'b28825', 'b23182', 'b28370', 'b24207', 'b14812', 'b08261', 'b03897', 'b33224', 'b24433', 'b10836', 'b16827', 'b13679', 'b06103', 'b03963', 'b25848', 'b28860', 'b07393', 'b39356', 'b02294', 'b10208', 'b41885', 'b06504', 'b15790', 'b39947', 'b32161', 'b33532', 'b06553', 'b18823', 'b14199', 'b07145', 'b02434', 'b38630', 'b12920', 'b19458', 'b33541', 'b28448', 'b06830', 'b05299', 'b13143', 'b33939', 'b22888', 'b07729', 'b33491', 'b16716', 'b22104', 'b21801', 'b06253', 'b27429', 'b37384', 'b20801', 'b35234', 'b24960', 'b12799', 'b12638', 'b31525', 'b33922', 'b11395', 'b03705', 'b00786', 'b10289', 'b02530', 'b24192', 'b21805', 'b24190', 'b15505', 'b22367', 'b40219', 'b06848', 'b17774', 'b33947', 'b04078', 'b14614']
    #print len(all_businesses)
    #return

    confusion_matrix = [[0, 0, 0, 0, 0],  # True label 1
                        [0, 0, 0, 0, 0],  # True label 2
                        [0, 0, 0, 0, 0],  # True label 3
                        [0, 0, 0, 0, 0],  # True label 4
                        [0, 0, 0, 0, 0]]  # True label 5
    for i in range(len(all_businesses)/100+1):
        businesses = all_businesses[i:i+100]

        #review_thread = list(get_reviews(b, -1))
        true_stars = list(get_stars(['data/processed/threads/' + b for b in businesses], -1))

        review_phrases = PhraseReader.get_reviews(args.phrase_directory, businesses)
        # Predict
        data,_  = module.clusterer.transform_reviews(review_phrases)
        stars = module.classifier.predict(data)

        for star,true_star in zip(stars,true_stars):
            confusion_matrix[true_star-1][star-1] += 1
            #print "(%d-STAR) (%d-TRUE)" % (star, true_star)

        print 'Processed %d businesses: '%((i+1)*100)
        for row in confusion_matrix:
            print "\t".join([str(r) for r in row])

        print ''

        # Show confusion matrix in a separate window
        plt.matshow(confusion_matrix)
        plt.title('Confusion matrix')
        plt.colorbar()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        plt.show()

if __name__ == "__main__":
    main()
