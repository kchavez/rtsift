import os
import argparse
import glob
import cPickle
import matplotlib.pyplot as plt
import numpy as np
import scipy
from src.rtsift.rtsift import PhraseClusterer

# kmeans.phrases-100000.clusters-10000.mdl
def evaluate_model(km_filename,output_filename="data/temp/rtsift/kmeans-analysis.txt"):
    # Write "num phrases, num centroids, inertia
    phrases = km_filename.split(".")[1].split("-")[1]
    clusters = km_filename.split(".")[2].split("-")[1]
    with open(km_filename) as fp:
        km = cPickle.load(fp)

    with open(output_filename,'a') as fp:
        print >> fp, phrases, clusters, km.inertia_

    return int(phrases), int(clusters), km.inertia_

def evaluate_directory(directory):
    # Evaluate all kmeans model in a directory
    phrases = []
    clusters = []
    inertias = []
    for filename in glob.glob(os.path.join(directory,"kmeans.*")):
            p,c,i = evaluate_model(filename)
            phrases.append(p)
            clusters.append(c)
            inertias.append(i)

    plot_statistics(phrases,clusters,inertias)

def read_statistics(filename):
    phrases = []
    clusters = []
    inertias = []
    with open(filename) as fp:
        for line in fp:
            p,c,i = line.split()
            phrases.append(int(p))
            clusters.append(int(c))
            inertias.append(float(i))

    return phrases, clusters, inertias

def plot_statistics(phrases,clusters,inertias):
    plt.figure()
    # Normalize inertias
    max_inertia = max(inertias)
    inertias = [i/max_inertia*50 for i in inertias]
    plt.scatter(phrases,clusters,s=inertias)
    plt.title("KMeans Phrase Cluster Inertias")
    plt.xlabel("Number of phrases")
    plt.ylabel("Number of clusters")
    plt.show()

def centroid_distance_distribution(clusterer,metric="euclidean"):
    plt.figure()
    hist = None
    for i in range((clusterer.km.cluster_centers_.shape[0]-1)/10000+1):
        d = scipy.spatial.distance.pdist(
                clusterer.km.cluster_centers_[i*10000:10000*(i+1),:],
                metric)

        # Compute and update min/max distances
        min_d = np.argmin(d)
        max_d = np.argmax(d)
        if hist is None:
            hist, bins = np.histogram(d,bins=50)
        else:
            h, _ = np.histogram(d,bins=bins)
            hist += h
    width = 0.7 * (bins[1] - bins[0])
    center = (bins[:-1] + bins[1:]) / 2
    plt.bar(center, hist, align='center', width=width)
    plt.xlabel("distance")
    plt.ylabel("frequency")
    plt.show()

def radius_distribution(clusterer):
    plt.figure()
    radii = clusterer.radii
    plt.hist(radii,bins=50)
    plt.show()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("action",choices=["dist","inertia","radii"])
    parser.add_argument("--directory",'-d',default="data/temp/rtsift")
    parser.add_argument("--clusterer","-c",default=None)
    parser.add_argument("--metric","-m",choices=["euclidean","cosine"])
    args = parser.parse_args()

    if args.clusterer:
        with open(args.clusterer) as fp:
            clusterer = cPickle.load(fp)

    if args.action == "inertia":
        evaluate_directory(args.directory)
    elif args.action == "dist":
        centroid_distance_distribution(clusterer,metric=args.metric)
    elif args.action == "radii":
        radius_distribution(clusterer)

if __name__ == "__main__":
    main()
