import argparse
import matplotlib.pyplot as plt
import numpy as np
import cPickle

def view_sparsity_pattern(matrix,filename=None,precision=1e-3,
                          xlabel="",title=""):
    fig = plt.imshow(np.absolute(matrix) > precision,aspect="auto",cmap="Greys")
    fig.axes.get_yaxis().set_visible(False)
    plt.title(title)
    plt.xlabel(xlabel)
    if filename:
        plt.savefig(filename+".svg")
    else:
        plt.show()


def view_classifier(classifier_filename):
    with open(classifier_filename) as fp:
        clf = cPickle.load(fp)
        view_sparsity_pattern(clf.coef_,title="Sparsity pattern",
                              xlabel="phrase cluster")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("classifier")
    parser.add_argument("--outputfile", '-o', default=None)

    args = parser.parse_args()

    view_classifier(args.classifier)


if __name__ == "__main__":
    main()
