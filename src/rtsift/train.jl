using SyntacticallyUntiedRNN
using LanguageUtils
using HDF5, JLD
using ArgParse

# Train system using parse trees in single file
function train_business(surnn::SURNN, parsetrees_filename::String, stars_filename::String, words::Dict{String,Vector{Float32}}; batchsize::Integer=1)
	function producer()
		produce_parsetrees(parsetrees_filename)
	end
    
    function stars_producer()
        produce_stars(stars_filename)
    end

    stars = Task(stars_producer)
    
    i = 0
    curr_star = consume(stars)
    for tree_string in Task(producer)
		if isempty(strip(tree_string))
            curr_star = consume(stars)
            if curr_star == nothing
                #println("Warning: Ran out of star ratings from threads file")
                #println("Occurred in ", stars_filename," for sentence ", i)
                break
            end
			continue
		end
        i += 1
		sentence = leaves(tree_string)
		data = Array(Float32,surnn.inputdim,length(sentence))
		get_sentencematrix!(sentence,words,data)
		rloss,closs,num_nodes,predicted_label,probs = train!(surnn,tree_string,data,curr_star;alpha=float32(0.005))
        if isnan(closs) || isnan(rloss)
            println("C loss: ", closs, " R Loss: ", rloss)
            Sys.exit(1)
        end
		if i % 25 == 0
            @printf "R loss: %2.5f, C Loss: %2.5f, P %d, T %d\n" rloss closs predicted_label curr_star
            println(probs)
        end
	end
end

# Files to ignore because of Windows carriage return problems
const ignore = Set{String}(["b00912"])

function main()
	s = ArgParseSettings()
    @add_arg_table s begin
        "--wordvectors", "-w"
            help = "file containing the word vectors"
            default = "../../data/word-vectors/vectors.6B.50d.txt"
        "--model", "-m"
            help = "directory containing all save models (and output directory for new models"
            default = "../../data/temp/rtsift/models/"
        "--parsetrees", "-p"
            help = "directory containing the parsetrees"
            default = "../../data/processed/parsetrees/"
        "--threads", "-t"
            help = "directory containing the threads"
            default = "../../data/processed/threads/"
        "--warmstart", "-s"
        	help = "Warm start from this business"
        	arg_type = Int
        	default = 0
        "--beta", "-b"
        	help = "Weight on classification loss"
        	arg_type = Real
        	default = 1.0
    end

	# Default arguments
	parsed_args = parse_args(ARGS, s)
    
	wordvector_file = parsed_args["wordvectors"]
	saved_directory = parsed_args["model"]
	data_directory = parsed_args["parsetrees"]
    threads_directory = parsed_args["threads"]
	dim = 50
    num_classes = 5

	# Create new SURNN
	surnn = SURNN(dim,num_classes; beta=parsed_args["beta"])

	# Load model if specified
	start_index = parsed_args["warmstart"]
	if (start_index != 0)
		formatted_num = @sprintf "%05d" start_index
		surnn = load(saved_directory*"/surnn-"*formatted_num*".jld","surnn")
	end

	print("Loading word vectors...")
	words = load_wordvectors(wordvector_file)
	println("done. (", length(words), " words read.)")

	# Create directory (if it doesn't exist) to save models
	mkpath(saved_directory)

	filenames = get_filenames(data_directory)[start_index+1:end]
    i = start_index + 1
	for f in filenames
        if f in ignore
            i += 1
            println("Ignoring ", f)
            continue
        end
		println("Processing ", f)
		train_business(surnn,data_directory*f,threads_directory*f,words)
		if (i%100 == 0)
			formatted_num = @sprintf "%05d" i
			@save saved_directory*"/surnn-"*formatted_num*".jld" surnn
		end
        i += 1
	end
end

main()
