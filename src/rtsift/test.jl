using SyntacticallyUntiedRNN
using LanguageUtils
using HDF5, JLD
using ArgParse

function test(surnn::SURNN, parsetrees_filename::String, stars_filename::String, wordvectors::Dict{String,Vector{Float32}}; batchsize::Integer=1)
	function producer()
		produce_parsetrees(parsetrees_filename)
	end
    
    function stars_producer()
        produce_stars(stars_filename)
    end

    stars = Task(stars_producer)
    
    i = 0
    loss = 0.0
    total_num_nodes = 0
    curr_star = consume(stars)
    for tree_string in Task(producer)
		if isempty(strip(tree_string))
            curr_star = consume(stars)
            if curr_star == nothing
                #println("Warning: Ran out of star ratings from threads file")
                #println("Occurred in ", stars_filename," for sentence ", i)
                break
            end
			continue
		end
        i += 1
		sentence = leaves(tree_string)
		data = Array(Float32,surnn.inputdim,length(sentence))
		get_sentencematrix!(sentence,wordvectors,data)
        trees, num_nodes = evaluate(surnn,tree_string,data)
        loss += reconstruction_loss(trees) + surnn.beta*classification_loss(surnn,trees,curr_star)
        total_num_nodes += num_nodes
    end

    return (loss, total_num_nodes)
end

function evaluate_model(model_filename::String,wordvectors::Dict{String,Vector{Float32}},business_filenames::Vector{ASCIIString},basepath::ASCIIString)
    surnn = load(model_filename,"surnn")
    loss = 0.0
    num_nodes = 0
    for fname in business_filenames
        l, n = test(surnn,basepath*fname,"../../data/processed/threads/"*fname,wordvectors)
        loss += l
        num_nodes += n
    end

    println(loss," ", num_nodes)
    return loss
end
    
using StatsBase
function main()
    s = ArgParseSettings()
    @add_arg_table s begin
        "--wordvectors", "-w"
            help = "file containinf the word vectors"
            default = "../../data/word-vectors/vectors.6B.50d.txt"
        "--parsetrees", "-p"
            help = "path to parse trees"
            default = "../../data/processed/parsetrees/"
        "--models", "-m"
            help = "directory containing the models to evaluate"
            default = "../../data/temp/rtsift/models/"
        "--output", "-o"
            help = "output file"
            default = "model-evaluation.txt"
    end

    parsed_args = parse_args(ARGS, s)

    wordvectors_filename = parsed_args["wordvectors"]
    parsetrees_path = parsed_args["parsetrees"]
    surnn_path = parsed_args["models"]
    output_filename = "../../data/temp/rtsift/statistics/"*parsed_args["output"]

    # Making output directory
    mkpath("../../data/temp/rtsift/statistics/")

    num_business = 100
    wordvectors = load_wordvectors(wordvectors_filename)
    all_filenames = readdir(parsetrees_path)
    business_indexes = sample(6000:length(all_filenames),100)

    used_models = String[]
    if isfile(output_filename)
        fp = open(output_filename)
        business_indexes = map(int,split(readline(fp)[2:end-2],","))
        for line in eachline(fp)
            push!(used_models,split(line,":")[1])
        end
        close(fp)
    else
        fp = open(output_filename,"a")
        write(fp,string(business_indexes)"\n")
        close(fp)
    end

    business_filenames = map(string,all_filenames[business_indexes])
    
    for model in readdir(surnn_path)
        if model in used_models
            continue
        end
        println("Processing: ", model)
        total_loss = evaluate_model(surnn_path*model,wordvectors,business_filenames,parsetrees_path)
        println("Total Loss: ", total_loss)
        fp = open(output_filename,"a")
        write(fp,model*": "*string(total_loss)*"\n")
        close(fp)
    end
end

main()
