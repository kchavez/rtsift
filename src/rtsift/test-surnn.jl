using SyntacticallyUntiedRNN
using Softmax
using LanguageUtils

loss = SyntacticallyUntiedRNN.loss

# Gradient check for gradient of local reconstruction loss
function gradient_check(W::Matrix{Float64},b::Vector{Float64},U::Matrix{Float64},c::Vector{Float64}, x::Vector{Float64})
    dW, db, dU, dc, dx = gradient!(W,b,U,c,x)
    # Finite difference computation
    dWhat = zeros(size(dW))
    epsilon = 0.0001
    n, kn = size(W)
    for i = 1:n
        for j = 1:kn
            true_value = W[i,j]
            W[i,j] += epsilon
            loss_plus = loss(W,b,U,c,x)
            W[i,j] = true_value - epsilon
            loss_minus = loss(W,b,U,c,x)
            W[i,j] = true_value 
            dWhat[i,j] = (loss_plus - loss_minus)/(2*epsilon)
        end
    end

    println("Gradient check wrt W...")
    println(vecnorm(dW-dWhat)/length(dW))

    dxhat = zeros(size(dx))
    for i = 1:kn
        true_value = x[i]
        x[i] += epsilon
        loss_plus = loss(W,b,U,c,x)
        x[i] = true_value - epsilon
        loss_minus = loss(W,b,U,c,x)
        x[i] = true_value 
        dxhat[i] = (loss_plus - loss_minus)/(2*epsilon)
    end

    println("Gradient check wrt X...")
    println(norm(dx-dxhat)/length(dx))
end


read_rnn_structure = SyntacticallyUntiedRNN.read_rnn_structure
extract_phrase_trees = SyntacticallyUntiedRNN.extract_phrase_trees
reset_gradients! = SyntacticallyUntiedRNN.reset_gradients!
rootvectors! = SyntacticallyUntiedRNN.rootvectors!
backpropagate = SyntacticallyUntiedRNN.backpropagate
# Function: gradient_check
# -----------------------------------------------------------------------------
# Numerically estimates the gradient of the per tree loss with respect to all
# of the SURNN's parameters and compares it to the gradients computed via
# backpropagation. Prints rms error per element of the gradient.
function gradient_check(surnn::SURNN,tree_string::String,data::Matrix{Float64},class::Integer)
    trees, num_nodes = evaluate(surnn,tree_string, data)
    # Do RNN stuff
    reset_gradients!(surnn)
    offset = 1

    X = Array(Float64,surnn.inputdim,length(trees))
    rootvectors!(trees,X)

    dx, dtheta = softmax_gradient(surnn.theta,X,class,surnn.lambda)
    println("DX")
    println(typeof(dx))
    println(size(dx))
    for t in trees
        backpropagate(surnn,t,dx,class)
    end

    gW = Dict()
    gb = Dict()
    gU = Dict()
    gc = Dict()
    gtheta = dtheta

    for label in keys(surnn.parameters)
        gW[label] = surnn.parameters[label].gW
        gb[label] = surnn.parameters[label].gb
        gU[label] = surnn.parameters[label].gU
        gc[label] = surnn.parameters[label].gc
    end

    gWhat = Dict()
    gbhat = Dict()
    gUhat = Dict()
    gchat = Dict()
    
    # Finite difference
    epsilon = 0.0001
    for label in keys(surnn.parameters)
        # gW
        W = surnn.parameters[label].W
        gWhat[label] = zeros(size(W))
        for i = 1:size(W,1)
            for j = 1:size(W,2)
                true_value = W[i,j]
                surnn.parameters[label].W[i,j] += epsilon
                trees, n = evaluate(surnn,tree_string,data)
                loss_plus = reconstruction_loss(trees) + classification_loss(surnn,trees,class)
                surnn.parameters[label].W[i,j] = true_value - epsilon
                trees, n = evaluate(surnn,tree_string,data)
                loss_minus = reconstruction_loss(trees) + classification_loss(surnn,trees,class)
                surnn.parameters[label].W[i,j] = true_value 
                gWhat[label][i,j] = (loss_plus - loss_minus)/(2*epsilon)
            end
        end

        # gb
        b = surnn.parameters[label].b
        gbhat[label] = zeros(size(b))
        for i = 1:size(b,1)
            true_value = b[i]
            surnn.parameters[label].b[i] += epsilon
            trees, n = evaluate(surnn,tree_string,data)
            loss_plus = reconstruction_loss(trees) + classification_loss(surnn,trees,class)
            surnn.parameters[label].b[i] = true_value - epsilon
            trees, n = evaluate(surnn,tree_string,data)
            loss_minus = reconstruction_loss(trees) + classification_loss(surnn,trees,class)
            surnn.parameters[label].b[i] = true_value 
            gbhat[label][i] = (loss_plus - loss_minus)/(2*epsilon)
        end

        # gU
        U = surnn.parameters[label].U
        gUhat[label] = zeros(size(U))
        for i = 1:size(U,1)
            for j = 1:size(U,2)
                true_value = U[i,j]
                surnn.parameters[label].U[i,j] += epsilon
                trees, n = evaluate(surnn,tree_string,data)
                loss_plus = reconstruction_loss(trees) + classification_loss(surnn,trees,class)
                surnn.parameters[label].U[i,j] = true_value - epsilon
                trees, n = evaluate(surnn,tree_string,data)
                loss_minus = reconstruction_loss(trees) + classification_loss(surnn,trees,class)
                surnn.parameters[label].U[i,j] = true_value 
                gUhat[label][i,j] = (loss_plus - loss_minus)/(2*epsilon)
            end
        end

        # gc
        c = surnn.parameters[label].c
        gchat[label] = zeros(size(c))
        for i = 1:size(c,1)
            true_value = c[i]
            surnn.parameters[label].c[i] += epsilon
            trees, n = evaluate(surnn,tree_string,data)
            loss_plus = reconstruction_loss(trees) + classification_loss(surnn,trees,class)
            surnn.parameters[label].c[i] = true_value - epsilon
            trees, n = evaluate(surnn,tree_string,data)
            loss_minus = reconstruction_loss(trees) + classification_loss(surnn,trees,class)
            surnn.parameters[label].c[i] = true_value 
            gchat[label][i] = (loss_plus - loss_minus)/(2*epsilon)
        end
    end

    # gtheta
    gThetahat = zeros(size(surnn.theta))
    for i = 1:size(surnn.theta,1)
        for j = 1:size(surnn.theta,2)
            true_value = surnn.theta[i,j]
            surnn.theta[i,j] += epsilon
            trees, n = evaluate(surnn,tree_string,data)
            loss_plus = reconstruction_loss(trees) + classification_loss(surnn,trees,class)
            surnn.theta[i,j] = true_value - epsilon
            trees, n = evaluate(surnn,tree_string,data)
            loss_minus = reconstruction_loss(trees) + classification_loss(surnn,trees,class)
            surnn.theta[i,j] = true_value 
            gThetahat[i,j] = (loss_plus - loss_minus)/(2*epsilon)
        end
    end

    for label in keys(gW)
        println("gW_{",label,"}",vecnorm(gW[label]-gWhat[label])/sqrt(length(gW[label])))
        println("gB_{",label,"}",vecnorm(gb[label]-gbhat[label])/sqrt(length(gb[label])))
        println("gU_{",label,"}",vecnorm(gU[label]-gUhat[label])/sqrt(length(gU[label])))
        println("gC_{",label,"}",vecnorm(gc[label]-gchat[label])/sqrt(length(gc[label])))
    end
    
    println("gTheta ", vecnorm(dtheta - gThetahat)/sqrt(length(dtheta)))

end


const sentenceTrees = [
"""(ROOT
    (S
       (ADVP (RB Still))
       (, ,)
       (NP (NNP Quantum))
       (VP (VBZ has)
           (SBAR
             (S
               (NP (DT a) (NN crisis))
               (VP (TO to)
               (VP (VB get)
                   (NP (JJ past) (NN right))
                   (ADVP (RB now)))))))
(. .)))""", 
"""(ROOT
  (S
    (NP (PRP She))
    (ADVP (RB really))
    (VP
      (VP (VBD took)
        (NP (DT the) (NN time))
        (S
          (VP (TO to)
            (VP (VB explain)
              (SBAR
                (SBAR
                  (WHNP (WP what))
                  (S
                    (NP (PRP\$ my) (NNS muscles))
                    (VP (VBD were)
                      (VP (VBG doing)))))
                (CC and)
                (SBAR
                  (WHADVP (WRB why))))))))
      (, ,)
      (CC and)
      (VP (VBD provided)
        (NP (PRP me))
        (PP (IN with)
          (NP
            (NP (DT an) (NN arsenal))
            (PP (IN of)
              (NP (JJ simple) (NNS stretches)
                (CC and)
                (NNS exercises)))
            (SBAR
              (WHNP (WDT that))
              (S
                (VP (VBP have)
                  (VP (VBN made)
                    (NP
                      (NP (DT the) (NN world))
                      (PP (IN of)
                        (NP (NN difference))))))))))))
    (. .)))
""",
"""(ROOT
  (S
    (NP (PRP I))
    (VP (VBP 'm)
      (ADVP (RB finally))
      (PP (IN on)
        (NP
          (NP (DT the) (NN road))
          (PP (TO to)
            (NP (NN recovery))))))
    (. !)))
""",
"""(ROOT
  (S
    (NP (PRP\$ My) (JJ only) (NN regret))
    (VP (VBZ is)
      (SBAR (IN that)
        (S
          (NP (PRP I))
          (VP (VBD did) (RB n't)
            (VP (VB come)
              (S
                (VP (TO to)
                  (VP (VB see)
                    (NP (PRP\$ her) (JJ sooner) (NN !!))))))))))))"""
]
# Functions for testing correctness.
Profile.clear()
function main()
    n = 50
    surnn = SURNN(n,5)
    data = [float32(randn(n,length(leaves(sentence)))) for sentence in sentenceTrees]
    labels = [rand(1:5) for s in sentenceTrees]
    for i = 1:1000
        if mod(i,100) == 0
            println("Processed ", i, " sentences")
        end
        s = sentenceTrees[(i)%length(sentenceTrees)+1]
        x = data[(i)%length(sentenceTrees)+1]
        c = labels[(i)%length(sentenceTrees)+1]
        #if i != 1
        #    @profile r_loss, c_loss, n = train!(surnn,s,x,c;alpha=0.05)
        #else
        #    r_loss, c_loss, n = train!(surnn,s,x,c;alpha=0.05)
        #end
        @time r_loss, c_loss, n = train!(surnn,s,x,c;alpha=float32(0.05))
        pc = predict(surnn,s,x)
        @printf "r loss: %2.6f, c loss: %2.6f, num nodes: %03d, %d/%d\n" r_loss c_loss n pc c
    end
    Profile.print(format=:flat)
end

function run_gradient_check()
    n = 50
    surnn = SURNN(n,5)
    data = [float32(randn(n,length(leaves(sentence)))) for sentence in sentenceTrees]
    labels = [rand(1:5) for s in sentenceTrees]
    for i in 1:length(sentenceTrees)
        gradient_check(surnn,sentenceTrees[i],data[i],labels[i])
    end
end

main()
#run_gradient_check()
