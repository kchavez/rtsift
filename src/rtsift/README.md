# RTSIFT: Review Thread Sifter

## Source files
* exportsurnn.jl: converts an surnn saved as a jld file to a set of csv files with all of the trained parameters.
    Usage:
        julia exportsurnn.jl ../../data/temp/rtsift/models/surnn-400.jld
* extractphrases.jl: extracts phrases and phrase vector representations from training data.
    Usage:
        julia extractphrases.jl [surnn jld file]
