# Softmax Regression Test Script
module Softmax
export softmax_cost, softmax_probs, softmax_predict, softmax_gradient

using NumericExtensions
function softmax_probs(theta::Matrix, x::Vector)
    p = softmax(theta*x) + 1e-6
	return p/sum(p)
end

function softmax_probs(theta::Matrix, X::Matrix)
	p = softmax(theta*sum(X,2)) + 1e-6
	return p/sum(p)
end

function softmax_predict(theta::Matrix, X)
    probs = softmax_probs(theta,X)
    max_prob, label = maximum([(probs[i],i) for i = 1:length(probs)])
    return label
end

function indicator(y::Integer,k::Integer)
	v = zeros(k)
	v[y] = 1.0
	return v
end

function softmax_cost(theta::Matrix,x::Vector,y::Integer,lambda::Real)
	p = softmax_probs(theta,x)
	J = -log(p[y]) + lambda/2 * sum(theta.*theta)
	return J
end

function softmax_cost(theta::Matrix,X::Matrix,y::Integer,lambda::Real)
	p = softmax_probs(theta,X)
	J = -log(p[y]) + lambda/2 * sum(theta.*theta)
	return J
end

function grad_x(theta::Matrix,x::Vector,y::Integer)
	return theta'*(indicator(y,size(theta,1)) - softmax_probs(theta,x))
end

function grad_theta(theta::Matrix,x::Vector,y::Integer,lambda::Real)
	return (softmax_probs(theta,x) - indicator(y,size(theta,1)))*x' + lambda*theta
end

function softmax_gradient(theta::Matrix, x::Vector, y::Integer, lambda::Real)
	p = softmax_probs(theta,x)
	i = indicator(y,size(theta,1))
	return theta'*(i - p), (p-i)*x' + lambda*theta
end


function softmax_gradient(theta::Matrix, X::Matrix, y::Integer, lambda::Real)
	p = softmax_probs(theta,X)
	i = indicator(y,size(theta,1))
	return theta'*(i - p)[:,1], (p-i)*sum(X,2)' + lambda*theta
end

function sgd_step!(theta::Matrix,x::Vector,class::Integer;alpha::Real=0.001,lambda::Real=0.001)
	gx, gtheta = softmax_gradient(theta,x,class,lambda)
	theta[:] = theta - alpha*gtheta
end

function test(lambda,alpha)
	theta = randn(5,100)
	x = randn(100,10)
	y = rand(1:5)

	for i = 1:10000
		sgd_step!(theta,x[:,1],y[1]; lambda=lambda,alpha=alpha)
		c = softmax_cost(theta,x[:,1],y[1],lambda)
		println(i, " Cost: ", c)
	end
end

function gradient_check(theta,x,y,lambda)
	ghat = zeros(size(theta))
	epsilon = 0.0001
	for i = 1:size(theta,1)
		for j = 1:size(theta,2)
			val = theta[i,j]
			theta[i,j] = val + epsilon
			cost_plus = softmax_cost(theta,x,y,lambda)
			theta[i,j] = val - epsilon
			cost_minus = softmax_cost(theta,x,y,lambda)
			ghat[i,j] = (cost_plus - cost_minus)/(2*epsilon)
			theta[i,j] = val
		end
	end

	gx, gt = softmax_gradient(theta,x,y,lambda)

	error = vecnorm(gt - ghat)/length(gt)
	println("Vecnorm difference: ", error)
end

# END MODULE
end
