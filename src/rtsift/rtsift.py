""" Review Thread Sifter system, based on clustered phrases
"""
import time
import os
import glob
import argparse
import nltk.data # sentence tokenization
import sys
import subprocess # launching stanford parser
import cPickle
import shutil

import numpy as np
import scipy
from numpy.linalg import norm
from sklearn.cluster import MiniBatchKMeans
from sklearn.linear_model import SGDClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn import metrics
from sklearn.preprocessing import normalize
from sklearn.cross_validation import train_test_split
from ..util.util import *

import re

# Paths
MODULES_PATH = "modules"

class RTSiftSystem(object):
    def __init__(self, clusterer, classifier, parser_path='3rdparty/stanford-parser/stanford-parser-full-2014-08-27'):
        self.classifier = classifier
        self.clusterer = clusterer

        class_probs = normalize(np.exp(classifier.feature_log_prob_ +
            np.reshape(classifier.class_log_prior_,(5,1))),norm="l1",axis=0)
        cluster_entropies = scipy.stats.entropy(class_probs)

        # Centroids in sorted order (by entropy)
        self.entropies = cluster_entropies
        self.class_indicators = np.argmax(class_probs,axis=0)
        self.ranks = scipy.stats.rankdata(cluster_entropies)
        self.order = np.argsort(self.entropies)
        self.probabilities = np.sum(np.exp(classifier.feature_log_prob_),axis=0)

        self.parser_path = parser_path
        self.temp_space = "rtsift-run"
        self.vectors = "%s/vectors"%(self.temp_space)

        self.scores = np.sum(np.absolute(self.classifier.coef_), axis=0)
        self.prob_thresh = np.mean(self.probabilities)/2
        print "Mean", np.mean(self.probabilities)
        print "STD", np.std(self.probabilities)
        print "Probability Threshold:", self.prob_thresh

    def get_top_clusters(self,label,prob_thresh,N=10):
        candidates = [self.order[i] for i in range(self.order.size) if
                      self.class_indicators[self.order[i]] == label]
        probs = [self.probabilities[c] for c in candidates]

        return [candidates[i] for i in range(len(candidates))
                if probs[i] >= prob_thresh][0:N]

    def get_sentences(self, reviews):
        sentences = []
        sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
        for review in reviews:
             sentences.append([sentence.encode('utf-8') for sentence in sent_detector.tokenize(unicode(review, "utf-8").strip()) if len(sentence) >2])

        new_sentences = []
        for review_sentences in sentences:
            new_review_sentences = []
            for sentence in review_sentences:
                candidates = re.split('[:;]', sentence)
                candidates = [s for s in candidates if len(s) > 2]
                new_review_sentences += candidates
            new_sentences.append(new_review_sentences)
        return new_sentences

    def compute_phrase_vectors(self, reviews):
        # Paths
        parse_trees_path = "%s/parsetrees"%(self.temp_space)

        # Create temp space
        if os.path.exists(self.temp_space):
            # Remove old files
            shutil.rmtree(self.temp_space)
        os.mkdir(self.temp_space)
        os.makedirs(parse_trees_path)

        # First nltk tokenize the review
        print 'Tokenizing and parsing reviews...'
        review_sentences = self.get_sentences(reviews)
        for i,review in enumerate(reviews):
            sentences = review_sentences[i]

            # Next, write sentences to file
            with open(self.temp_space + '/sentenceFile', 'w') as inputFile:
                for sentence in sentences:
                    print >> inputFile, sentence
                    #print >> inputFile, '\n'
                inputFile.close()
                #print 'Writing done....'

            # Call the parser
            with open("%s/b00000"%(parse_trees_path), 'a') as resultFile:
                devnull = open(os.devnull, 'w')
                process = subprocess.Popen([self.parser_path + '/lexparser-custom.sh', self.temp_space + '/sentenceFile'], stdout=resultFile.fileno(), stderr=devnull)
                process.wait()
                print >> resultFile, ""

        # Extract the phrases
        print 'Extracting phrases...'
        process = subprocess.Popen(['julia', 'extractphrases.jl', "-p",
            "../../%s"%(parse_trees_path), "-o",
            "../../%s/vectors"%(self.temp_space), "-m",
            "../../data/temp/rtsift/models/surnn-05500.jld"], cwd='src/rtsift/', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        julia_output = process.stdout.read()

        return review_sentences
        #print julia_output
        # Compute num_phrases read
        #start = julia_output.rfind('Added ') + 6
        #end = julia_output.find('phrase', start)
        #return int(julia_output[start:end])

    def predict_stars(self,reviews):
        if isinstance(reviews,str):
            reviews = [reviews]

        self.compute_phrase_vectors(reviews)
        review_phrases = PhraseReader.get_reviews(self.vectors, ['b00000'])

        # Predict
        data,_  = self.clusterer.transform_reviews(review_phrases)
        predictions = self.classifier.predict(data)

        return predictions

    def summarize(self,reviews, size=10):
        if isinstance(reviews,str):
            reviews = [reviews]

        original_sentences = [item for l in self.compute_phrase_vectors(reviews) for item in l]
        review_phrases = PhraseReader.get_reviews(self.vectors, ['b00000'])

        _, assignments = self.clusterer.transform_reviews(review_phrases)

        # Summarize
        phrases = [phrase for phrase,_,_ in get_phrases(self.vectors, ['b00000']) if phrase]

        candidates = []
        stars = iter(stars)
        true_star = next(stars)
        num_phrases = [r.shape[0] for r in review_phrases]

        offset = 0
        review_index = 0
        for assignment,phrase in zip(assignments,phrases):
            while offset == num_phrases[review_index]:
                true_star = next(stars)
                offset = 0
                review_index += 1

            if abs(self.class_indicators[assignment] - true_star) <= 1 and \
                    self.probabilities[assignment] >= self.prob_thresh:
                candidates.append((phrase,self.entropies[assignment],self.probabilities[assignment],assignment))
            offset += 1

        candidates.sort(key=lambda x: x[1])

        sentences = [sentence.replace(' ', '') for sentence in original_sentences]
        summary = set()
        for phrase,_,_,_ in candidates:
            if len(summary) == size:
                break
            for i,sentence in enumerate(sentences):
                if phrase.replace(' ', '') in sentence:
                    summary.add(original_sentences[i])
                    break
        # Debugging output
        if debug:
            print "Phrase".center(40),"Entropy".center(10), \
                  "Prob.".center(10), "Cluster".center(10)
            print "-"*73
            for phrase,entropy,prob,assignment in candidates:
                print phrase.ljust(40),"%0.4f".center(10) % entropy,\
                "%0.5f".center(10) % prob,"%d".center(10) % assignment

        return summary

    def predict_summarize(self, reviews, stars, size=10, debug=False):
        if isinstance(reviews,str):
            reviews = [reviews]

        original_sentences = [item for l in self.compute_phrase_vectors(reviews) for item in l]
        review_phrases = PhraseReader.get_reviews(self.vectors, ['b00000'])

        # Predict
        data, assignments = self.clusterer.transform_reviews(review_phrases)
        predictions = self.classifier.predict(data)

        # Summarize
        phrases = [phrase for phrase,_,_ in get_phrases(self.vectors, ['b00000']) if phrase]

        candidates = []
        stars = iter(stars)
        true_star = next(stars)
        num_phrases = [r.shape[0] for r in review_phrases]

        offset = 0
        review_index = 0
        for assignment,phrase in zip(assignments,phrases):
            while offset == num_phrases[review_index]:
                true_star = next(stars)
                offset = 0
                review_index += 1

            if abs(self.class_indicators[assignment] - true_star) <= 1 and \
                    self.probabilities[assignment] >= self.prob_thresh:
                candidates.append((phrase,self.entropies[assignment],self.probabilities[assignment],assignment))
            offset += 1

        candidates.sort(key=lambda x: x[1])

        sentences = [sentence.replace(' ', '') for sentence in original_sentences]
        summary = set()
        for phrase,_,_,_ in candidates:
            if len(summary) == size:
                break
            for i,sentence in enumerate(sentences):
                if phrase.replace(' ', '') in sentence:
                    summary.add(original_sentences[i])
                    break
        # Debugging output
        if debug:
            print "Phrase".center(40),"Entropy".center(10), \
                  "Prob.".center(10), "Cluster".center(10)
            print "-"*73
            for phrase,entropy,prob,assignment in candidates[0:2*size]:
                print phrase.ljust(40),"%0.4f".center(10) % entropy,\
                "%0.5f".center(10) % prob,"%d".center(10) % assignment

        return (predictions, summary)

class PhraseClusterer(object):
    def __init__(self,source_directory):
        self.source_dir = source_directory
        self.km = None
        self.radii = None

    def create_clusters(self,num_phrases,num_clusters,output_directory=None):
        KM_BATCH_SIZE = max(20000,num_clusters) #max(1000,num_clusters)
        NUM_ITERS = 25

        phrases = get_phrases(self.source_dir)
        init_vectors = np.empty((num_clusters,50))
        print "Randomly initializing centroids...",
        sys.stdout.flush()
        start = time.time()
        for i in range(num_clusters):
            while True:
                try:
                    phrase, vector, tag = next(phrases)
                    if vector and random.random() < 0.5:
                        break
                except StopIteration:
                    raise

            init_vectors[i,:] = vector

        end = time.time()
        print "done (%d seconds)." % (end - start)

        self.km = MiniBatchKMeans(
                      n_clusters=num_clusters,
                      batch_size=KM_BATCH_SIZE,
                      verbose=True,
                      init=init_vectors)

        for iteration_num in range(NUM_ITERS):
            pvt_iter = get_phrases(self.source_dir)
            batch_num = 0
            num_phrases_processed = 0
            while num_phrases > num_phrases_processed:
                print
                print "-"*40
                print "Iteration %02d: batch %d (%d / %d)" % \
                          (iteration_num,batch_num,(batch_num+1)*KM_BATCH_SIZE,num_phrases)
                print "-"*40
                print "Loading phrases...",
                sys.stdout.flush()
                start = time.time()
                n = min(KM_BATCH_SIZE,num_phrases - num_phrases_processed)
                vectors = np.empty((n,50))
                for i in range(n):
                    v = next(pvt_iter)[1]
                    while not v:
                        v = next(pvt_iter)[1]
                    vectors[i,:] = v

                end = time.time()
                print "done. (%d seconds)" % (end - start)

                print "Minibatch update...",
                sys.stdout.flush()
                start = time.time()
                assignments = self.km.partial_fit(vectors)
                num_phrases_processed += KM_BATCH_SIZE
                end = time.time()
                print "done. (%d seconds)" % (end - start)
                print "Inertia: %f" % self.km.inertia_
                batch_num += 1


            km_filename = "data/temp/rtsift/km.p-%06d.c-%05d.iter-%02d.mdl" % \
                              (num_phrases, num_clusters,iteration_num)
            with open(km_filename,'w') as fp:
                cPickle.dump(self.km,fp)
            print "Saved model."
        self.compute_radii(args.num_phrases)


    def compute_radii(self,num_phrases,output_directory=None,batch_size=20000):
        # Do another pass through the data to find radii
        # Find "radius" of each cluster
        pvt_iter = get_phrases(self.source_dir)
        num_phrases_processed = 0
        RADII_BATCH_SIZE = batch_size
        batch_num = 0
        radii = np.zeros((self.km.n_clusters,))
        print "Batch size:", RADII_BATCH_SIZE
        while num_phrases > num_phrases_processed:
            print "Computing radii. Batch #%d, (%d / %d)" % \
                      (batch_num,num_phrases_processed,num_phrases)
            print "-"*40
            print "Loading phrases...",
            sys.stdout.flush()
            start = time.time()
            n = min(RADII_BATCH_SIZE,num_phrases - num_phrases_processed)
            vectors = np.empty((n,50))
            phrases = []
            for i in range(n):
                p,v = next(pvt_iter)[0:2]
                while not v:
                    p,v = next(pvt_iter)[0:2]
                vectors[i,:] = v
                phrases.append(p)

            end = time.time()
            print "done. (%d seconds)" % (end - start)

            print "Predicting cluster memberships...",
            sys.stdout.flush()
            start = time.time()
            assignments = self.km.predict(vectors)
            end = time.time()
            print "done (%d seconds)" % (end - start)
            for i in range(vectors.shape[0]):
                cluster = assignments[i]
                distance = norm(self.km.cluster_centers_[cluster] - vectors[i,:])
                if distance > radii[cluster]:
                    radii[cluster] = distance

            # Save files with phrase clusters
            if output_directory:
                print "Writing phrase clouds to files..."
                self.save_clusters(
                    phrases,
                    assignments,
                    output_directory)
                print "done."

            num_phrases_processed += n
            batch_num += 1

        print "Cluster radii:", radii
        self.radii = radii


    def save_clusters(self,phrases,assignments,directory):
        if not os.path.isdir(directory):
            os.makedirs(directory)

        for i,phrase in enumerate(phrases):
            n = assignments[i]
            with open(os.path.join(directory,"cluster-%05d.txt" % n),'a') as fp:
                print >> fp, phrase

    def transform_reviews(self,reviews):
        # Review phrases is a list of (lists of phrase vectors) a review, each
        # corresponding to a review
        review_vectors = []

        # TIMERS
        prediction_timer = 0
        transformation_timer = 0
        conversion_timer = 0

        start = time.time()
        assignments = self.km.predict(np.concatenate(reviews, axis=0))
        end = time.time()
        prediction_timer += (end - start)


        matrix = np.zeros((len(reviews),self.km.n_clusters))
        offset = 0
        for i,review in enumerate(reviews):
            start = time.time()
            for phrase_index,a in enumerate(assignments[offset:offset+review.shape[0]]):
                if norm(self.km.cluster_centers_[a] - review[phrase_index,:]) < self.radii[a]:
                    matrix[i,a] += 1
            end = time.time()
            transformation_timer += (end - start)
            offset += review.shape[0]

        start = time.time()
        result = scipy.sparse.csr_matrix(matrix)
        end = time.time()
        conversion_timer += (end - start)

        print 'Total time spent for prediction: %d s' % (prediction_timer)
        print 'Total time spent for transformation: %d s' % (transformation_timer)
        print 'Total time spent for conversion: %d s' % (conversion_timer)
        return result, assignments


    def predict(self,phrase_vectors):
        # Returns index of cluster memembership or None if it doesn't
        # belong to any (as determined by radii)
        assignments = list(self.km.predict(phrase_vectors))
        for i,a in enumerate(assignments):
            if norm(self.km.cluster_centers_[a] -
                    phrase_vectors[i,:]) > self.radii[a]:
                assignments[i] = None

        return assignments

    def release_memory(self):
        # Release memory of objects not needed for prediction
        pass
        #del self.phrases
        #del self.assignments

    def save(self,filename):
        self.release_memory()
        with open(filename,'w') as fp:
            cPickle.dump(self,fp)

    @staticmethod
    def load(filename):
        with open(filename) as fp:
            return cPickle.load(fp)


def main():
    # Fixed paths
    TEMP_DATA = "data/temp/rtsift"
    MODULE_PATH = "modules"

    parser = argparse.ArgumentParser()

    # Arguments to set paths
    parser.add_argument("--phrase-directory",dest="phrase_directory",
                        default="data/phrase-vectors")
    parser.add_argument("--threads-directory",dest="threads_directory",
                        default="data/processed/threads")

    # Arguments for Clusterer
    parser.add_argument("--num-phrases","-p",dest="num_phrases",
                        type=int,default=100000)
    parser.add_argument("--num-clusters",'-c',dest="num_clusters",
                        type=int,default=10000)
    parser.add_argument("--write-clusters","-w",action="store_true",
                        dest="write_clusters",default=False)

    # Arguments for Multinomial Naive Bayes
    parser.add_argument("--num-businesses","-b",dest="num_businesses",type=int,default=100)
    parser.add_argument("--alpha",'-a',type=float,default=1.0)
    parser.add_argument("--warm-start", "-s",dest="warm_start",type=int,default=0)

    args = parser.parse_args()

    # Load phrase clusterer if it exists
    clusterer_filename = os.path.join(TEMP_DATA,
                         "phrase-clusterer.p-%06d.c-%05d.mdl" %
                         (args.num_phrases,args.num_clusters))
    #print clusterer_filename
    if os.path.exists(clusterer_filename):
        print "Loading phrase clusters..."
        clusterer = PhraseClusterer.load(clusterer_filename)
        clusterer.compute_radii(args.num_phrases)
    else:
        # Create clusterer and save model
        print "Creating phrase clusters..."
        clusterer = PhraseClusterer(args.phrase_directory)
        output_directory = "data/temp/rtsift/phrase-clouds" \
                            if args.write_clusters else None
        clusterer.create_clusters(args.num_phrases,args.num_clusters,
                                  output_directory=output_directory)
        print "done."
        print "Saving model to file..."
        clusterer.save(clusterer_filename)
        print "done."

    nb_filename = os.path.join(TEMP_DATA,
                   "nb.p-%06d.c-%05d.b-%04d.mdl" %
                   (args.num_phrases,args.num_clusters,
                    args.num_businesses))

    # Check if model exists
    if os.path.exists(nb_filename):
        print "Model already exists."
        response = raw_input("Retrain anyway?")
        if response != 'y':
            with open(nb_filename,'r') as fp:
                nb = cPickle.load(fp)
            print "Saving module..."
            module = RTSiftSystem(clusterer,nb)
            filename = "rtsift.p-%06d.c-%05d.b-%04d.module" % (args.num_phrases,args.num_clusters,
                                                                    args.num_businesses)
            with open(os.path.join(MODULES_PATH,filename),'w') as fp:
                cPickle.dump(module,fp)
            print "done."
            return

    if args.warm_start > 0:
        with open("warm-start-nb.txt") as fp:
            businesses = eval(fp.readline())

        if len(businesses) != args.num_businesses:
            print "warm start file doesn't match"
            return

        nb_filename = os.path.join(TEMP_DATA,
                       "nb.p-%06d.c-%05d.b-%04d.mdl" %
                       (args.num_phrases,args.num_clusters,
                        args.warm_start))
        if os.path.exists(nb_filename):
            with open(nb_filename) as fp:
                nb = cPickle.load(fp)
        else:
            print "Cannot warm start from %d. Model does not exist." % args.warm_start
            return
    else:
        nb = MultinomialNB(alpha=args.alpha, class_prior=None)
        # Load businesses to use
        businesses = PhraseReader.random_businesses(args.phrase_directory,
                args.num_businesses,suffix=".phrases")
        with open("warm-start-nb.txt",'w') as fp:
            print >> fp, businesses

    # Batch training for multinomial naive bayes
    batch_size = 100 # businesses
    def chunks(l, n):
        """ Yield successive n-sized chunks from l.
        """
        for i in xrange(0, len(l), n):
            yield l[i:i+n]

    batches = chunks(businesses,batch_size)

    dev_businesses = next(batches)
    reviews = PhraseReader.get_reviews(args.phrase_directory, dev_businesses)
    filenames = [os.path.join(args.threads_directory,b) for b in dev_businesses]
    dev_labels = np.array([s for s in get_stars(filenames,-1)])
    print "Transforming to review representation for dev set"
    dev_data, dev_assignments = clusterer.transform_reviews(reviews)
    print "done."

    for i,batch in enumerate(batches):
        # Skip until warm start mark
        if (i*batch_size) < args.warm_start:
            continue

        start = time.time()
        print "Loading reviews and labels..."
        reviews = PhraseReader.get_reviews(args.phrase_directory, batch)
        print "done."

        filenames = [os.path.join(args.threads_directory,b) for b in batch]
        labels = np.array([s for s in get_stars(filenames,-1)])

        if len(labels) != len(reviews):
            print "PROBLEMMMOOOOOOOO"
            print batch
            print len(labels), len(reviews)
            continue

        print "Transforming to review representation..."
        data,_ = clusterer.transform_reviews(reviews)
        print "done."

        nb.partial_fit(data,labels,classes=[1,2,3,4,5])
        predicted_train_labels = nb.predict(data)
        predicted_dev_labels = nb.predict(dev_data)

        tp,tr,tf,_ = metrics.precision_recall_fscore_support(labels,
                        predicted_train_labels)
        dp,dr,df,_ = metrics.precision_recall_fscore_support(dev_labels,
                        predicted_dev_labels)

        end = time.time()

        print "Training"
        print tp
        print tr
        print tf
        print
        print "Dev"
        print dp
        print dr
        print df
        print

        print "Time elapsed: %d seconds" % (end - start)
        nb_filename = os.path.join(TEMP_DATA,
                       "nb.p-%06d.c-%05d.b-%04d.mdl" %
                       (args.num_phrases,args.num_clusters,
                        (i+1)*batch_size))
        # Save classifier
        with open(nb_filename,'w') as fp:
            cPickle.dump(nb,fp)


    print "Saving module..."
    module = RTSiftSystem(clusterer,nb)
    filename = "rtsift.p-%06d.c-%05d.b-%04d.module" % (args.num_phrases,args.num_clusters,
                                                            args.num_businesses)
    with open(os.path.join(MODULES_PATH,filename),'w') as fp:
        cPickle.dump(module,fp)
    print "done."

if __name__ == "__main__":
    main()
