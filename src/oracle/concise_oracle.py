from ..util.util import *
import argparse
import cPickle
import random
import os
import sklearn.metrics as metrics

class Results:
    def __init__(self, k):
        self.k = k
        self.true_stars = []
        self.pred_stars = []

    def add_prediction(self, true, pred):
        self.true_stars.append(true)
        self.pred_stars.append(pred)

    def get_scores(self):
        tp,tr,tf,_ = metrics.precision_recall_fscore_support(self.true_stars,
                self.pred_stars, labels = [1,2,3,4,5], warn_for=())

        return tp, tr, tf

    def num_predictions(self):
        return len(self.pred_stars)

def main():
    parser = argparse.ArgumentParser()

    # Arguments to set paths
    parser.add_argument("--output-directory",dest="output",
                        default="data/temp/oracle/")
    parser.add_argument("--threads-directory",dest="threads_directory",
                        default="data/processed/threads")
    parser.add_argument("--representation-size",'-k',dest="rep_size",
                        type=int, default=5)

    args = parser.parse_args()

    OUTPUT = os.path.join(args.output,'concise-' + str(args.rep_size) + '.res')

    # Create output directory
    if not os.path.isdir(args.output):
        os.makedirs(args.output)

    if os.path.exists(OUTPUT):
        print "Loading earlier results..."
        with open(OUTPUT) as fp:
            results = cPickle.load(fp)
            print 'Loaded %d predictions'%(results.num_predictions())
    else:
        results = Results(args.rep_size)

    filenames = [args.threads_directory + '/' + b for b in PhraseReader.random_businesses(args.threads_directory, 10)]
    review_thread = list(get_reviews(filenames, -1))
    true_stars = list(get_stars(filenames, -1))

    for i, review in enumerate(review_thread):
        words = review.split()
        if len(words) < args.rep_size:
            continue
        words = random.sample(words, args.rep_size)
        print words
        pred = raw_input('What is your prediction? (q to exit): ')
        if pred == 'q':
            break

        pred = int(pred)
        print '(PRED-%d) (TRUE-%d)'%(pred, true_stars[i])
        results.add_prediction(true_stars[i], pred)

        tp,tr,tf = results.get_scores()
        print tp
        print tr
        print tf
        print ''

        with open(OUTPUT,'w') as fp:
            cPickle.dump(results,fp)
        
if __name__ == "__main__":
    main()