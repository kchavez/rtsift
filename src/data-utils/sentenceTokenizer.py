import nltk.data
import sys
import os

if len(sys.argv) != 2:
	print 'Please provide the directory with the text as input!'

dirname = sys.argv[1]

print 'Loading tokenizer model...'
sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')

if not os.path.exists(dirname + '/../sentences/'):
	print 'Creating output Directory...'
	os.mkdir(dirname + '/../sentences/')

fileList = os.listdir(dirname)
numFiles = str(len(fileList))
for fileIndex, filename in enumerate(fileList):
	print 'Processing ' + filename + ' (' + str(fileIndex+1) + '/' + numFiles + ')'
	filepath = dirname + '/' + filename
	resultFilepath = dirname + '/../sentences/' + filename

	with open(filepath) as reviewFile:
		with open(resultFilepath, 'w') as resultFile:
			for line in reviewFile:
				sentences = [sentence.encode('utf-8') for sentence in sent_detector.tokenize(unicode(line[2:], "utf-8").strip()) if len(sentence) >2 ]

				print >> resultFile, "\n".join(sentences)
				print >> resultFile, ''
