#!/bin/bash

# Many reviews end without a period or a exclamation etc, and this
# confuses the Stanford parser. Hence, this script adds a period
# to the end of reviews as necessary

if [ ! $# -ge 1 ]; then
  echo Usage: `basename $0` '<directory>'
  echo
  exit
fi

dir=$1

for file in `ls $dir`
do
	filename=$dir'/'$file
	echo "Processing "$file
	perl -0777 -pi -e 's/([^\.])\n\n/\1\.\n\n/g' $filename
done