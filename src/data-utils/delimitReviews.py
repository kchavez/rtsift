import sys
import os

if len(sys.argv) != 4:
	print 'Usage: python delimitReviews.py <path-to-parse-trees> <path-to-sentences> <path-to-output>'
	sys.exit()

parseTreesDir = sys.argv[1]
sentencesDir = sys.argv[2]
outputDir = sys.argv[3]

if not os.path.exists(outputDir):
	print 'Creating output Directory...'
	os.mkdir(outputDir)

alreadyDone = os.listdir(outputDir)
print alreadyDone
for parseTree in os.listdir(parseTreesDir):
	print 'Processing ' + parseTree + '...',
	if parseTree in alreadyDone:
		print 'skipping'
		continue

	with open(parseTreesDir + '/' + parseTree, 'r') as originalFile:
		with open(sentencesDir + '/' + parseTree, 'r') as sentenceFile:
			with open(outputDir + '/' + parseTree, 'w') as outputFile:
				for currSentence in sentenceFile:
					currSentence = currSentence.strip()

					endOfReview = False
					if currSentence == '':
						endOfReview = True
						print "End of review!"
					currParseTreeLine = '<BEGIN>'
					while not endOfReview and currParseTreeLine != '':
						currParseTreeLine = originalFile.readline()
						#print "PARSE LINE " + currParseTreeLine
						print >> outputFile, currParseTreeLine,
						currParseTreeLine = currParseTreeLine.strip()

					if endOfReview:
						print >> outputFile, "\n",
	print 'done'


