# Notes for the data_utils
## Scripts
* **es_* scripts:** Elastic Search stuff
* **generate_threads.py:** Takes the raw yelp data, and outputs one file per business containing all the reviews for that business. Also creates a mapping.txt, with mappings from *Yelp business ID's* to something of the form *b12345*
* **measure.py:** Collects some statistics (?)
* **renameBusinesses.py:** Renames businesses to the form *b12345* and creates a mapping file (incase you have already genereted threads using an earlier version of *generate_threads.py*)
* **sentenceTokenizer.py:** Runs the nltk sentence tokenizer on the reviews so that we have a nice representation of each thread with one sentence per line
* **sanitizeSentences.sh:** Some reviews don't end with a period or an exclamation, so that Stanford parser gets confused and merges the last sentence of the review with the first sentence of the next review. This script just adds periods where necessary.

## Manual Tweaks
* sentences/b19824 - Requires minor manual tokenizing for review containing 'The Room :'
* sentences/b17123 - Line 152 is lyrics to a song, very long line. Deleted.
* sentences/b21816 - Line 8 is very long. Deleted.
* sentences/b22744 - Line 514 is very long. Deleted.
* sentences/b29548 - Line 222 is very long. Deleted.
* sentences/b30331 - Line 1984
* sentences/b32667 - Line 335
* sentences/b30334 - Line 6805
* sentences/b35612 - Line 659
* sentences/b36038 - Line 10597
* sentences/b38120 - Line 4033
* sentences/b39670 - Line 32

## Quick Scripts
* Run the following command, and it will tell you how many sentences there were, and how many parse trees we have. The number is not always the same, as sometimes the parser lumps multiple sentences into one, because of weird sentence endings like '!!!' etc. Sometime it also separates one sentence into two, if they contain '..' or other weird combinations.
     
    ``
    for i in `ls`; do echo $i; cat $i | grep ROOT | wc -l; cat ../sentences/$i | sed '/^\s*$/d' | wc -l; echo '------'; done
    ``

* Run the following command to check the total runtime in minutes spent on barley building parse trees

	``
	cat *sh.e* | grep real |  awk -F '[\tms]' '{MINS += $2; SECS += $3} END {print MINS+SECS/60}'
	``

* Run the following commands to check for discrepencies in phrase vectors and original reviews:

	``
	b=( b1 b2 b3 business-list bn )
	for i in $b; do echo $i; grep -c '^$' phrase-vectors/$i.phrases; grep -c -v '^$' processed/threads/$i; echo -----; done
	``

## Using Barley machines
* Logon to any corn machine. Preferebly launch a screen session.
* Run the following to renew afs and kerberos tickets:
	`` kinit; aklog ``
* Use qsub <command> to submit a job to the barley machines
* Use qstat to see all current jobs
* Use qdel -u <userid> to delete all running jobs

## 'The Story'
* Number of businesses reduced, as some of them don't have any reviews.
* High alpha (0.1), diverges.
* cluster02828 - talks a whole lot about crusts
* After introducing label information in the SURNN, we have clusters like "love the <FOOD ITEM>" (cluster-49780)
* Challenge: Nearest neighbor in high dimensional space
* Took 52 days 22 hours 10 minutes to create parse trees

* Consider using business b21331 (about Costco) for full-sentence summarization. It is long enoug that you don't want to read the whole thing, but the summary seems great
* Classification system - each has own way of ranking - maybe use an aggregate of classifier
* Confusion matrix

## Applications
* Summarization
* Business Comparison
* Would I like this place?

## Packages
* NearPy
* SciPy
* SKlearn
* NLTK
* Numpy
