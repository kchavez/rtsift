# Import all Yelp data into elasticsearch
import argparse
import os
import json
import itertools

from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk

def generate_actions(filename,type_name,index="yelp",max_items=None):
    with open(filename,'r') as fp:
        if max_items:
            for _ in xrange(max_items):
                yield {    '_index': index,
                            '_type': type_name,
                            '_source': json.loads(next(fp))     }
        else:
            confirm = raw_input("Are you sure you want to import the whole \
                                 file? ")
            if confirm in ("y","yes"):
                for line in fp:
                    yield {    '_index': index,
                                '_type': type_name,
                                '_source': json.loads(line)     }

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--path","-p",default="yelp_data")
    parser.add_argument("--maxitems","-m",type=int,default=10000)
    parser.add_argument("--batch","-b",type=int,default=10000,help="batch size for \
        pushing to elasticsearch")
    args = parser.parse_args()

    filenames = { "review": "yelp_academic_dataset_review.json",
                "business": "yelp_academic_dataset_business.json" }

    es = Elasticsearch()
    # Load review data
    for _type in filenames:
        actions = generate_actions(os.path.join(args.path,filenames[_type]),
                _type,max_items=args.maxitems)

        for n in range(args.maxitems / args.batch + 1):
            batch_actions = itertools.islice(actions, 0, args.batch)
            es_response = bulk(es,batch_actions)
            print es_response

if __name__ == "__main__":
    main()
