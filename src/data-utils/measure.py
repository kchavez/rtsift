import json
from matplotlib import pyplot as plt

def main():
    review_file = "../yelp_data/yelp_academic_dataset_review.json"
    business_file = "../yelp_data/yelp_academic_dataset_business.json"

    review_counts = {}
    with open(business_file,'r') as fp:
        for line in fp:
            business = json.loads(line)
            business_id = business["business_id"]
            if "Restaurants" in business["categories"]:
                review_counts[business_id] = 0

    with open(review_file,'r') as fp:
        for line in fp:
            review = json.loads(line)
            business_id = review['business_id']
            if business_id in review_counts:
                review_counts[business_id] += 1

    big_review_groups = filter(lambda x: x > 64, review_counts.values())
    print "Num restaurants:", len(big_review_groups)
    print "Total reviews:", sum(big_review_groups)
    plt.hist(review_counts.values(),bins=[2**x for x in range(10)])
    plt.show()

if __name__ == "__main__":
    main()
