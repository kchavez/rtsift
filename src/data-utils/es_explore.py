import argparse
from elasticsearch import Elasticsearch

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--max-results","-m",dest="max_results",type=int,default=50)
    args = parser.parse_args()

    es = Elasticsearch()
    while True:
        command = raw_input(">>> ")
        if command in ("q","quit"):
            return

        arguments = command.split()
        if arguments[0] in ("f","find"):
            query = { "query": {
                            "match": {
                                "text": {
                                    "query":    " ".join(arguments[1:]),
                                    "operator": "and"
                                }
                            }
                        }
                    }

            results = es.search('yelp',"review",query,
                                _source=True,size=args.max_results)
            for i,review in enumerate(results['hits']['hits']):
                    print "Review #%d".ljust(20) % i,"Relevance score:",review['_score']
                    print "-"*60
                    print "Star rating:", review['_source']['stars'], \
                            "Date:", review['_source']['date']
                    print review['_source']['text']
                    print "\n"
        else:
            print "Invalid command. Please use one of the following:"
            print "    find <query>"
            print "    quit"

        print
        print

if __name__ == "__main__":
    main()
