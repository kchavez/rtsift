import sys
import os

DATA_DIR = '../yelp_data/threads-stars/'

fileList = os.listdir(DATA_DIR)
numFiles = str(len(fileList))

with open(DATA_DIR + '/../business_star_mapping.txt', 'w') as mappingFile: 
	for fileIndex, filename in enumerate(fileList):
		print 'Processing ' + filename + ' (' + str(fileIndex+1) + '/' + numFiles + ')'
		os.rename(DATA_DIR + '/' + filename, DATA_DIR + '/' + "b%05d"%(fileIndex+1))
		print >> mappingFile, filename + '    ' + "b%05d"%(fileIndex+1)
