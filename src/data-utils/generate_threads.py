import argparse
import json
import os

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("reviewfile")
    parser.add_argument("output")
    parser.add_argument("--num-reviews","-n",
            dest="num_reviews",type=int,default=-1)
    parser.add_argument("--stars","-s",default=False,
                        action="store_true",help="include star rating per review")
    args = parser.parse_args()

    os.makedirs(args.output)

    numBusinessesProcessed = 0

    with open(args.reviewfile,'r') as fp:
        mappingFile = open('mapping.txt', 'w')
        prev_business = None
        prev_business_fp = None
        if args.num_reviews < 0:
            # infinite iterator
            iterator = iter(int,1)
        else:
            iterator = xrange(args.num_reviews)

        for _ in iterator:
            try:
                line = next(fp)
            except:
                break
            review = json.loads(line)
            business = review["business_id"]
            text = review["text"].replace('\n',' ')
            #user = review["user_id"]

            if business != prev_business:
                if prev_business_fp:
                    prev_business_fp.close()
                numBusinessesProcessed += 1
                businessFile = "b%05d"%(numBusinessesProcessed)
                print >> mappingFile, business + ' ' + businessFile
                print 'Processing Business #' + str(numBusinessesProcessed)
                prev_business_fp = open(os.path.join(args.output, businessFile),'a')

            if args.stars:
                print >> prev_business_fp, review["stars"],

            prev_business = business
            print >> prev_business_fp, text.encode('utf8')
            
        mappingFile.close()

if __name__ == "__main__":
    main()
