import subprocess
import os
import sys

class StanfordParser():
	def __init__(self, parserPath):
		self.parserPath = parserPath

	def parseSentences(self, sentences):
		with open(self.parserPath + '/sentenceFile', 'w') as inputFile:
			for sentence in sentences:
				print >> inputFile, sentence
				print >> inputFile, '\n'
			inputFile.close()
			print 'Writing done....'
			# Call the parser
			process = subprocess.Popen([self.parserPath + '/lexparser-custom.sh', self.parserPath + '/sentenceFile'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			output = process.stdout.read()

			# Extract the trees
			print output
			return '' 

	def parseFile(self, filename, resultFilepath):
		with open(resultFilepath, 'w') as resultFile:
			devnull = open(os.devnull, 'w')
			process = subprocess.Popen([self.parserPath + '/lexparser-custom.sh', filename], stdout=resultFile.fileno(), stderr=devnull)
			process.wait()
			#output = process.stdout.read()

			# Extract the trees
			#print >> resultFile, output

	def parseDirectory(self, directory, start=-1, end=-1):
		if not os.path.exists(directory + '/../parsetrees/'):
			print 'Creating output Directory...'
			os.mkdir(directory + '/../parsetrees/')

		fileList = sorted(os.listdir(directory), key=lambda item: (len(item), item))
		numFiles = str(len(fileList))
		for filename in fileList:
			process = False
			if start != -1 and end !=-1:
				#print start, int(filename[1:]), end, start <= int(filename[1:]) <= end
				if start <= int(filename[1:]) <= end:
					process = True
			else:
				process = True

			if process:
				print 'Processing ' + filename + '...',
				filepath = directory + '/' + filename
				resultFilepath = directory + '/../parsetrees/' + filename

				if not os.path.exists(resultFilepath):
					print ''
					with open(filepath) as reviewFile:
						self.parseFile(filepath, resultFilepath)
				else:
					print 'skipping'

if len(sys.argv) < 3:
	print 'Usage python StanfordParserWrapper <path-to-stanford-parser> <path-to-sentences> [<start>] [<end>]'
	sys.exit()

start, end = (-1, -1)
if len(sys.argv) >= 4:
	start = int(sys.argv[3])
if len(sys.argv) >= 4:
	end = int(sys.argv[4])

parser = StanfordParser(sys.argv[1])
parser.parseDirectory(sys.argv[2], start, end)



