""" Better baseline based on clustered word vectors
"""
import os
import glob
import argparse
import cPickle
import itertools
from datetime import datetime
import time
import sys

import numpy as np
from sklearn import metrics
from sklearn.cluster import MiniBatchKMeans
from sklearn.linear_model import SGDClassifier
from sklearn.cross_validation import train_test_split
import nltk
import scipy
import scipy.sparse
from scipy.sparse import csr_matrix


from ..util.trial import Trial
from ..util.util import *

def formatStatistics(precision, recall, f1):
    return ", ".join(["%1.5f"%p for p in precision]) + \
            "\n" + ", ".join(["%1.5f"%r for r in recall]) + \
            "\n" + ", ".join(["%1.5f"%f for f in f1]) + "\n"

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--threads-path",'-p',
                         dest="path",default="data/processed/threads")
    parser.add_argument('-n',dest="num_reviews",type=int,
                        default=1000,help="max number of reviews")
    parser.add_argument('--file','-f',default=None,
                        help="file of business IDs to use")
    parser.add_argument('--clusters','-c',type=int,default=10,help="number of \
                        points used to represent a review")
    parser.add_argument('--output','-o',default=None,help="output directory")
    parser.add_argument('--wordvectors','-w',default="data/word-vectors/vectors.6B.50d.txt")
    parser.add_argument('--kmeans-model',dest="km", default=None,help="previously trained \
                        k-means model")
    parser.add_argument('--save-clusters',dest="save_clusters",action="store_true",default=False)
    args = parser.parse_args()

    # Ensure we have a place to save data and models
    output_path = args.output
    if not output_path:
        output_path = "data/temp/betterbaseline/"

    if not args.km:
        print "Looking for data/temp/betterbaseline/minibatch-kmeans-%05d.mdl"%(args.clusters)
        if os.path.exists("data/temp/betterbaseline/minibatch-kmeans-%05d.mdl"%(args.clusters)):
            args.km = "data/temp/betterbaseline/minibatch-kmeans-%05d.mdl"%(args.clusters)

    trial = Trial(output_path,message=""" Better baseline model representing a
            review as a set of k points in semantic word vector space.""")

    trial.add_to_readme("Commandline args: " + str(args))

    review_thread_files = glob.glob(os.path.join(args.path,"*"))

    reviews = get_reviews(review_thread_files,args.num_reviews)

    # Tokenizing all reviews 
    review_words_file = "data/temp/betterbaseline/review-words-%d.mdl"%(args.num_reviews)
    if os.path.exists(review_words_file):
        print "Loading reviews...",
        sys.stdout.flush()

        start = time.time()
        sys.stdout.flush()

        with open(review_words_file) as fp:
            review_words = cPickle.load(fp)

        end = time.time()
        print "done (%d s)"%(end - start)
    else:
        print "Tokenizing reviews...",
        sys.stdout.flush()

        start = time.time()
        review_words = []
        num_words = 0
        for review in reviews:
            review_words.append(nltk.word_tokenize(unicode(review,"utf-8")))

        trial.save_model(review_words,"review-words-%d.mdl"%(args.num_reviews))

        end = time.time()
        print "done (%d s)"%(end - start)

    # Loading word vectors
    print "Loading word vectors from %s..." % args.wordvectors,
    sys.stdout.flush()
    start = time.time()
    word_vectors = GloveWordVectors(args.wordvectors)
    end = time.time()
    print "done (%d s)"%(end - start)

    # Creating data matrix with word vectors
    print "Creating data matrix(clustering)...",
    sys.stdout.flush()
    start = time.time()

    # NumPy array
    word_to_index = {}
    X = np.empty((len(word_vectors), 50), dtype='float32')

    for word_index, word in enumerate(word_vectors):
        word_to_index[word] = word_index
        X[word_index,:] = word_vectors[word]

    end = time.time()
    print "done (%d s)"%(end - start)
    #print X.shape

    #print np.sum(X1-X)
    if args.km:
        print "Loading kmeans model...",
        sys.stdout.flush()
        with open(args.km) as fp:
            km = cPickle.load(fp)
        print "done."
    else:
        print "Training kmeans model...",
        sys.stdout.flush()

        start = time.time()
        km = MiniBatchKMeans(n_clusters=args.clusters, n_init=10, random_state=429)
        km.fit(X)
        print "done (%d s)."%(time.time() - start)

        print "Saving trained model...",
        sys.stdout.flush()
        trial.save_model(km,"minibatch-kmeans-%d.mdl"%(args.clusters))
        print "done."

    cluster_membership = km.predict(X)

    if args.save_clusters:
        print "Writing word clouds to file...",
        for n in range(km.n_clusters):
            trial.write_file([words[i] for i in range(len(words)) if
                cluster_membership[i] == n], "cluster-%04d.txt" % n)
        print "done."

    # Creating datamatrix
    print "Creating data matrix (classifying)...",
    sys.stdout.flush()
    data = scipy.sparse.lil_matrix((args.num_reviews, km.n_clusters),dtype=float)

    unknownCount = 0
    totalCount = 0
    for review_index in xrange(args.num_reviews):
        for word in review_words[review_index]:
            if word not in word_to_index:
                unknownCount += 1
            totalCount +=1
            word_index = word_to_index.get(word, word_to_index[GloveWordVectors.unknown])

            data[review_index, cluster_membership[word_index]] += 1
    data = scipy.sparse.csr_matrix(data)
    
    print "done."
    print "Unknown words: %d / %d"%(unknownCount, totalCount)
    labels = list(get_stars(review_thread_files,args.num_reviews))
    train, dev, train_labels, dev_labels = train_test_split(data,labels, \
                                                              test_size=0.30,random_state=0)

    # Regularization parameter
    alpha = 0.0001
    sgd = SGDClassifier(loss='log',penalty='l1',alpha=alpha,warm_start=True)

    print "Training classifier..."
    sgd.fit(train,train_labels)
    trial.save_model(sgd,"sgd.mdl")
    predicted_labels = sgd.predict(train)
    print "Done!"

    # Training error
    precision, recall, fscore, support = \
        metrics.precision_recall_fscore_support(train_labels,predicted_labels)
    trial.add_to_readme("Training error:\n" + formatStatistics(precision,recall,fscore))

    # Dev error
    predicted_labels = sgd.predict(dev)
    precision, recall, fscore, support = \
        metrics.precision_recall_fscore_support(dev_labels,predicted_labels)
    trial.add_to_readme("\nDev error:\n" + formatStatistics(precision,recall,fscore))

    trial.add_to_readme("\n")

if __name__ == "__main__":
    main()
