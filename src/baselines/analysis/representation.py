import argparse
import cPickle
import scipy
import numpy as np

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import confusion_matrix

from src.baselines.baseline import *
from src.util.util import *

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("module")
    parser.add_argument("--path",default="data/processed/threads")
    parser.add_argument("--num-reviews","-n",type=int,default=1000)
    args = parser.parse_args()

    with open(args.module) as fp:
        module = cPickle.load(fp)
        classifier = module.classifier
        vectorizer = module.vectorizer

    review_thread_files = list(reversed(glob.glob(os.path.join(args.path,"*"))))
    review_thread_files = ['b01757', 'b20386', 'b21614', 'b27537', 'b04235', 'b33374', 'b06960', 'b14082', 'b31897', 'b09778', 'b28400', 'b01963', 'b31075', 'b03531', 'b09309', 'b35932', 'b23356', 'b14755', 'b03954', 'b28283', 'b30481', 'b30417', 'b35061', 'b06085', 'b20499', 'b31930', 'b05250', 'b13584', 'b28825', 'b23182', 'b28370', 'b24207', 'b14812', 'b08261', 'b03897', 'b33224', 'b24433', 'b10836', 'b16827', 'b13679', 'b06103', 'b03963', 'b25848', 'b28860', 'b07393', 'b39356', 'b02294', 'b10208', 'b41885', 'b06504', 'b15790', 'b39947', 'b32161', 'b33532', 'b06553', 'b18823', 'b14199', 'b07145', 'b02434', 'b38630', 'b12920', 'b19458', 'b33541', 'b28448', 'b06830', 'b05299', 'b13143', 'b33939', 'b22888', 'b07729', 'b33491', 'b16716', 'b22104', 'b21801', 'b06253', 'b27429', 'b37384', 'b20801', 'b35234', 'b24960', 'b12799', 'b12638', 'b31525', 'b33922', 'b11395', 'b03705', 'b00786', 'b10289', 'b02530', 'b24192', 'b21805', 'b24190', 'b15505', 'b22367', 'b40219', 'b06848', 'b17774', 'b33947', 'b04078', 'b14614']
    review_thread_files = [os.path.join(args.path,b) for b in
            review_thread_files]
    args.num_reviews = -1
    review_matrix = vectorizer.transform(get_reviews(review_thread_files,
                                                     args.num_reviews))
    print review_matrix.shape

    # Testing
    print classifier.feature_count_
    print classifier.class_count_

    true_ratings = [rating for rating in
            get_stars(review_thread_files,args.num_reviews)]

    pred_ratings = classifier.predict(review_matrix)

    _,_,scores,_ = metrics.precision_recall_fscore_support(
                       true_ratings, pred_ratings)


    cm = confusion_matrix(pred_ratings,true_ratings)
    print cm
    print scores
    class_probs = normalize(np.exp(classifier.feature_log_prob_ +
                            np.reshape(classifier.class_log_prior_,(5,1))),norm="l1",axis=0)
    word_entropies = scipy.stats.entropy(class_probs)
    # Words in sorted order (by entropy)
    arg_sorted = np.argsort(-word_entropies)

    arg_sorted = np.argsort(np.sum(classifier.feature_count_,axis=0))

    review_matrix = scipy.sparse.lil_matrix(review_matrix)
    words = vectorizer.get_feature_names()
    denoms = np.sum(classifier.feature_count_,axis=1)
    K = float(len(arg_sorted))
    pts = []
    for i in range(len(arg_sorted)):
        # Remove feature from model
        review_matrix[:,arg_sorted[i]] = 0.
        scale_factor = (denoms+K)/(denoms + K -
                        classifier.feature_count_[:,arg_sorted[i]] - i - 1)
        #print "Scale:", scale_factor
        classifier.feature_log_prob_[:,arg_sorted[i+1:]] += np.log(np.reshape(scale_factor,(5,1)))

        denoms -= classifier.feature_count_[:,arg_sorted[i]]
        K -= 1

        pred_ratings = classifier.predict(review_matrix)
        _,_,scores,_ = metrics.precision_recall_fscore_support(
                           true_ratings, pred_ratings)

        pts.append((len(arg_sorted)-i-1,np.mean(scores)))
        if i % 100 == 0:
            print pts[-1], scores, words[arg_sorted[i]]

    with open("baseline-pred-vs-size.txt",'w') as fp:
        cPickle.dump(pts,fp)

if __name__ == "__main__":
    main()
