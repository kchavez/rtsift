""" Baseline based on bag-of-words (TFIDF) and Naive Bayes
"""
import os
import glob
import argparse
import heapq
import cPickle
from datetime import datetime

import numpy as np
import scipy
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import metrics
from sklearn.naive_bayes import BernoulliNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.cross_validation import train_test_split
from sklearn.preprocessing import normalize
from nltk.stem import PorterStemmer
import nltk

from ..util.trial import Trial
from ..util.util import *

# Paths
MODULES_PATH = "modules"

stemmer = PorterStemmer()
def stem_tokenizer(string):
    return [stemmer.stem(word) for word in nltk.word_tokenize(string)]

class BaselineSystem(object):
    def __init__(self,classifier,vectorizer):
        """
        Args:
            classifier: A trained naive bayes classifier
            size:       Max number of words to keep in a summary
        """
        self.classifier = classifier
        self.vectorizer = vectorizer

        class_probs = normalize(np.exp(classifier.feature_log_prob_ +
            np.reshape(classifier.class_log_prior_,(5,1))),norm="l1",axis=0)
        word_entropies = scipy.stats.entropy(class_probs)

        # Words in sorted order (by entropy)
        arg_sorted = np.argsort(word_entropies)
        self.words = np.array(vectorizer.get_feature_names())[arg_sorted]
        self.class_indicators = np.argmax(class_probs,axis=0)[arg_sorted]

        # Keep a mapping of word stems to entropy ranks
        self.entropies = {}
        for rank,word in enumerate(self.words):
            self.entropies[word] = rank

    def summarize(self,review_thread,size=10):
        """ Produces a list of strings that summarize the thread.

            Args:
                review_thread:  list of reviews, each review is a string
        """

        # Handle single string inputs
        if isinstance(review_thread,str):
            review_thread = [review_thread]

        summary_words = set()
        summary = []
        for review in review_thread:
            for word in nltk.word_tokenize(review):
                # Ignore duplicates
                if word in summary_words:
                    continue

                priority = -self.entropies.get(stemmer.stem(word),len(self.entropies))
                candidate = (priority,word)
                if len(summary) > size:
                    lowest = heapq.heappop(summary)
                    summary_words.remove(lowest[1])
                    candidate = max(lowest,candidate)

                heapq.heappush(summary,candidate)
                summary_words.add(candidate[1])

        return summary_words

    def print_to_file(self,filename):
        for rating in range(5):
            trial.write_file([self.words[x] for x in range(len(self.words)) \
                              if self.class_indicators[x] == rating],filename % (rating+1))

    def predict_stars(self,reviews):
        if isinstance(reviews,str):
            reviews = [reviews]

        X = self.vectorizer.transform(reviews)
        stars = self.classifier.predict(X)
        return stars

    def predict_summarize(self, reviews, true_stars, debug=False):
        return (predict_stars(reviews), summarize(reviews))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--threads-path",'-p',
                         dest="path",default="data/processed/threads")
    parser.add_argument('-n',dest="num_reviews",type=int,
                        default=1000,help="max number of reviews")
    parser.add_argument('--file','-f',default=None,
                        help="file of business IDs to use")
    parser.add_argument('--output','-o',default=None,help="output directory")
    args = parser.parse_args()

    # Ensure we have a place to save data and models
    output_path = args.output
    if not output_path:
        output_path = "data/temp/baseline/model-latest" # % datetime.now().isoformat()
    trial = Trial(output_path,message=""" Baseline model using bag of words
                                          with a Naive Bayes classifier""")
    trial.add_to_readme("Commandline args: " + str(args))

    # Create document-term matrix for reviews
    print "Generating document-term matrix..."
    review_thread_files = glob.glob(os.path.join(args.path,"*"))
    vectorizer = CountVectorizer(tokenizer=stem_tokenizer,stop_words="english",
                                 min_df=5,max_df=0.90,max_features=50000)
    review_matrix = vectorizer.fit_transform(get_reviews(review_thread_files,
                                                         args.num_reviews))
    print review_matrix.shape
    print "done."
    #print "Saving vectorizer..."
    #trial.save_model(vectorizer,"vectorizer")
    trial.add_to_readme("Vectorizer: " + str(vectorizer) +
                        "\nNum words: "+str(review_matrix.shape[1]))
    #print review_matrix.shape
    #print "done."

    true_ratings = [rating for rating in
            get_stars(review_thread_files,args.num_reviews)]

    # Shuffle data
    train, dev, train_ratings, dev_ratings = train_test_split(review_matrix,true_ratings, \
                                                              test_size=0.30,random_state=0)

    classifier = MultinomialNB()
    print "Training model..."
    classifier.fit(train,train_ratings)
    #print "Saving model..."
    #trial.save_model(classifier,"naive-bayes.mdl")
    print "done."

    # Save the module
    print "Saving module..."
    system = BaselineSystem(classifier,vectorizer)
    with open(os.path.join(MODULES_PATH,"baseline.%d.module" % args.num_reviews),'w') as fp:
        cPickle.dump(system,fp)
    print "done."

    trial.add_to_readme( "Training error")
    yhat = classifier.predict(train)
    precision, recall, fscore, support = metrics.precision_recall_fscore_support(train_ratings,yhat)
    trial.add_to_readme( precision)
    trial.add_to_readme( recall)
    trial.add_to_readme( fscore)
    trial.add_to_readme( "Class counts: " + str(classifier.class_count_))
    trial.add_to_readme( "Dev error:")
    yhat = classifier.predict(dev)
    precision, recall, fscore, support = metrics.precision_recall_fscore_support(dev_ratings,yhat)
    trial.add_to_readme( precision)
    trial.add_to_readme( recall)
    trial.add_to_readme( fscore)

    significant = raw_input("Save as significant? ")
    if significant in ('y','yes','Y'):
        trial.add_to_readme("\n\n*SIGNIFICANT*")

if __name__ == "__main__":
    main()
