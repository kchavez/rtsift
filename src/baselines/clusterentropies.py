""" Better baseline based on clustered word vectors
"""
import os
import glob
import argparse
import cPickle
import itertools
import time
import sys
import random


import nltk

import matplotlib.pyplot as plt
from scipy.stats import entropy

from ..util.util import *

def formatStatistics(precision, recall, f1):
    return ", ".join(["%1.5f"%p for p in precision]) + \
            "\n" + ", ".join(["%1.5f"%r for r in recall]) + \
            "\n" + ", ".join(["%1.5f"%f for f in f1]) + "\n"

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--threads-path",'-p',
                         dest="path",default="data/processed/threads")
    parser.add_argument('-n',dest="num_reviews",type=int,
                        default=1000,help="max number of reviews")
    parser.add_argument('--file','-f',default=None,
                        help="file of business IDs to use")
    parser.add_argument('--clusters','-c',type=int,default=10,help="number of \
                        points used to represent a review")
    parser.add_argument('--output','-o',default=None,help="output directory")
    parser.add_argument('--wordvectors','-w',default="data/word-vectors/vectors.6B.50d.txt")
    args = parser.parse_args()

    # Ensure we have a place to save data and models
    output_path = args.output
    if not output_path:
        output_path = "data/temp/betterbaseline/"

    review_thread_files = glob.glob(os.path.join(args.path,"*"))

    reviews = get_reviews(review_thread_files,args.num_reviews)



    labels = list(get_stars(review_thread_files,args.num_reviews))

    word_to_ratings = dict()
    for i,review in enumerate(get_reviews(review_thread_files,args.num_reviews)):
        words = nltk.word_tokenize(unicode(review,"utf-8"))
        for word in words:
            if word not in word_to_ratings:
                word_to_ratings[word] = [0,0,0,0,0]
            word_to_ratings[word][int(labels[i])-1] += 1

    #print word_to_ratings


    # Checking cluster
    cluster_dir = "data/temp/betterbaseline/clusters-04000/*"
    cluster_to_ratings = dict()
    c = 0
    t = 0
    for clusterfile in glob.glob(cluster_dir):
        cluster = clusterfile[-4:]
        with open(clusterfile) as cf:
            
            for word in cf:
                t += 1
                word = word.strip()
                if word in word_to_ratings:
                    c += 1
                    if cluster not in cluster_to_ratings:
                        cluster_to_ratings[cluster] = [0,0,0,0,0]
                    for i in range(5):
                        cluster_to_ratings[cluster][i] += word_to_ratings[word][i]
                #else: 
                #    print 'UNKNOWN:',words 

    print "Total words in training: ",len(word_to_ratings)
    print "Total words from clusters in training:",c
    print "Total words from all clusters:",t


    ENTROPY_THRESHOLD = 1.1
    bad_clusters = 0
    total_clusters = 0
    entropies = dict()
    for cluster in cluster_to_ratings:
        total_clusters += 1
        entropies[cluster] = entropy(cluster_to_ratings[cluster])
        if entropies[cluster] > ENTROPY_THRESHOLD:
            bad_clusters += 1
    print "Bad clusters: %0.2f%% (%d/%d)"%(100.0*bad_clusters/total_clusters, bad_clusters, total_clusters)
    #plt.bar([1,2,3,4,5], [-1,-4,4,2,-3])
    #plt.bar(range(len(entropies.values())),entropies.values())
    def normalize(l):
        s = float(sum(l))
        return [i/s for i in l]
    # for _ in range(15):
    #     cluster = random.choice(cluster_to_ratings.keys())
    #     print cluster,cluster_to_ratings[cluster]
    #     plt.figure()
    #     plt.bar(range(5), normalize(cluster_to_ratings[cluster]))

    #     plt.title(str(entropies[cluster]))
    #     plt.axis([0,5,0,1.5])
    # plt.show()

if __name__ == "__main__":
    main()
