# RTSift
Kevin Chavez (kjchavez)
Fahim Dalvi (fdalvi)

# Critical libraries for Python
* SciPy
* SKlearn
* NLTK
* Numpy

# Critical libraries for Julia
* HDF5
* JLD
* ArgParse
* StatsBase
* NumericExtensions

# Note on computation
The project requires modules, which take about a day to compute. Once you have these modules, you can use the bin/demo script to test the system. Modules are huge in size, and hence not included in the submission, but are available upon request. Full GIT repository is also available (contains all src, docs, data, modules). 

# Description of source tree
* bin
    * demo.py - Runs an interactive demo given a module
    * summary_experiment - Given a module, creates summaries for multiple businesses, along with Random and First-Last summaries
* src
    * baselines
        * analysis
            * representation.py - Analyzes the baseline output representation
        * baseline.py - Implementation of baseline
        * betterbaseline.py - Failed implementation (but with great insights) of a better baseline
        * clusterentropies.py - Script to check validity of clusters formed by doing kmeans on baseline features
    * data-utils
        * delimitReviews.py - Delimites the parse trees in a file by reviews
        * generate_threads.py - Generates threads from RAW yelp data
        * renameBusinesses.py - Renames businesses to a more canonical form
        * sanitizeSentences.sh - Sanitizes some of the review threads to get rid of weird characters
        * sentenceTokenizer.py - Tokenizes reviews into sentences
        * StanfordParserWrapper.py - Runs the Stanford Parser on given sentences
    * rtsift
        * analysis
            * cluster_analysis.py - Analyzes clusters
            * confusion_matrix.py - Computes confusion matrix
            * representation.py - Analyzes representation
            * sparsity.py - Visualizing sparsity patterns in SURNN model
            * survey_reader.py - Util to read survey results
            * vspace_visualization.py - PCA to reduce dimensionality and visualization
        * cluster_rank.py - Ranks phrase clouds using entropy scores
        * exportsurnn.jl - Exports SURNN parameters 
        * extractphrases.jl - Extracts phrases given an SURNN and parse trees
        * LanguageUtils.jl - Language utils
        * rtsift.py - Python wrapper for SURNN
        * Softmax.jl - Softmax utils
        * SyntacticallyUntiedRNN.jl - Implementation of SURNN
        * test-surnn.jl - Tests the SURNN and gradient checks 
        * test.jl - Computes statistics on the SURNN
        * train.jl - Trains the SURNN
    * util
        * plotting.py - Plots model evaluation training curve
        * trial.py - Utility to save trials in their respective folders
        * util.py - Misc utils

# Running the project
## Get data that doesn't fit in 20MB
### Get stanford parser
cd 3rdparty/stanford-parser
./get_stanford_parser.sh

### Get GloVe word vectors
Get from http://www-nlp.stanford.edu/data/glove.6B.50d.txt.gz
Extract in mindata/word-vectors/

### Get Julia

## Data Preparation
cd src/data-utils
python sentenceTokenizer.py ../../minidata/threads

### This step takes a long time. We spent 52 days worth compute time for the full dataset, so this should roughly take 30 hours. 
python StanfordParserWrapper.py ../../3rdparty/stanford-parser/stanford-parser-full-2014-08-27/ ../../minidata/sentences
python delimitReviews.py ../../minidata/parsetrees ../../minidata/sentences ../../minidata/parsetrees-new/
cd ../../
cd minidata
rm -rf parsetrees
mv parsetrees-new parsetrees

## RTSift
cd ..
cd src/rtsift
### This step takes a long time. It might take about an hour
julia train.jl -w ../../minidata/word-vectors/vectors.6B.50d.txt -m ../../minidata/models/ -p ../../minidata/parsetrees -t ../../minidata/threads

### This step takes moderate amount of time ~15 minutes for 1000 business
julia extractphrases.jl  -w ../../minidata/word-vectors/vectors.6B.50d.txt -m ../../minidata/models/surnn-01000.jld -p ../../minidata/parsetrees/ -o ../../minidata/phrase-vectors/
cd ../..
python -m src.rtsift.rtsift --phrase-directory minidata/phrase-vectors --num-clusters 5000 --num-phrases 250000 --threads-directory minidata/threads/ --num-businesses 900


# Scores on minidata set provided:
Star rating scores on minidata set
Training
Precision: [ 0.67777778  0.64676617  0.60649819  0.57064018  0.55223881]
Recall   : [ 0.60197368  0.46931408  0.39344262  0.6025641   0.6998739 ]
F1       : [ 0.63763066  0.54393305  0.47727273  0.5861678   0.61735261]

Dev
Precision: [ 0.36158192  0.2866242   0.24444444  0.40450771  0.53632479]
Recall   : [ 0.34782609  0.23195876  0.18394649  0.44458931  0.56152125]
F1       : [ 0.35457064  0.25641026  0.20992366  0.42360248  0.54863388]



# Collaboration Statement
Everything was done together. Independently done works were peer-review always.
    
