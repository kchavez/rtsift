# Project Motto
> What I cannot create, I do not understand.

# Getting the data
* Download the data from the Yelp dataset challenge 2014 and extract it here. We'll refer to the path as $YELP_DATA
* Run 
    python data-utils/generate_threads.py $YELP_DATA/yelp_academic_dataset_review.json $YELP_DATA/threads


# Word2Vec
* Download the model trained by Google on GoogleNews [here](https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit)
* Install gensim package for python.

# Elasticsearch
Not necessary, but if you're interested in browsing/searching through the data painlessly, it's probably a good idea

* Install and start elasticsearch service. If you're running Ubuntu/Debian, you can just run the *install_elasticsearch_debian* script.
* To put put the review and business data in elasticsearch:
    python data-utils/es_import.py --path $YELP_DATA --maxitems 100000
* Then you can use
    python data-utils/es_explore.py
  to do simple queries on the review text

# Critical libraries
* Theano

# Reading List

* Paraphrase detection with recursive auto-encoders
    - [Paraphrase-RAE](http://papers.nips.cc/paper/4204-dynamic-pooling-and-unfolding-recursive-autoencoders-for-paraphrase-detection.pdf)

* Compositional Vector Grammars
    - [SocherBauerManningNg](http://nlp.stanford.edu/pubs/SocherBauerManningNg_ACL2013.pdf)

* For word2vec related info
    - [Linguistic Regularities in Continuous Space Word Representations](http://research.microsoft.com/pubs/189726/rvecs.pdf)
    - [Distributed Representations of Words and Phrases and their Compositionality](http://arxiv.org/pdf/1310.4546.pdf)
    - [Efficient Estimation of Word Representations in Vector Space](http://arxiv.org/pdf/1301.3781.pdf)

* Recursive neural networks
    - [Learning Meaning for Sentences](http://cseweb.ucsd.edu/~elkan/250B/learningmeaning.pdf)
    - [Parsing Natural Scenes and Natural Language with Recursive Neural Networks](http://nlp.stanford.edu/pubs/SocherLinNgManning_ICML2011.pdf)
    - [Tech Talk](http://techtalks.tv/talks/54422/)

* [Richard Socher's Thesis](http://nlp.stanford.edu/~socherr/thesis.pdf) should you be so inclined. It's long, but very readable.

* Optimization techniques
    - [AdaGrad](http://www.ark.cs.cmu.edu/cdyer/adagrad.pdf)

* [Learning Phrase Representations using RNN Encoder–Decoder for Statistical Machine Translation](http://arxiv.org/pdf/1406.1078v1.pdf)
