import argparse
import cPickle
from src.baselines.baseline import *
from src.rtsift.rtsift import *
from src.util.util import *

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("module",help="filename of module to load for demo")
    parser.add_argument("--debug", "-d", action="store_true", default=False)
    args = parser.parse_args()

    print "Loading %s module..." % args.module,
    with open(args.module) as fp:
        module = cPickle.load(fp)
    print "done."

    while True:
        rt_filename = raw_input(">>> Enter review thread file: ")
        if not rt_filename:
            break

        try:
            review_thread = list(get_reviews([rt_filename], -1))
            true_stars = list(get_stars([rt_filename], -1))
        except IOError:
            print "Review thread not found."
            continue

        stars, summary = module.predict_summarize(review_thread,
                true_stars,size=5, debug=args.debug)

        print "Review Thread: %s" % rt_filename
        print "-"*80
        print "Summary: ", " - " + "\n - ".join(summary)
        print
        for star,true_star,review in zip(stars,true_stars,review_thread):
            print "(%d-STAR) (%d-TRUE) %s" % (star, true_star, review)

        print "\n"

if __name__ == "__main__":
    main()
