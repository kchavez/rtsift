#!/bin/python

import argparse
import cPickle
from src.rtsift.rtsift import *
from src.util.util import *
import random

def append_to_survey(summaries, survey_file, order_file, business, review_thread):
    with open(survey_file, 'a') as sf:
        print >> sf, '[[Block:%s]]'%business
        print >> sf, '1. <span style="font-family:lucida sans unicode,lucida grande,sans-serif;">Read the following review thread carefully. Once you&#39;ve finished reading, click next, where you&#39;ll be asked to select the text that best summarizes the reviews. Keep in mind that once you&#39;ve clicked next, you will not be allowed to re-read the review thread!</span>'
        print >> sf, '2. %s'%"<br /><br />".join(review_thread)
        print >> sf, ''
        print >> sf, '[[PageBreak]]'
        print >> sf, ''
        print >> sf, '3. Select the text that best summarizes the review thread:'
        print >> sf, ''
        for system, summary in summaries:
            print >> sf, ' - ' + '<br/> - '.join(summary)
        print >> sf, ''

    with open(order_file, 'a') as of:
        print >> of, '%s'%business
        inadequate = False
        for system, summary in summaries:
            print >> of, system
            if system == 'RTSift':
                if len(summary) < 5:
                    inadequate = True
        if inadequate:
            print >> of, 'INADEQUATE'
        print >> of, ''


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("module",help="filename of module to load for demo")

    args = parser.parse_args()

    print "Loading %s module..." % args.module,
    with open(args.module) as fp:
        module = cPickle.load(fp)
    print "done."

    OUTPUT_FILE = 'survey.txt'
    SUMMARY_SIZE = 5
    businesses = ['data/processed/threads/b00008','data/processed/threads/b00014','data/processed/threads/b00020','data/processed/threads/b00025','data/processed/threads/b00026','data/processed/threads/b00046','data/processed/threads/b00049','data/processed/threads/b00059','data/processed/threads/b00064','data/processed/threads/b00069','data/processed/threads/b00079','data/processed/threads/b00086','data/processed/threads/b00088','data/processed/threads/b00108','data/processed/threads/b00109','data/processed/threads/b00113','data/processed/threads/b00114','data/processed/threads/b00115','data/processed/threads/b00139','data/processed/threads/b00140','data/processed/threads/b00154','data/processed/threads/b00155','data/processed/threads/b00164','data/processed/threads/b00167','data/processed/threads/b00171','data/processed/threads/b00179','data/processed/threads/b00181','data/processed/threads/b00186','data/processed/threads/b00193','data/processed/threads/b00196','data/processed/threads/b00208','data/processed/threads/b00217','data/processed/threads/b00224','data/processed/threads/b00233','data/processed/threads/b00239','data/processed/threads/b00240','data/processed/threads/b00249','data/processed/threads/b00258','data/processed/threads/b00259','data/processed/threads/b00260','data/processed/threads/b00266','data/processed/threads/b00301','data/processed/threads/b00302','data/processed/threads/b00303','data/processed/threads/b00304','data/processed/threads/b00308','data/processed/threads/b00311','data/processed/threads/b00325','data/processed/threads/b00326','data/processed/threads/b00392','data/processed/threads/b00401','data/processed/threads/b00406','data/processed/threads/b00430','data/processed/threads/b00443','data/processed/threads/b00450','data/processed/threads/b00473','data/processed/threads/b00475','data/processed/threads/b00480','data/processed/threads/b00502','data/processed/threads/b00507','data/processed/threads/b00509','data/processed/threads/b00530','data/processed/threads/b00534','data/processed/threads/b00551','data/processed/threads/b00560','data/processed/threads/b00564','data/processed/threads/b00567','data/processed/threads/b00573','data/processed/threads/b00592','data/processed/threads/b00594','data/processed/threads/b00597','data/processed/threads/b00598','data/processed/threads/b00607','data/processed/threads/b00620','data/processed/threads/b00623','data/processed/threads/b00643','data/processed/threads/b00651','data/processed/threads/b00657','data/processed/threads/b00689','data/processed/threads/b00691','data/processed/threads/b00704','data/processed/threads/b00706','data/processed/threads/b00707','data/processed/threads/b00713','data/processed/threads/b00722','data/processed/threads/b00737','data/processed/threads/b00744','data/processed/threads/b00752','data/processed/threads/b00756','data/processed/threads/b00784','data/processed/threads/b00790','data/processed/threads/b00795','data/processed/threads/b00812','data/processed/threads/b00813','data/processed/threads/b00827','data/processed/threads/b00844','data/processed/threads/b00858','data/processed/threads/b00867','data/processed/threads/b00869','data/processed/threads/b00887','data/processed/threads/b00922','data/processed/threads/b00933','data/processed/threads/b00936','data/processed/threads/b00945','data/processed/threads/b00961','data/processed/threads/b00976','data/processed/threads/b00988','data/processed/threads/b01003','data/processed/threads/b01021','data/processed/threads/b01035','data/processed/threads/b01045','data/processed/threads/b01062','data/processed/threads/b01068','data/processed/threads/b01069','data/processed/threads/b01070','data/processed/threads/b01081','data/processed/threads/b01102','data/processed/threads/b01104','data/processed/threads/b01112','data/processed/threads/b01133','data/processed/threads/b01167','data/processed/threads/b01168','data/processed/threads/b01185','data/processed/threads/b01204','data/processed/threads/b01205','data/processed/threads/b01219','data/processed/threads/b01220','data/processed/threads/b01235','data/processed/threads/b01240','data/processed/threads/b01241','data/processed/threads/b01245','data/processed/threads/b01251','data/processed/threads/b01259','data/processed/threads/b01262','data/processed/threads/b01264','data/processed/threads/b01270','data/processed/threads/b01273','data/processed/threads/b01275','data/processed/threads/b01282','data/processed/threads/b01287','data/processed/threads/b01289','data/processed/threads/b01292','data/processed/threads/b01298','data/processed/threads/b01306','data/processed/threads/b01307','data/processed/threads/b01310','data/processed/threads/b01314','data/processed/threads/b01328','data/processed/threads/b01331','data/processed/threads/b01333','data/processed/threads/b01350','data/processed/threads/b01351','data/processed/threads/b01353','data/processed/threads/b01357','data/processed/threads/b01362','data/processed/threads/b01368','data/processed/threads/b01378','data/processed/threads/b01379','data/processed/threads/b01387','data/processed/threads/b01388','data/processed/threads/b01389','data/processed/threads/b01423','data/processed/threads/b01425','data/processed/threads/b01426','data/processed/threads/b01431','data/processed/threads/b01432','data/processed/threads/b01434','data/processed/threads/b01444','data/processed/threads/b01446','data/processed/threads/b01467','data/processed/threads/b01475','data/processed/threads/b01477','data/processed/threads/b01479','data/processed/threads/b01482','data/processed/threads/b01489','data/processed/threads/b01490','data/processed/threads/b01493','data/processed/threads/b01494','data/processed/threads/b01504','data/processed/threads/b01509','data/processed/threads/b01527','data/processed/threads/b01536','data/processed/threads/b01554','data/processed/threads/b01555','data/processed/threads/b01556','data/processed/threads/b01559','data/processed/threads/b01566','data/processed/threads/b01569','data/processed/threads/b01572','data/processed/threads/b01582','data/processed/threads/b01594','data/processed/threads/b01596','data/processed/threads/b01605','data/processed/threads/b01621','data/processed/threads/b01631','data/processed/threads/b01649','data/processed/threads/b01650','data/processed/threads/b01652','data/processed/threads/b01660','data/processed/threads/b01678','data/processed/threads/b01680','data/processed/threads/b01688','data/processed/threads/b01689','data/processed/threads/b01691','data/processed/threads/b01699','data/processed/threads/b01705','data/processed/threads/b01724','data/processed/threads/b01740','data/processed/threads/b01746','data/processed/threads/b01755','data/processed/threads/b01763','data/processed/threads/b01775','data/processed/threads/b01778','data/processed/threads/b01789','data/processed/threads/b01795','data/processed/threads/b01814','data/processed/threads/b01819','data/processed/threads/b01824','data/processed/threads/b01826','data/processed/threads/b01831','data/processed/threads/b01835','data/processed/threads/b01839','data/processed/threads/b01857','data/processed/threads/b01866','data/processed/threads/b01879','data/processed/threads/b01896','data/processed/threads/b01910','data/processed/threads/b01912','data/processed/threads/b01913','data/processed/threads/b01930','data/processed/threads/b01935','data/processed/threads/b01966','data/processed/threads/b01967','data/processed/threads/b01971','data/processed/threads/b01972','data/processed/threads/b01986','data/processed/threads/b01995','data/processed/threads/b02009','data/processed/threads/b02014','data/processed/threads/b02017','data/processed/threads/b02020','data/processed/threads/b02032','data/processed/threads/b02046','data/processed/threads/b02047','data/processed/threads/b02061','data/processed/threads/b02064','data/processed/threads/b02067','data/processed/threads/b02072','data/processed/threads/b02085','data/processed/threads/b02101','data/processed/threads/b02116','data/processed/threads/b02127','data/processed/threads/b02145','data/processed/threads/b02156','data/processed/threads/b02161','data/processed/threads/b02162','data/processed/threads/b02164','data/processed/threads/b02166','data/processed/threads/b02177','data/processed/threads/b02182','data/processed/threads/b02184','data/processed/threads/b02205','data/processed/threads/b02223','data/processed/threads/b02227','data/processed/threads/b02231','data/processed/threads/b02233','data/processed/threads/b02249','data/processed/threads/b02250','data/processed/threads/b02251','data/processed/threads/b02259','data/processed/threads/b02263','data/processed/threads/b02265','data/processed/threads/b02272','data/processed/threads/b02289','data/processed/threads/b02292','data/processed/threads/b02316','data/processed/threads/b02321','data/processed/threads/b02335','data/processed/threads/b02341','data/processed/threads/b02352','data/processed/threads/b02361','data/processed/threads/b02362','data/processed/threads/b02369','data/processed/threads/b02370','data/processed/threads/b02373','data/processed/threads/b02386','data/processed/threads/b02394','data/processed/threads/b02396','data/processed/threads/b02403','data/processed/threads/b02407','data/processed/threads/b02411','data/processed/threads/b02419','data/processed/threads/b02428','data/processed/threads/b02434','data/processed/threads/b02438','data/processed/threads/b02445','data/processed/threads/b02447','data/processed/threads/b02451','data/processed/threads/b02466','data/processed/threads/b02472']
    for rt_filename in businesses:
        print 'Processing',rt_filename

        filenames = [rt_filename]

        try:
            # Summary from RTSift
            review_thread = list(get_reviews(filenames, -1))
            sentences = []
            sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
            for review in review_thread:
                 sentences.append([sentence.encode('utf-8') for sentence in sent_detector.tokenize(unicode(review, "utf-8").strip()) if len(sentence) >2])

            true_stars = list(get_stars(filenames, -1))


            _, summary_rtsift = module.predict_summarize(review_thread, true_stars, size=SUMMARY_SIZE, debug=True)

            # Summary from random
            summary_random = random.sample([item for l in sentences for item in l], SUMMARY_SIZE)

            # Summary from first-last
            directory, business = rt_filename.rsplit('/', 1)
            directory = 'data/phrase-vectors/'
            summary = []
            for r in range(len(review_thread)):
                summary.append(sentences[r][0])
                summary.append(sentences[r][-1])

            #print summary
            summary_first_last = random.sample(summary, SUMMARY_SIZE)
        except Exception as e:
            print 'Exception!!!'
            print e
            continue

        summaries = [("RTSift",summary_rtsift), ("Random",summary_random), ("First-Last",summary_first_last)]
        random.shuffle(summaries)

        inadequate = False
        for system, summary in summaries:
            if system == 'RTSift':
                if len(summary) < 5:
                    inadequate = True
        if inadequate:
            print 'INADEQUATE'
        else:
            append_to_survey(summaries, 'survey.txt', 'order.txt', rt_filename, review_thread)
        #print "Review Thread: %s" % rt_filename
        #print "-"*80
        
        #
        # for system, summary in summaries:
        #     print system
        #     print " - " + "\n - ".join(summary)
        #     print ""

        # raw_input("Show truth?")
        # for system, summary in summaries:
        #   print system
        #   print ", ".join(summary)
        #   print ""


if __name__ == "__main__":
    main()
