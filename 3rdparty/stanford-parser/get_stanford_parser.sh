#! /bin/bash

# This script gets the stanford parser and creates a new executable to run with custom arguments
curl -O http://nlp.stanford.edu/software/stanford-parser-full-2014-08-27.zip
unzip stanford-parser-full-2014-08-27.zip
rm stanford-parser-full-2014-08-27.zip
cp lexparser-custom.sh stanford-parser-full-2014-08-27/lexparser-custom.sh