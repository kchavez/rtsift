This folder contains data processed from 'source':
- threads: All reviews along with their star rating (one per line), with one file per business.
- sentences: All sentences from each review
- parsetrees: All parsetrees for all sentences